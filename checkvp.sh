#!/bin/bash
#
# Script para gerar relatório com o IP dos verificadores.
#
#

USERACTUAL=$(grep "x:$EUID" /etc/group | awk -F ":" '{print $1}')
PATH_USER="/home/$USERACTUAL/programvp"
PATH_LOG="$PATH_USER/log"
ARQRET="$PATH_USER/relatorio_verificador.log"
LOG="$PATH_LOG/error.verificador.log"
ARQSHOPS="$PATH_USER/.temp_tiendas_vp.txt"
USER="root"
PASS="root"
DTATUAL="$(date +%Y%m%d)"

function main(){
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inicio programa." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inciando função ['fnGroupShop']." >> $LOG
    fnGroupShop
    ERR=$?
    if [ $ERR -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Função ['FnGroupShop'] executada com sucesso." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inciando função ['fnGetIP']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciamos um while no array ['ARRAYSHOPS']" >> $LOG
        i=0
        while [ $i != ${#ARRAYSHOPS[@]} ]
        do
            if [[ ${ARRAYSHOPS[i]} = ?(+|-)+([0-9]) ]]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Valor ['${ARRAYSHOPS[i]}'] é do tipo número, incluímos 0 a esquerda." >> $LOG
                SHOP=`echo "${ARRAYSHOPS[i]}"`
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Valor ['${ARRAYSHOPS[i]}'] não é do tipo númerico, será ignorado." >> $LOG
            fi
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Iniciando a função ['GetIP']." >> $LOG
            fnGetIP
            ERR=$?
            if [ $ERR -eq 0 ]; then
                fnValidaConexao
                ERR=$?
                if [ $ERR -eq 0 ]; then
                    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Conexão com sucesso a loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['10001']." >> $LOG
                    sshpass -p $PASS ssh -o ConnectTimeout=1 $ip -p10001 -l $USER '. /confdia/bin/setvari; netstat -uptan | grep "6500.*10.10.10." | cut -d ":" -f2 | sed "s/.\{13\}//" | while read ip ; do if [ "$ip" == "10.10.10.19" ]; then echo "$NUMETIEN""|""$ip""|""NOK"; else echo "$NUMETIEN""|""$ip""|""OK" ; fi ; done' >> $ARQRET
                    ERR=$?
                    if [ $ERR -eq 0 ]; then
                        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Informação para a loja ['${ARRAYSHOPS[i]}']:['$ip']:['10001'] extraida com sucesso." >> $LOG
                    else
                        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao obter dados da loja ['${ARRAYSHOPS[i]}']:['$ip']:['10001']." >> $LOG
                    fi
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao conectar com a loja/caixa ['${ARRAYSHOPS[i]}']:['$ip']:['10001'].Err -> ['$ERR']." >> $LOG
                fi
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:MAIN:Erro ao obter ip para a loja ['${ARRAYSHOPS[i]}'].." >> $LOG
            fi
            let "i = i +1"
        done 
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Erro ao iniciar função ['fnGroupShop'] Err -> ['$ERR']." >> $LOG
    fi

}

function fnGroupShop(){

	echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Inciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Verificamos se arquivo ['$ARQTEMP'] existe." >> $LOG
    if [ -e $ARQSHOPS ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo OK." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Iniciamos um while read no arquivo ['$ARQSHOPS'] e gravamos cada valor em um array." >> $LOG
        while read idshop;
            do
                progress=("$idshop")
        done < $ARQSHOPS
        ARRAYSHOPS=(${progress[0]})
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:ARRAY Shops -> ['$ARRAYSHOPS']." >> $LOG
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo NOK." >> $LOG
    fi
    
}

function fnValidaConexao(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VALIDACONEXAO:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VALIDACONEXAO:Validando conexão ['$SHOP:$ip']." >> $LOG
    sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip exit
}

function fnGetIP(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Verificamos se a loja ['$SHOP'] existe no banco ['$BD']." >> $LOG
    EXISTE=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP'"`
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno existe ['$EXISTE']." >> $LOG
    if [ $EXISTE -eq 1 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] existe, buscamos o IP." >> $LOG
        ip=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP'"`
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno IP -> ['$ip']." >> $LOG
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
    fi
}

main