#!/bin/bash
#
# Autor: Walter Moura (wam001br)
# Data Criação: 05-02-2020
# Dara Modificação: 05-02-2020
#-----------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------
# Descrição : Script que receberá as interações via terminal do servidor (10.106.77.224), 
# realizar o envio de arquivos, recebimentos das lojas.
#
# Versão : 0.1
# - Criar metodo de autenticação por meio de certicado
# - Receber conexão da máquina 10.106.77.224
# - Interpretar por um UID, qual o programa será executado
# - Gerar log das interações
#-----------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------

IDPROGRAMA=$1
PATHAPI="/home/www-data"
PROGRAMAPI="$PATHAPI/programapi"
FILESEND="$PATHAPI/filesend"
FILERECV="$PATHAPI/filerecv"
LOG="$PATHAPI/log/erro.manegerapi.log"
API="$PATHAPI/api/aux-api"
PROGRAMBASH=""

function main(){

    if [ -z $IDPROGRAMA ]; then
        echo 0
    else
        PROGRAMBASH=`cat $API | grep ^$IDPROGRAMA | awk -F ":" '{print $2;}'`
        bash $PROGRAMAPI/$PROGRAMBASH 10.106.77.226
    fi    
}
main
