# encoding: utf-8

import glob
import xml.dom.minidom

if __name__=="__main__":
    z=0
    shop=raw_input("Informe o número da loja, para pesquisar nos arquivos xml:")
    if(shop) and shop.isdigit():
        for file in glob.glob("xml/DCV*.xml"):
            if(file):
                x = xml.dom.minidom.parse(file)
                dataFile = x.documentElement
                if dataFile.getAttribute('idStore') == '%s' %shop:
                    z+=1
                    print '|--> %s' % dataFile.getAttribute('idStore')
                    data = [Product for Product in dataFile.childNodes if Product.nodeType == x.ELEMENT_NODE]
                    for dt in data:
                        if dt.nodeName == "DataProduct":
                            dataProduct = [DataProduct for DataProduct in dt.childNodes if DataProduct.nodeType == x.ELEMENT_NODE]
                            vtDesc=[]
                            for dtProduct in dataProduct:
                                vtDesc.append((dtProduct.getAttribute('desComPro')))

                        if dt.nodeName == "DataPrice":
                            dataPrice = [DataPrice for DataPrice in dt.childNodes if DataPrice.nodeType == x.ELEMENT_NODE]
                            vtCodi=[]
                            vtPrice=[]
                            for dtPrice in dataPrice:
                                vtCodi.append((dtPrice.getAttribute('code')))
                                vtPrice.append((dtPrice.getAttribute('value')))

                            if len(vtDesc) == len(vtCodi):
                                for i in range(len(vtCodi)):
                                    print '|---> %s %s %s' %(vtCodi[i], vtDesc[i], vtPrice[i])
                            else:
                                print 'As estruturas de dados do xml não possuem o mesmo tamanho.'
                                print 'Tamanho DataProduct = %s | DataPrice = %s' % (len(vtDesc), len(vtCodi))

            else:
                print 'Não foi econtrado nenhum arquivo xml no diretório [%s].' %file
    else:
        print 'O valor [%s] não é númerico ou está vazio.' %shop

    if z == 0:
        print 'Não foi encontrado nenhum arquivo para a loja [%s].' % shop
