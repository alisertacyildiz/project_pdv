#ifndef _VERSION_H_
#define _VERSION_H_

#ifdef __cplusplus
extern "C" {
#endif

const char * getpackageversion();

#ifdef __cplusplus
}
#endif

#endif
