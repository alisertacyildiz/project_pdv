#!/bin/bash -x
# Programa para verificar as lojas que não transmitiram no dia anterior, e
# e retornar o motivo da não transmissão.
# 
# Author: Marcos Marin/Walter Moura
# Data Criação: 29/10/2019 - 15:51
# Modificado por: Walter Moura/Marcos Marin
# Data Modificação: 31/10/2019 - 13:16



# Definições de variaveis 'globais'
USERACTUAL=$(grep $EUID /etc/group | awk -F ":" '{print $1}')
PATH_USER="/home/$USERACTUAL/programgn"
PATH_BKP="$PATH_USER/backup"
PATH_LOG="$PATH_USER/log"
BD="$PATH_USER/.baseDeDados.db"
ARQRET="$PATH_USER/analise_transmisao.log"
ARQNAME="analise_transmisao.log"
ARQARRAYFILES="$PATH_USER/.array_files.txt"
ARQARRAYFILESLOG="$PATH_USER/.array_files_log.txt"
FILEJOB="$PATH_USER/.jobcom"
LOG="$PATH_LOG/error.programgn.log"
LOGNAME="error.programgn.log"
USER="root"
PASS="root"

function main(){

    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inicio programa." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciando a função ['AjustArqLog']." >> $LOG
    AjustArqLog
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciando a função ['AjustArqRetorno']." >> $LOG
    AjustArqRetorno
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciando a função ['GroupShop']." >> $LOG
    GroupShop
    i=0
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciamos um while no array ['ARRAYSHOPS']" >> $LOG
    while [ $i != ${#ARRAYSHOPS[@]} ]
    do
        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Verificamos se o valor ['${ARRAYSHOPS[i]}'] é do tipo númerico." >> $LOG
        if [[ ${ARRAYSHOPS[i]} = ?(+|-)+([0-9]) ]]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Valor ['${ARRAYSHOPS[i]}'] é do tipo número, incluímos 0 a esquerda." >> $LOG
            SHOP=`printf "%05d" ${ARRAYSHOPS[i]}`
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Valor ['${ARRAYSHOPS[i]}'] não é do tipo númerico, será ignorado." >> $LOG
        fi
        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciando a função ['GetIP']." >> $LOG
        GetIP
        if [ $RET -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciando a função ['ValidaConexao']." >> $LOG
            ValidaConexao
            if [ $? -eq 0 ]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Enviamos o arquivo ['$FILEJOB'] -> ['$SHOP:$ip']." >> $LOG
                sshpass -p $PASS scp -o ConnectTimeout=1 -P10001 $FILEJOB $USER@$ip:/tmp/
                if [ $? -eq 0 ]; then
                        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciando a função ['VerificaTienda']." >> $LOG
                        VerificaTienda
                    if [ $? -eq 0 ] && [ $STLOJA -eq 0 ]; then
                        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Iniciando a função ['VerificaLojaAberta']." >> $LOG
                        VerificaLojaAberta
                    elif [ $? != 0 ] && [ $STLOJA -eq 1 ]; then
                        echo "$SHOP""|""Loja encontrada difere da informada, [$NUMETIEN]." >> $ARQRET
                    else
                        echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Erro ao executar comando -> ['$?']." >> $LOG
                    fi
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Comando não executado -> ['$?']." >> $LOG
                fi
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Erro ao conectar com a loja ['${ARRAYSHOPS[i]}']." >> $LOG
            fi
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Erro ao obter ip da loja ['${ARRAYSHOPS[i]}']." >> $LOG
        fi
        let "i = i +1"
    done

}

function AjustArqLog(){

    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Iniciando a função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Verifica se existe o diretorio ['$PATH_LOG']." >> $LOG
    if [ -e $PATH_LOG ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Diretório OK." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Verifica se existe o arquivo ['$LOGNAME']." >> $LOG
        if [ -e $LOG ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Arquivo OK." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Verificamos o tamanho do arquivo ['$LOGNAME'']." >> $LOG
            SIZEKB=`du -hsk $LOG | awk -F " " '{print $1;}'`
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Size -> ['$SIZEKB']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Verificamos se o tamanho do log é superior a 100MB." >> $LOG
            if [ $SIZEKB -ge 100000 ]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Arquivo de log maior que 100MB." >> $LOG
                COUNTLOG=`ls -ltr $PATH_BKP/$LOGNAME* | wc -l`
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Verificamos a quntidade de arquivos -> ['$COUNTLOG']." >> $LOG
                if [ $COUNTLOG -eq 5 ]; then
                    cd $PATH_BKP
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Verificamos os arquivos -> ['$PATH_BKP/$LOGNAME']." >> $LOG
                    FILETEMPLOG=`ls -tr $LOGNAME*`
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Preenchemos a variável ['FILETEMPLOG'] com um ['ls $LOGNAME*'] ." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:FILETEMPLOG -> ['$FILETEMPLOG']." >> $LOG
                    echo $FILETEMPLOG > $ARQARRAYFILESLOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Passamos os valores da varável ['FILETEMPLOG'] -> ['$ARQARRAYFILES']." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Iniciamos um while read n arquivo ['$ARQARRAYFILES'] e gravamos cada valor em um array." >> $LOG
                    while read filenamelog;
                        do
                            progsx=("$filenamelog")
                    done < $ARQARRAYFILESLOG
                    ARRAYFILESLOG=(${progsx[0]})
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQAJUSTARQLOGRETORNO:ARRAY Files -> ['$ARRAYFILESLOG']." >> $LOG
                    i=0
                    x=1
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Iniciamos um while no array ['ARRAYFILESLOG']" >> $LOG
                    while [ $i != ${#ARRAYFILESLOG[@]} ]
                    do
                        if [ $i -eq 0 ]; then
                            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Removendo o arquivo ['${ARRAYFILESLOG[i]}']." >> $LOG
                            rm -vf ${ARRAYFILESLOG[i]}
                            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Renomendo o arquivo ['${ARRAYFILESLOG[x]}'] -> ['${ARRAYFILESLOG[i]}']." >> $LOG
                            mv -vf ${ARRAYFILESLOG[x]} ${ARRAYFILESLOG[i]}
                        else
                            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Renomendo o arquivo ['${ARRAYFILESLOG[x]}'] -> ['${ARRAYFILESLOG[i]}']." >> $LOG
                            mv -vf ${ARRAYFILESLOG[x]} ${ARRAYFILESLOG[i]}
                        fi
                        let "i = i +1"
                        let "x = x +1" 
                    done
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Movendo o arquivo ['$LOGNAME'] -> ['$PATH_BKP/$LOGNAME.5']." >> $LOG
                    mv -vf $LOGNAME $ARQNAME.5
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Detectado ['$COUNTLOG'] arquivo." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Renomeando este arquivo." >> $LOG
                    cd $PATH_LOG
                    mv $LOGNAME $LOGNAME.$COUNTLOG
                    mv $LOGNAME.$COUNTLOG $PATH_BKP
                fi
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Arquivo de log menor que 100MB." >> $LOG
            fi
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Arquivo NOK." >> $LOG
        fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Diretório NOK." >> $LOG
    fi
    cd $PATH_USER

}

function AjustArqRetorno(){

    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Iniciando a função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Verifica se existe o diretorio ['$PATH_BKP']." >> $LOG
    if [ -e $PATH_BKP ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Diretório OK." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Verifica se existe o arquivo ['$ARQRET']." >> $LOG
        if [ -e $ARQRET ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Arquivo OK." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Verifica a quntidades de arquivos [$ARQNAME] dentro do diretorio ['$PATH_BKP']." >> $LOG
            COUNTBKP=`ls -ltr $PATH_BKP/$ARQNAME* | wc -l`
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Quantidade de arquivos econtrados -> ['$COUNTBKP']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Testamos se o valor retornado é maior que 0." >> $LOG
            if [ $COUNTBKP -eq 0 ]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Valor retornado igual a 0." >> $LOG
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Movendo ['$ARQRET'] -> ['$PATH_BKP/$ARQNAME.0']." >> $LOG
                mv $ARQRET $PATH_BKP/$ARQNAME.0
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Valor retornado maior que 0." >> $LOG
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Verificamos se valor retornado é igual a 100." >> $LOG
                if [ $COUNTBKP -eq 100 ]; then
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Valor retornado igual a 100." >> $LOG
                    cd $PATH_BKP
                    FILESTEMP=`ls -tr $ARQNAME*`
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Preenchemos a variável ['FILESTEMP'] com um ['ls $ARQNAME*'] ." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:FILESTEMP -> ['$FILESTEMP']." >> $LOG
                    echo $FILESTEMP > $ARQARRAYFILES
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Passamos os valores da varável ['FILESTEMP'] -> ['$ARQARRAYFILES']." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Iniciamos um while read n arquivo ['$ARQARRAYFILES'] e gravamos cada valor em um array." >> $LOG
                    while read filename;
                        do
                            progs=("$filename")
                    done < $ARQARRAYFILES
                    ARRAYFILES=(${progs[0]})
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:ARRAY Files -> ['$ARRAYFILES']." >> $LOG
                    i=0
                    x=1
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Iniciamos um while no array ['ARRAYFILES']" >> $LOG
                    while [ $i != ${#ARRAYFILES[@]} ]
                    do
                        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Removendo o arquivo ['${ARRAYFILES[i]}']." >> $LOG
                        if [ $i = 0 ]; then
                            rm -vf ${ARRAYFILES[i]}
                            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Renomendo o arquivo ['${ARRAYFILES[x]}'] -> ['${ARRAYFILES[i]}']." >> $LOG
                            mv -vf ${ARRAYFILES[x]} ${ARRAYFILES[i]}
                        else
                            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Renomendo o arquivo ['${ARRAYFILES[x]}'] -> ['${ARRAYFILES[i]}']." >> $LOG
                            mv -vf ${ARRAYFILES[x]} ${ARRAYFILES[i]}
                        fi
                        let "i = i +1"
                        let "x = x +1" 
                    done
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Movendo o arquivo ['$ARQRET'] -> ['$PATH_BKP/$ARQNAME.100']." >> $LOG
                    mv $ARQRET $ARQNAME.100
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Valor é menor que 100." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Movendo o arquivo ['$ARQRET'] -> ['$PATH_BKP/$ARQNAME.$COUNTBKP']." >> $LOG    
                    mv $ARQRET $PATH_BKP/$ARQNAME.$COUNTBKP
                fi
            fi
        fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Diretório NOK." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Criando diretório ['$PATH_BKP']." >> $LOG
        cd $PATH_USER
        mkdir backup
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Movendo o arquivo ['$ARQRET'] -> ['$PATH_BKP/$ARQNAME.0']." >> $LOG
        mv $ARQRET $PATH_BKP/$ARQNAME.0
    fi
    cd $PATH_USER
    
}

function GroupShop(){
    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Inciando função." >> $LOG
    ARQTEMP="$PATH_USER/temp_list.txt"
    ARQSHOPS="$PATH_USER/.list_shop.txt"
    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Verificamos se arquivo ['$ARQTEMP'] existe." >> $LOG
    if [ -e $ARQTEMP ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo OK." >> $LOG
        VARTEMP=`while read linha ; do printf "%d\t" $linha ; done < $ARQTEMP`
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Construímos um array com número das lojas e passamos a variável ['VARTEMP']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:VARTEM -> ['$VARTEMP']." >> $LOG
        if [ $? -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Passamos os valores da varável ['VARTEMP'] -> ['$ARQSHOPS'].." >> $LOG
            echo $VARTEMP > $ARQSHOPS
            if [ $? -eq 0 ]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Verificamos se arquivo ['$ARQSHOPS'] existe." >> $LOG
                if [ -e $ARQSHOPS ]; then
                    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo OK." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Iniciamos um while read no arquivo ['$ARQSHOPS'] e gravamos cada valor em um array." >> $LOG
                    while read idshop;
                        do
                            progress=("$idshop")
                    done < $ARQSHOPS
                    ARRAYSHOPS=(${progress[0]})
                    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:ARRAY Shops -> ['$ARRAYSHOPS']." >> $LOG
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo NOK." >> $LOG
                fi
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Erro ao executar comando VARTEMP -> ARQSHOPS -> ['$?']." >> $LOG
            fi
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Erro ao executar comando VARTEMP -> WHILE -> ['$?']." >> $LOG
        fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo NOK." >> $LOG
    fi
}

function GetIP(){
    echo "$(date +%Y%m%d-%H%M%S.%s):GETIP:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):GETIP:Verificamos se a loja ['$SHOP'] existe no banco ['$BD']." >> $LOG
    EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
    echo "$(date +%Y%m%d-%H%M%S.%s):GETIP:Retorno existe ['$EXISTE']." >> $LOG
    if [ $EXISTE -eq 1 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):GETIP:Loja ['$SHOP'] existe, buscamos o IP." >> $LOG
        ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        echo "$(date +%Y%m%d-%H%M%S.%s):GETIP:Retorno IP -> ['$ip']." >> $LOG
        RET=0
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):GETIP:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
        RET=1
    fi
}

function ValidaConexao(){
    echo "$(date +%Y%m%d-%H%M%S.%s):VALIDACONEXAO:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):VALIDACONEXAO:Validando conexão ['$SHOP:$ip']." >> $LOG
    sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip exit
}

function VerificaTienda(){
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICATIENDA:Verificamos se a loja ['$SHOP'] é a mesma que conectamos pelo ip ['$ip']." >> $LOG
    NUMETIEN=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /confdia/bin/setvari;
    echo $NUMETIEN')
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICATIENDA:Valor retornado -> ['$NUMETIEN']." >> $LOG
    if [ $NUMETIEN == $SHOP ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICATIENDA:['$SHOP'] == ['$NUMETIEN']." >> $LOG
        STLOJA="0"
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICATIENDA:['$SHOP'] != ['$NUMETIEN']." >> $LOG
        STLOJA="1"
    fi
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICATIENDA:Iniciamos a função ['VerificaLojaAberta']." >> $LOG
}

function VerificaLojaAberta(){
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICALOJAABERTA:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICALOJAABERTA:Validamos o retorno da função ['VarificaLojaAberta']." >> $LOG
    STLABERTA=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip ' STINIDIA=$(DT=$(date +%d --date="-1 day"); DT=$(printf "%d" $DT); if [ $DT -ge 10 ]; then cat /var/log/messages* | grep -i -a "^$(date "+%b" --date="-1 day")" | grep -i -a "^....$DT"; else cat /var/log/messages* | grep -i -a "^$(date "+%b" --date="-1 day")" | grep -i -a "^.....$DT"; fi | grep -a -i "syslog: Iniciac.*syslogd succeeded" | tail -n1 | wc -l); echo $STINIDIA') 
    if [ $STLABERTA -eq 0 ]; then
        NTPVS=`echo "SELECT tpvs FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        STCAJAS=0
        n=0
        for n in $(seq 2 $NTPVS); do STCAJAS=$(sshpass -p $USER ssh -o ConnectTimeout=1 -p1000$n -l $USER $ip 'cat /confdia/DE/D_E_$(date +%Y%m%d --date="-1 day")*.log | grep -a "RETIRADA" | wc -l'); if [ -z $STCAJAS ]; then echo "0"; else if [ $STCAJAS -ge 1 ]; then break; fi; fi; done
        if [ $STCAJAS -ge 1 ]; then 
            STCAJAS="2"
        else 
            STCAJAS="3"
        fi
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICALOJAABERTA:['$STCAJAS']." >> $LOG
    fi
    if [ $STLABERTA -eq 1 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICALOJAABERTA:['$STLABERTA']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICALOJAABERTA:Iniciamos a função ['VerificaCajeraLog']." >> $LOG
        VerificaCajeraLog
    elif [ $STLABERTA -eq 0 ] && [ $STCAJAS -eq 2 ]; then
        echo "$SHOP""|""Problema na máster." >> $ARQRET
    elif [ $STLABERTA -eq 0 ] && [ $STCAJAS -eq 3 ]; then
         echo "$SHOP""|""Loja não foi aberta." >> $ARQRET
    fi

}

function VerificaCajeraLog(){
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAJERALOG:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAJERALOG:Validamos o retorno da função ['VarificaTienda']['$STLOJA']." >> $LOG
    if [ $STLOJA -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAJERALOG:Retorno da função ['VerificaTienda'] -> ['$STLOJA']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAJERALOG:Verificamos qual caminho estão os logs necessário para avaliação." >> $LOG
        STVERCACAJERA=$(sshpass -p $USER ssh -o ConnectTimeout=1 -p10001 root@$ip 'comando=$(cat /var/log/cajera.log | grep -a -e "$(date +%Y%m%d --date="-1 day")" | wc -l) ; . /tmp/.jobcom;
        if [ $comando -ge 1 ]; then
            echo "export DIRCAJE="\0"" >> /tmp/.jobcom
            echo "DIRCAJE=0"
        else
            echo "export DIRCAJE="\1"" >> /tmp/.jobcom
            echo "DIRCAJE=1"
        fi')
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAJERALOG:['$STVERCACAJERA']." >> $LOG
        VerificaSeFezFimDiaAnt
    fi
}

function VerificaSeFezFimDiaAnt(){
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICASEFEZFIMDIAANT:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICASEFEZFIMDIAANT:Validamos o Fim Dia." >> $LOG
    STFIMDIA=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom; 
    if [ $DIRCAJE -eq 0 ]; then
        STFIMDIA=$(cat /var/log/cajera.log | grep -a -e  "$(date +%Y%m%d --date="-1 day")-2[0123].*/ FIM DE DIA" | wc -l)
        if [ $STFIMDIA -eq 1 ]; then
            echo "1"
        else
            echo "0"
        fi
    else
        STFIMDIA=$(zcat /confdia/backup/cajera.log.gz.1 | grep -a -e  "$(date +%Y%m%d --date="-1 day")-2[0123].*/ FIM DE DIA" | wc -l)
        if [ $STFIMDIA -eq 1 ]; then
            echo "1"
        else
            echo "0"
        fi
    fi')
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICASEFEZFIMDIAANT:['$STFIMDIA']." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICASEFEZFIMDIAANT:Iniciamos a função ['VerificaInventario']." >> $LOG
    VerificaInventario

}

function VerificaInventario(){
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAINVENTARIO:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAINVENTARIO:Validamos o retorno do Fim Dia -> ['$STFIMDIA']." >> $LOG
    if [ $STFIMDIA -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAINVENTARIO:Validamos o Inventario." >> $LOG
        STINVEN=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom; 
        if [ $DIRCAJE -eq 0 ]; then
            STINVEN=$(cat /var/log/cajera.log | grep -a -e  "$(date +%Y%m%d --date="-1 day")-2[0123].*05.*INVENTARIOS" | wc -l)
            if [ $STINVEN -ge 1 ]; then
                echo "1"        
            else
                echo "0"
            fi
        else
            STINVEN=$(zcat /confdia/backup/cajera.log.gz.1 | grep -a -e "$(date +%Y%m%d --date="-1 day")-2[0123].*05.*INVENTARIOS" | wc -l)
            if [ $STINVEN -ge 1 ]; then
                echo "1"
            else
                echo "0"
            fi
        fi')
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAINVENTARIO:['$STINVEN']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAINVENTARIO:Iniciamos a função ['VerificaUltMsgVisorCajera']." >> $LOG
        VerificaUltMsgVisorCajera
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAINVENTARIO:Validamos o Inventario." >> $LOG
        STINVEN=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom; 
        if [ $DIRCAJE -eq 0 ]; then
            STINVEN=$(cat /var/log/cajera.log | grep -a -e  "$(date +%Y%m%d --date="-1 day")-2[0123].*05.*INVENTARIOS" | wc -l)
            if [ $STINVEN -ge 1 ]; then
                echo "1"        
            else
                echo "0"
            fi
        else
            STINVEN=$(zcat /confdia/backup/cajera.log.gz.1 | grep -a -e "$(date +%Y%m%d --date="-1 day")-2[0123].*05.*INVENTARIOS" | wc -l)
            if [ $STINVEN -ge 1 ]; then
                echo "1"
            else
                echo "0"
            fi
        fi')
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAINVENTARIO:['$STINVEN']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAINVENTARIO:Iniciamos a função ['VerificaUltMsgVisorCajera']." >> $LOG
        VerificaUltMsgVisorCajera
    fi
    
}

function VerificaUltMsgVisorCajera(){
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAULTMSGVISORCAJERA:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAULTMSGVISORCAJERA:Validamos o retorno do Inventário -> ['$STINVEN']." >> $LOG
    if [ $STINVEN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAINVENTARIO:Validamos a mensagem do Visor Cajera." >> $LOG
        STVISOR=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom ;
        if [ $DIRCAJE -eq 0 ]; then
            export VAR_TEMP=`cat /var/log/cajera.log | grep $(date +%Y%m%d --date="-1 day") | tail -n1 | cut -d ":" -f 4 | cut -d "/" -f 1 | sed 's/.\{2\}//'`
            if [ -z "$VAR_TEMP" ]; then
                echo "1"
            else
                echo "0"
            fi
        else
            export VAR_TEMP=`zcat /confdia/backup/cajera.log.gz.1 | grep $(date +%Y%m%d --date="-1 day") | tail -n1 | cut -d ":" -f 4 | cut -d "/" -f 1 | sed 's/.\{2\}//'`
            if [ -z "$VAR_TEMP" ]; then
                echo "1"
            else
                echo "0"
            fi
        fi')
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAULTMSGVISORCAJERA:['$STVISOR']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFIVERIFICAULTMSGVISORCAJERACAINVENTARIO:Iniciamos a função ['VerificaSeFaltNao']." >> $LOG
        VerificaSeFaltNao
    elif [ $STINVEN -eq 1 ]; then
        echo "$SHOP""|""Loja estava em Inventário." >> $ARQRET
    fi

}

function VerificaSeFaltNao(){
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICASEFALTNAO:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICASEFALTNAO:Validamos o retorno mensagem Visor Cajera -> ['$STVISOR']." >> $LOG
    if [ $STVISOR -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICASEFALTNAO:Validamos se a loja apertou o ultimo ['NÃO']." >> $LOG
        STULTNAO=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom;
        if [ $DIRCAJE -eq 0 ]; then
            STULTNAO=$(cat /var/log/cajera.log | grep $(date +%Y%m%d --date="-1 day") | tail -n1 | cut -d " " -f 3)
            if [ "$STULTNAO" == "4:TICKET" ] ; then
                echo "1"
            elif [ "$STULTNAO" == "IMPRESSAO" ]; then
                echo "2"
            elif [ "$STULTNAO" == "DESEJA" ]; then
                echo "3"
            else
                echo "0"
            fi
        else
            STULTNAO=$(zcat /confdia/backup/cajera.log.gz.1 | grep $(date +%Y%m%d --date="-1 day") | tail -n1 | cut -d " " -f 3)
            if [ "$STULTNAO" == "4:TICKET" ]; then
                echo "1"
            elif [ "$STULTNAO" == "IMPRESSAO" ]; then
                echo "2"
            elif [ "$STULTNAO" == "DESEJA" ]; then
                echo "3"
            else
                echo "0"
            fi            
        fi')
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICASEFALTNAO:['$STULTNAO']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICASEFALTNAO:Iniciamos a função ['VerificaLink']." >> $LOG
        VerificaLink
    elif [ $STVISOR -eq 1 ]; then
        echo "$SHOP""|""Desligou por chave na 2." >> $ARQRET
    fi

}

function VerificaLink(){
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICALINK:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICALINK:Validamos o retorno da tecla NÃO -> ['$STVISOR']." >> $LOG
    if [ $STULTNAO -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICALINK:Validamos se houve algum problema de comunicação." >> $LOG
        STLINK=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom ;
        if [ $DIRCAJE -eq 0 ]; then
            export VAR_TEMP=`cat /var/log/cajera.log | grep $(date +%Y%m%d --date="-1 day") | tail -n1 | cut -d ":" -f 4 | cut -d "/" -f 1 | sed 's/.\{2\}//'`
            if [ "$VAR_TEMP" == "  ESPERANDO  CHAMADA  " ];then
                STFASE=`cat /var/log/cajera.log | grep -a "$(date +%Y%m%d).*FASE" | cut -d ":" -f 4 | cut -d "/" -f 1 | sed 's/.\{1\}//' | wc -l`
                if [ $STFASE -ge 1 ]; then
                    STTRANSM=`cat /var/log/cajera.log | grep -a "$(date +%Y%m%d).*  TRANSMITIU          /" | tail -n1 | cut -d ":" -f4 | cut -c 3-12 | wc -l`
                    if [ $STTRANSM -eq 0 ]; then
                        echo "1"
                    else
                        STTRAN=`cat /var/log/cajera.log | grep -a "$(date +%Y%m%d).*  TRANSMITIU          /" | tail -n1 | cut -d ":" -f4 | cut -d "/" -f2`
                        if [ -z $STTRAN ]; then    
                            echo "3"
                        else
                            echo "5"
                        fi
                    fi			
                elif [ "$VAR_TEMP" == "  ESPERANDO  CHAMADA  " ] && [ $STFASE -eq 0 ];then
                    echo "4"
                else
                    echo "0"
                fi
            else
                echo "2"
            fi	
        else
            export VAR_TEMP=`zcat /confdia/backup/cajera.log.gz.1 | grep $(date +%Y%m%d --date="-1 day") | tail -n1 | cut -d ":" -f 4 | cut -d "/" -f 1 | sed 's/.\{2\}//'`
            if [ "$VAR_TEMP" == "  ESPERANDO  CHAMADA  " ];then
                STFASE=`zcat /confdia/backup/cajera.log.gz.1 | grep -a "$(date +%Y%m%d).*FASE" | cut -d ":" -f 4 | cut -d "/" -f 1 | sed 's/.\{1\}//' | wc -l`
                if [ $STFASE -ge 1 ]; then
                    STTRANSM=`cat /var/log/cajera.log | grep -a "$(date +%Y%m%d).*  TRANSMITIU          /" | tail -n1 | cut -d ":" -f4 | cut -c 3-12 | wc -l`
                    if [ $STTRANSM -eq 0 ]; then
                        echo "1"
                    else
                        STTRAN=`cat /var/log/cajera.log | grep -a "$(date +%Y%m%d).*  TRANSMITIU          /" | tail -n1 | cut -d ":" -f4 | cut -d "/" -f2`
                        if [ -z $STTRAN ]; then    
                            echo "3"
                        else
                            echo "5"
                        fi
                    fi
                elif [ "$VAR_TEMP" == "  ESPERANDO  CHAMADA  " ] && [ $STFASE -eq 0 ];then
                    echo "4"
                else
                    echo "0"
                fi
            else
                echo "2"
            fi
        fi')
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICALINK:['$STLINK']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICALINK:Iniciamos a função ['VerificaChave4']." >> $LOG
        VerificaChave4
    elif [ $STULTNAO -eq 1 ]; then
	    echo "$SHOP""|""Loja não apertou o úlimo não, máster ficou em [4:TICKET    6:DIARIO / 2:Av. Pag.  8:Re. Pa]." >> $ARQRET
    elif [ $STULTNAO -eq 2 ]; then
        echo "$SHOP""|""Loja não apertou a tecla sim, máster ficou em [IMPRESSAO EM PROCESS / CANCELAR IMPRESSAO]." >> $ARQRET
    elif [ $STULTNAO -eq 3 ]; then
        echo "$SHOP""|""Loja não apertou o úlimo não, máster ficou em [DESEJA IMPRIMIR(S/N) /                   ]." >> $ARQRET
    fi
}

function VerificaChave4(){
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICACHAVE4:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICACHAVE4:Validamos o retorno do Link -> ['$STLINK']." >> $LOG
    if [ $STLINK -eq 0 ] || [ $STLINK -eq 2 ] && [ $STFIMDIA -eq 1 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICACHAVE4:Validamos se entrou em Selecione Programa." >> $LOG
        STSP=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom;
        if [ $DIRCAJE -eq 0 ]; then
            STSP=$(cat /confdia/ficcaje/error.ventas.log | grep -a "$(date +%Y%m%d --date="-1 day")-2[0123].*SALIDA CONTROLADA" | cut -d ":" -f 3 | tr -d "*" | tail -n1 | wc -l)
            if [ $STSP -eq 1 ]; then
                echo "1"
            else
                echo "0"
            fi 
        else
            STSP=$(zcat /confdia/backup/error.ventas.log.gz.1 | grep -a "$(date +%Y%m%d --date="-1 day")-2[0123].*SALIDA CONTROLADA" | cut -d ":" -f 3 | tr -d "*" | tail -n1 | wc -l)
            if [ $STSP -eq 1 ]; then
                echo "1"
            else
                echo "0"
            fi            
        fi')
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICACHAVE4:['$STSP']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICACHAVE4:Iniciamos a função ['VerificaBoot']." >> $LOG
        VerificaBoot
    elif  [ $STLINK -eq 4 ]; then
        STKERNEL=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom;
        STKERNEL=$(DT=$(date +%d --date="-1 day"); DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages | grep -i -a "^$(date "+%b" --date="-1 day")" | grep -i "^....$DT.*2[0,1,2,3].*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | wc -l ; else cat /var/log/messages | grep -i -a "^$(date "+%b" --date="-1 day")" | grep -i "^.....$DT.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | wc -l; fi)
        if [ $STKERNEL -eq 1 ]; then
            STKBOOT=$(cat /confdia/ficcaje/error.ventas.log | grep "$(date +%Y%m%d)-0[567].*DESLIGADO INCORRETAMENTE" | cut -d ":" -f 3 | tail -n1 | wc -l)
            if [ $STKBOOT -eq 1 ]; then
                echo "1"
            else
                echo "0"
            fi
        elif [ $STKERNEL -eq 0 ]; then
            STKERNELOLD=$(DT=$(date +%d --date="-1 day"); DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages.1 | grep -i -a "^$(date "+%b" --date="-1 day")" | grep -i "^....$DT.*2[0,1,2,3].*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | wc -l ; else cat /var/log/messages.1 | grep -i -a "^$(date "+%b" --date="-1 day")" | grep -i "^.....$DT.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | wc -l; fi)
            if [ $STKERNELOLD -eq 1 ]; then
                STKBOOT=$(cat /confdia/ficcaje/error.ventas.log | grep "$(date +%Y%m%d)-0[567].*DESLIGADO INCORRETAMENTE" | cut -d ":" -f 3 | tail -n1 | wc -l)
                if [ $STKBOOT -eq 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            else
                echo "0"
            fi
        else
            echo "0"
        fi')
        if [ $STKERNEL -eq 0 ]; then
            echo "$SHOP""|""Problema com o link." >> $ARQRET
        elif [ $STKERNEL -eq 1 ]; then
            echo "$SHOP""|""Detectado falha de energia." >> $ARQRET
        fi
    elif [ $STLINK -eq 1 ]; then
        echo "$SHOP""|""Problema com o link." >> $ARQRET
    elif [ $STLINK -eq 2 ] && [ $STFIMDIA -eq 0 ]; then
        echo "$SHOP""|""Loja não fez o fim dia ou realizou processo fora do horário." >> $ARQRET
    elif [ $STLINK -eq 3 ]; then
        echo "$SHOP""|""Loja transmitiu." >> $ARQRET
    elif [ $STLINK -eq 5 ]; then
        echo "$SHOP""|""Transmissão interrompida." >> $ARQRET
    fi
}

function VerificaBoot(){
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICABOOT:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICABOOT:Validamos o retorno Selecione Programa -> ['$STSP']." >> $LOG
    if [ $STSP -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICABOOT:Validamos se Desligou Incorretamente." >> $LOG
        STBOOT=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom;
        if [ $DIRCAJE -eq 0 ]; then
            STBOOT=$(cat /confdia/ficcaje/error.ventas.log | grep "$(date +%Y%m%d --date="-1 day")-2[0123].*DESLIGADO INCORRETAMENTE" | cut -d ":" -f 3 | tail -n1 | wc -l)
            if [ $STBOOT -eq 1 ]; then
                echo "1"
            else
                echo "0"
            fi
        else
            STBOOT=$(zcat /confdia/backup/error.ventas.log.gz.1 | grep "$(date +%Y%m%d --date="-1 day")-2[0123].*DESLIGADO INCORRETAMENTE" | cut -d ":" -f 3 | tail -n1 | wc -l)
            if [ $STBOOT -eq 1 ]; then
                echo "1"
            else
                echo "0"
            fi
        fi')
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICABOOT:['$STBOOT']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICABOOT:Iniciamos a função ['VerificaEBD']." >> $LOG
        VerificaEBD
    elif [ $STSP -eq 1 ]; then
        echo "$SHOP""|""Máster entrou em selecione programa." >> $ARQRET 
    fi    
}

function VerificaEBD(){
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAEBD:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAEBD:Validamos o retorno Verifica Boot -> ['$STBOOT']." >> $LOG
    if [ $STBOOT -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAEBD:Validamos se entrou em EBD." >> $LOG
        STEBD=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom;
        if [ $DIRCAJE -eq 0 ]; then
            STEBD=$(cat /var/log/cajera.log | grep -a -e "$(date +%Y%m%d --date="-1 day")-2[0123].* ARQU. dat NUMERO)" | tail -n1 | wc -l)
            if [ $STEBD -eq 1 ]; then
                echo "1"
            else
                STEEBD=$(cat /var/log/cajera.log | grep -a "$(date +%Y%m%d --date="-1 day")-2[0123].*ERRO EM BASE DADOS" | tail -n1 | wc -l)
                if [ $STEEBD ]; then
                    echo "2"
                else
                    echo "0"
                fi
            fi
        else
            STEBD=$(zcat /confdia/backup/cajera.log.gz.1 | grep -a -e "$(date +%Y%m%d --date="-1 day")-2[0123].* ARQU. dat NUMERO)" | tail -n1 | wc -l)
            if [ $STEBD -eq 1 ]; then
                echo "1"
            else
                STEEBD=$(zcat /confdia/backup/cajera.log.gz.1 | grep -a "$(date +%Y%m%d --date="-1 day")-2[0123].*ERRO EM BASE DADOS" | tail -n1 | wc -l)
                if [ $STEEBD ]; then
                    echo "2"
                else
                    echo "0"
                fi
            fi
        fi')
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAEBD:['$STEBD']." >> $LOG
        if [ $STEBD -eq 1 ]; then
            echo "$SHOP""|""Recuperando arquivos." >> $ARQRET
        elif [ $STEBD -eq 2 ]; then
            echo "$SHOP""|""Erro em Base de Dados." >> $ARQRET
        elif [ $STEBD -eq 0 ]; then
            echo "$SHOP""|""Erro não catalogado." >> $ARQRET
        fi
    elif [ $STBOOT -eq 1 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):VERIFICAEBD:Retorno não conhecido -> ['$STEBD']." >> $LOG
        echo "$SHOP""|""Caixa desligado incorretamente." >> $ARQRET
    fi
}

main