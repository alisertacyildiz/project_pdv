#!/bin/bash
#
# Criado por: Walter Moura(wam001br)
# Data Criação: 24.03.2020
# Modificado por: Walter Moura
# Data moficação: 27.03.2020
#
# Serviço para gerar um listado com stock online das lojas, com corte de 15/30 min e enviar os dados via FTP
# a um servidor de banco de dados, se encarregara de ler os dados e fazer o input no banco de dados.
#
# Version 1.0
# Base de dados
# - catalog.db 
# Ler as seguintes tabelas do PDV:
# - MAEARTI1
# - Family
# - SubFamily
# - Section
# - ACUARTI1

. /confdia/bin/setvari

PATH_USER="/root/outquery"
BD="/confdia/movil/catalog.db"
OUTFILE="$PATH_USER/list_stock"
NAMEFILE="list_stock"
SHOP=${NUMETIEN}
PATH_BKP="$PATH_USER/backup"
ARQARRAYFILES=".temp_files.txt"
OUTQUERY="$PATH_USER/.query_stock"

function main(){

	check_files
	query_items
	ajust_query

}

function check_files(){

	echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Verificando se o diretorio ['$PATH_USER'] existe."
	if [ -e $PATH_USER ]; then
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] já existe."
	else
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] não existe."
		mkdir outquery
		RETURN=$?
		if [ $RETURN -eq 0 ]; then
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Diretorio ['$PATH_USER'] criado com sucesso."
		else
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Ocorreu um erro ao criar o diretorio ['$PATH_USER']."
		fi
	fi
	echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Verificamos se o diretorio ['$PATH_BKP'] existe."
	if [ -e $PATH_BKP ]; then
		COUNTBKP=`ls -ltr $PATH_BKP/$NAMEFILE* | wc -l`
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Verificamos quantos arquivos existem no diretorio ['$PATH_BKP']:['$COUNTBKP']."
        cd  $PATH_USER
		if [ $COUNTBKP -eq 0 ]; then
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Nao foi encontrado nenhuma arquivo de backup ['$PATH_BKP']:['$COUNTBKP']."
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Movemos ['$NAMEFILE'] -> ['$PATH_BKP/$NAMEFILE.0']."
			mv -vf $NAMEFILE $PATH_BKP/$NAMEFILE.0
		else
			echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Verificamos se existem mais de 100 arquivos no diretorio ['$PATH_BKP']:['$COUNTBKP']."
			if [ $COUNTBKP -eq 101 ]; then
				cd $PATH_BKP
				FILESTEMP=`ls -tr $NAMEFILE*`
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:O diretorio ['$PATH_BKP']:['$COUNTBKP'] contem mais de 100, faremos ajuste dos arquivos."
				echo $FILESTEMP > $ARQARRAYFILES
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Passamos o nome de cada arquivo para variavel ['FILESTEMP'] e gravamos essa variavel em um arquivo de texto [$ARQARRAYFILES] que sera utilizado com um ARRAY."
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Iniciamos a leitura do arquivo ['$ARQARRAYFILES'] e passamos para uma variavel do tipo vetor ['VETORFILES']."
				while read filename;
				do
					progress=("$filename")
				done < $ARQARRAYFILES
				VETORFILES=(${progress[0]})
				i=0
				x=1
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Iniciamos a leitura do vetor ['$VETORFILES'] renomeando cada arquivo."
				while [ $i != ${#VETORFILES[@]} ]
				do
					if [ $i -eq 0 ]; then
						echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Deletamos o arquivo anterior ['${VETORFILES[i]}']."
					    rm -vf ${VETORFILES[i]}
						echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Renomeamos o arquivo  ['${VETORFILES[x]}'] -> ['${VETORFILES[i]}']."
					    mv -vf ${VETORFILES[x]} ${VETORFILES[i]}
					else
						echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Renomeamos o arquivo  ['${VETORFILES[x]}'] -> ['${VETORFILES[i]}']."
						mv -vf ${VETORFILES[x]} ${VETORFILES[i]}
					fi
					let "i = i +1"
					let "x = x +1"
					if [ $i -eq 100 ]; then
						break
					fi
				done
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Em 100 arquivos paramos o loop no vetor e renomeamos ['$NAMEFILE'] -> ['$PATH_BKP/$NAMEFILE.100']."
				mv -vf $NAMEFILE $PATH_BKP/$NAMEFILE.100
			else
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:O diretorio ['$PATH_BKP']:['$COUNTBKP'] contem menos de 100 arquivos."
                COUNTBKP=`echo $COUNTBKP | tr -d " "`
				echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Renomeamos ['$NAMEFILE'] -> ['$PATH_BKP/$NAMEFILE.$COUNTBKP']."
				mv -vf $NAMEFILE $PATH_BKP/$NAMEFILE.$COUNTBKP
			fi
		fi
	else
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:O diretorio ['$PATH_BKP'] nao existe, sera criado."
		cd $PATH_USER
		mkdir backup
		echo "$(date +%Y%m%d-%H%M%S.%s):CHECK_FILES:Renomeamos ['$NAMEFILE'] -> ['$PATH_BKP/$NAMEFILE.0']."
		mv -vf $NAMEFILE $PATH_BKP/$NAMEFILE.0
	fi
	cd $PATH_USER
}

function query_items(){

	echo	"SELECT 
				MAEARTI1.Codi, 
				MAEARTI1.Desc, 
				printf('%.2f', MAEARTI1.Pvp_0_) AS PVP_Tarifa, 
				printf('%.2f', MAEARTI1.Pvp_4_) AS PVP_NFidelizado, 
				printf('%.2f', MAEARTI1.Pvp_2_) AS PVP_Fidelizado, 
				Section.Description AS Secao, 
				Family.Description AS Familia, 
				SubFamily.Description AS SubFamilia, 
				MAEARTI1.TipoTrat, 
				(ACUARTI1.StocActuUnid - ACUARTI2.AcumUnid) AS StockActUnid,
                (ACUARTI1.StocActuKilo - ACUARTI2.AcumKilo) AS StockActKilo
			FROM 
				(MAEARTI1 
					INNER JOIN ACUARTI1 ON (MAEARTI1.Codi LIKE ACUARTI1.Codi) 
					INNER JOIN Family ON (MAEARTI1.TipoFami LIKE Family.FamilyID ) 
					INNER JOIN SubFamily ON (MAEARTI1.SubFami LIKE SubFamily.SubFamilyID) 
					INNER JOIN Section ON (MAEARTI1.CodiSecc LIKE Section.SectionID)
                    INNER JOIN ACUARTI2 ON (MAEARTI1.Codi LIKE ACUARTI2.Codi)
			    );" | sqlite3 $BD -noheader > $OUTQUERY
	RETURN=$?
    if [ $RETURN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):QUERY_ITEMS:Query gerada com sucesso."
        send_file
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUST_QUERY:Erro ao gerar query."
    fi
}

function ajust_query(){

	DATA_QUERY=$(date "+%d/%m/%Y - %H:%M:%S")
	echo "Shop""|""Date""|""Codi""|""Desc""|""PVP_Tarifa""|""PVP_NFidelizado""|""PVP_Fidelizado""|""Secao""|""Familia""|""SubFamilia""|""TipoTrat""|""StockActUnid""|""StockActKilo" > $OUTFILE
	IFS="|"
	while read Codi Desc PVP_Tarifa PVP_NFidelizado PVP_Fidelizado Secao Familia SubFamilia TipoTrat StockActUnid StockActKilo;
	do
		if [ $TipoTrat -eq 0 ]; then
			echo "$SHOP""|""$DATA_QUERY""|""$Codi""|""$Desc""|""$PVP_Tarifa""|""$PVP_NFidelizado""|""$PVP_Fidelizado""|""$Secao""|""$Familia""|""$SubFamilia""|""$TipoTrat""|""$StockActUnid""|""0" >> $OUTFILE
		elif [ $TipoTrat -eq 1 ]; then
			echo "$SHOP""|""$DATA_QUERY""|""$Codi""|""$Desc""|""$PVP_Tarifa""|""$PVP_NFidelizado""|""$PVP_Fidelizado""|""$Secao""|""$Familia""|""$SubFamilia""|""$TipoTrat""|""0""|""$StockActKilo" >> $OUTFILE
		fi
	done < $OUTQUERY
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUST_QUERY:Query ajustada com sucesso."
        send_file
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUST_QUERY:Erro ao ajustar query."
    fi

}

function send_file(){

	HOST_FTP="10.106.68.149"
    USER_FTP="ftpciss"
    PASSWD_FTP="diabrasil"
    FILE_SEND_FTP=list_stock_$SHOP.csv

    cd $PATH_USER
    cp -vf $NAMEFILE $FILE_SEND_FTP

    ftp -n $HOST_FTP <<END_SCRIPT
    quote user $USER_FTP 
    quote PASS $PASSWD_FTP
    cd STOCKONLINE
    put $FILE_SEND_FTP
    quit
END_SCRIPT
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Arquivo enviado com sucesso."
        rm -vf $FILE_SEND_FTP
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):SEND_FILE:Erro ao enviar arquivo."
    fi

}

main