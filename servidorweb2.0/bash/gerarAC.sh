#!/bin/bash
#
# Script para gerar a chaveAC SAT
# Criado por: Walter Moura (wam001br)
# Data Criação: 28-12-2019
# Data Modificação: 28-12-2019
# Versão: 0.01

ARQCNPJ="/var/www/html/resorces/bash/.cnpjs.txt"
BIN="/var/www/html/resorces/bash/.signAC.bin"
ACSAT="/var/www/html/resorces/bash/signAC.txt"
function main(){
    gerarAC
}

function gerarAC(){

    # Gerando o AC usando o OpenSSL
    openssl dgst -sha256 -sign ../certificate/$PEMIN -out $BIN $ARQCNPJ
    if [ -e "$BIN" ]; then
        openssl enc -base64 -in $BIN | tr -d "\n" > $ACSAT
        if [ $? -eq 0 ]; then
            echo -e "$(cat $ACSAT)"
        else
            echo -e "Error:Detectado um falha, ao gerar assinatura SAT."
        fi
    else
        echo "Arquivo bin não encontrado"
    fi
}
#exec 1>> sign.log
#exec 2>> sign.log
set -x
#cnpjContribuinte=$2
PEMIN=$1
main