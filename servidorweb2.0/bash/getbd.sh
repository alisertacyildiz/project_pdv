#!/bin/bash

ip=$1
pdv=$2
sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip '. /confdia/bin/setvari; tar -czvf backup_$(date +%Y%m%d)_$(date +%H%M%S)_L${NUMETIEN}_${NUMECAJA}.tgz /confdia/ctrdatos/ /confdia/ficcaje/' > /dev/null 2>&1
ERR=$?
if [ $ERR -eq 0 ]; then
    echo -e "Base de dados gerada com sucesso."
    #echo "Realizando a cópia da BD.\n"
    sshpass -p root scp -o ConnectTimeout=1 -P10001 root@$ip:/root/backup_$(date "+%Y%m%d")*_01.tgz ../base_de_dados/
    ERR=$?
    if [ $ERR -eq 0 ]; then
        #echo "Cópia da base realizada com sucesso.\n"
        sshpass -p root ssh -o ConnectTimeout=1 -p10001 -l root $ip 'rm -f /root/backup_*tgz'
    else
        echo "Erro ao copiar base de dados ['$ip']."
    fi
else
    echo "Erro ao gerar base de dados ['$ip']."
fi
