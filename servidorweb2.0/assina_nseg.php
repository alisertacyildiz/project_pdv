<?php

// carregando a chave privada do fabricante

$pkeyid = openssl_pkey_get_private('file:///root/fabricante.key', 'senha_chave_privada');

// acessando o arquivo contendo os numeros de serie e seguranca

$handle = fopen('/root/numeros_de_serie_e_seguranca.txt', 'r');

// lendo linha a linha o arquivo ate o final

while(($buffer = fgets($handle)) !== false) {

   // retirando o caracter de quebra de linha do final da linha

   $buffer = rtrim($buffer, "\n"); // caso o formato seja DOS retirar \r\n

   // carregando e separando em um vetor os numeros de serie e seguranca
   // formato da linha no arquivo: numero_de_serie;numero_de_seguranca

   $data = explode(';', $buffer);

   // gerando assinatura do numero de seguranca com SHA256

   openssl_sign($data[1], $signature, $pkeyid, OPENSSL_ALGO_SHA256);

   // armazenando a nova linha com a assinatura do numero de seguranca em base64

   $content .= "{$data[0]};{$data[1]};" . base64_encode($signature) . "\n";
}

// exibindo as novas linhas com as assinaturas

header('Content-type: text/plain; charset=utf-8');

echo $content;

?>
