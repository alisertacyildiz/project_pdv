<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie-edge" />
        <meta http-equiv="refresh" content="30">
        <link rel="icon" href="../img/favicon.ico" />
        <title>Suport TPVs | Admin</title>
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css//material-icons.min.css" />
        <link rel="stylesheet" href="../css/style.css" />
    </head>

    <body>
        <?php
        /*
            $xml = simplexml_load_file('xml/lshw.xml');
            // Obtendo o nome do autor do primeiro comentario da lista de comentarios
            echo strval($xml->comentarios->comentario->autor);
            echo "</br>";
            // Forma alternativa para obter autor do primeiro comentario
            echo strval($xml->comentarios[0]->comentario[0]->autor[0]);
            echo "</br>";
            // Obtendo o nome do autor do segundo comentario
            echo strval($xml->comentarios[0]->comentario[1]->autor[0]);
            echo "</br>";
            // Percorrendo os elementos "comentario" que estao dentro do primeiro elemento "comentarios"
            foreach ($xml->comentarios->comentario as $comentario) {
                echo strval($comentario->autor); // Obtem o nome do autor do comentario
                echo "</br>";
                echo strval($comentario->texto); // Obtem o texto do comentario
                echo "</br>";
            }
        */
            echo strval($xml->produtos->produto->codigo);
            echo "</br>";
            $xml = simplexml_load_file('xml/produtos.xml');
            foreach($xml->produtos->produto as $produto) {
                echo strval($produto->codigo);
                echo "</br>";
            }
        ?>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/jquery.slimscroll.min.js"></script>
        <script src="../js/script.js"></script>
        <script src="../js/feather.min.js"></script>
        <script>
            feather.replace()
        </script>
    </body>
</html>