<?php
    include_once("../security/seguranca.php");
	protegePagina();
	$id = $_SESSION['usuarioID'];
	$result_usuario = "SELECT * FROM tb_usuarios WHERE id = '$id'";
	$resultado_usuario = mysqli_query($conn, $result_usuario);
	$row_usuario = mysqli_fetch_assoc($resultado_usuario);
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie-edge" />
        <link rel="icon" href="../img/favicon.ico" />
        <title>Suport TPVs | User</title>
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css//material-icons.min.css" />
        <link rel="stylesheet" href="../css/style.css" />
    </head>

    <body>
        <?php
            date_default_timezone_set("America/Sao_Paulo");
            setlocale(LC_ALL, 'pt_BR');
            $num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
            //Obter a data atual
            $resultado_qnt_cadastros = mysqli_query($conn, $num_users);
            $row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
            
            $data['atual'] = date('Y-m-d H:i:s'); 

            //Diminuir 20 segundos 
            $data['online'] = strtotime($data['atual'] . " - 20 seconds");
            $data['online'] = date("Y-m-d H:i:s",$data['online']);
            
            //Pesquisar os ultimos usuarios online nos 20 segundo
            $result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
            
            $resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
            $row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
            
            $qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
            $qnt_perc = round((($row_qnt_visitas['online'] / $row_qnt_cadastros['cadastrado'])*100),2);
        ?>    
        <script src="../js/jquery-3.2.1.min.js"></script>    
        <script>
            //Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
            setInterval(function(){
            //Incluir e enviar o POST para o arquivo responsável em fazer contagem
            $.post("../processo/processa_vis.php", {contar: '',}, function(data){
                $('#online').text(data);
            });
            }, 10000);
        </script>
        <nav class="navbar navbar-expand-lg navbar-dark bg-mattBlackLight fixed-top">
            <button class="navbar-toggler sideMenuToggler" type="button">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="user.php">
                User - <?php echo $_SESSION['usuarioNome'];?>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons icon">
                                person
                            </i>
                            <span class="text">
                                Account
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaDadosPessoais">
                                <span data-feather="info"></span> Pefil</a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaSenha">
                                <span data-feather="lock"></span> Alterar senha</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="../security/sair.php">
                                <span data-feather="share"></span> Log Out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="wrapper d-flex">
            <div class="sideMenu bg-mattBlackLight">
                <div class="sidebar">
				<ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="user.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    home
                                </i>
                                <span class="text">
                                    Home
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="consultas.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    search
                                </i>
                                <span class="text">
                                    Consultas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="srv_chaves.php" class="nav-link px-2 active">
                                <i class="material-icons icon">
                                    dns
                                </i>
                                <span class="text">
                                    Servidor de Chaves
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link px-2 sideMenuToggler">
                                <i class="material-icons icon expandView ">
                                    view_list
                                </i>
                                <span class="text">
                                    Ocultar
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="content">

                <main>
                    <form name="frm_senha" action="../processo/salva_senha.php" method="POST">
                        <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content bg-mattBlackLight px-3 py-3">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>  
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                                                <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                                                <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-primary">Confirma</button>
                                    </div>
                                </div>
                            </div>
                        </div>
					</form>
					<form name="frm_dados_pessoais" action="../processo/atualizar_usuario.php" method="POST">
                        <div class="modal fade" id="editaDadosPessoais" tabindex="-1" role="dialog" aria-labelledby="editaDadosPessoaisLabel" aria-hidden="true">    
                            <div class="modal-dialog" role="document">          
                                <!-- Modal content-->      
                                <div class="modal-content bg-mattBlackLight">        
                                    <div class="modal-header">          
                                        <h4 class="modal-title texto-modal text-center">Dados Pessoais</h4>        
                                    </div>        
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="text">ID </label>
                                                <input for="text" class="form-control" name="id" value="<?php echo $row_usuario['id'] ?>" readonly>
                                                <label for="text">Nome </label>
                                                <input type="text" class="form-control" name="nome" value="<?php echo $row_usuario['nome']; ?>" >
                                                <label for="email">E-mail </label>
                                                <input type="email" class="form-control" name="email" value="<?php echo $row_usuario['email']; ?>">
                                                <label for="text">Login </label>
                                                <input type="text" class="form-control" name="login" value="<?php echo $row_usuario['login']; ?>" readonly>
                                            </div>
                                        </form>        
                                    </div>        
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-outline-success">Atualizar</button>        
                                    </div>      
                                </div> 
                            </div>
                        </div>
                    </form>
                    <div class="container-fluid">
                    <div class="my-5">
						</div>
						<div class="row">
							<div class="col-md-5">
								<div class="bg-mattBlackLight px-3 py-3">
                                    <h5>Consultar chave Servidor</h5>
									<form id="frm_consulta" action="srv_chaves.php" method="POST">
										<div class="input-group">
											<input type="text hidden" class="form-control" id="id_loja" name="nume_loja"  placeholder="Digite o número da loja">
											<div class="input-group-append">
												<!--
												<button class="btn btn-outline-secondary" type="button submit" id="myBtn" data-toggle="modal" data-target="#consulta_chaves" data-whatever="Resultado Loja - ">Consultar</button>-->
												<button class="btn btn-outline-success" type="submit" >Consultar</button>
											</div>
										</div>
										<?php
											include_once("../processo/consulta_chaves.php");
											get_num_loja();	
										?>
                                    </form>
                                    <div class="py-3">
                                        <button type="button" class="btn btn-md btn-outline-info" data-toggle="modal" data-target="#nova_chave_sat">Nova Chave SAT</button>
                                        <button type="button" class="btn btn-md btn-outline-info" data-toggle="modal" data-target="#nova_chave_nfce">Nova Chave NFCe</button>
                                    </div>
								</div>
                            </div>
							<div class="col-md-7">
								<div class="bg-mattBlackLight px-3 py-3">
                                    <h5>Gerar chave AC S@T</h5>
									<form id="frm_signAC" action="srv_chaves.php" method="POST">
                                        <h6 class="mb-2 py-2">Selecione o ano certificado</h6>	
                                        <select name="cb_certificate" class="custom-select">
                                            <?php
                                                $path='/var/www/html/resorces/certificate';
                                                $diretorio = dir($path);
                                                while($arquivo = $diretorio -> read()){	
                                                    if ($arquivo != '..' && $arquivo != '.'){
                                                        echo "<option value='$arquivo'>
                                                                <span data-feather='briefcase'></span>
                                                                <a style='color: white;' href='../certificate/".$arquivo."'>".$arquivo."</a>
                                                            </option>";		
                                                    }
                                                }
                                                $diretorio -> close();
                                            ?>
                                        </select>
										<div class="input-group py-3">
											<input type="text hiden"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control" id="id_contribuinte" name="cnpj_contribuinte" maxlength="14" placeholder="Digite o o CNPJ da loja">
											<div class="input-group-append">
												<button class="btn btn-outline-success" type="submit" >Assinar</button>
											</div>
										</div>
										<?php
                                            include_once("../processo_bash/gerar_ac_sat.php");
                                            get_ac();
										?>
									</form>
                                </div>
							</div>
						</div>
						<div class="my-5">
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="bg-mattBlackLight px-3 py-3">
									<div class="table-responsive">
										<table class="table table-sm table-striped table-bordered table-hover" id="tb_sat">				
											<thead>
												<tr>
												<th>#</th>
												<th>SAT</th>
												</tr>
											</thead>
												<?php
														echo "<tbody>";
														$path='/var/www/html/resorces/srv_chaves/arquivos/claves_sat';
														$diretorio = dir($path);
															while($arquivo = $diretorio -> read()) 										
																{	
																	if ($arquivo != '..' && $arquivo != '.'){
																			echo "<tr>";
																			echo "<td><center><span data-feather='briefcase'></span></center></td>";
																			echo "<td><a style='color: white;' href='../srv_chaves/arquivos/claves_sat/".$arquivo."'>".$arquivo."</a></td>";
																			echo "</tr>";		
																		}
																}
																$diretorio -> close();
														echo "</tbody>";
												?>
										</table>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="bg-mattBlackLight px-3 py-3">
									<div class="table-responsive">
										<table class="table table-sm table-striped table-bordered table-hover" id="tb_nfce">
											<thead>
												<tr>
													<th>#</th>
													<th>NFCe</th>
												</tr>
											</thead>
												<?php
													echo "<tbody>";
													$path2='/var/www/html/resorces/srv_chaves/arquivos/claves_nfce';
													$diretorio2 = dir($path2);
														while($arquivo2 = $diretorio2 -> read()) 										
															{	
																if ($arquivo2 != '..' && $arquivo2 != '.'){
																		echo "<tr>";
																		echo "<td><center><span data-feather='briefcase'></span></center></td>";
																		echo "<td><a style='color: white;' href='../srv_chaves/arquivos/claves_nfce/".$arquivo2."'>".$arquivo2."</a></td>";
																		echo "</tr>";		
																	}
															}
															$diretorio2 -> close();
													echo "</tbody>";
												?>
											</table>
										</div>
									</div>
								</div>
							</div>
						</main>
					</div>
				</div>
				<form name="frm_nova_chave" action="../processo/nova_chave.php" method="POST">
					<div class="modal fade" id="nova_chave_sat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content bg-mattBlackLight px-3 py-3">
							<div class="modal-header">
								<h4 class="modal-title" id="exampleModalLabel">Gerar Chaves</h4>
							</div>
							<div class="modal-body">
								<form>
								<div class="form-group">
									<label for="recipient-name" class="control-label">Loja:</label>
									<input type="text" class="form-control" name="loja">
								</div>
								<div class="form-group">
									<label for="recipient-name" class="control-label">CFe-Key:</label>
									<input type="text" class="form-control" name="cfe-key">
								</div>
								<div class="form-group">
									<label for="message-text" class="control-label">AC-SAT</label>
										<textarea type="text" class="form-control" name="ac-sat"></textarea>
								</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-outline-danger" data-dismiss="modal">Fechar</button>
								<button type="submit" class="btn btn-outline-success">Enviar</button>
							</div>
							</div>
						</div>
					</div>
			</form>
				<form name="frm_nova_chave_nfce" action="../processo/nova_chave_nfce.php" method="POST">
					<div class="modal fade" id="nova_chave_nfce" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content bg-mattBlackLight px-3 py-3">
							<div class="modal-header">
								<h4 class="modal-title" id="exampleModalLabel">Gerar Chaves</h4>								
							</div>
							<div class="modal-body">
								<form>
								<div class="form-group">
									<label for="recipient-name" class="control-label">Loja:</label>
									<input type="text" class="form-control" name="loja">
								</div>
								<div class="form-group">
									<label for="recipient-name" class="control-label">CFe-Key:</label>
									<input type="text" class="form-control" name="cfe-key">
								</div>
								<div class="form-group">
									<label for="recipient-name" class="control-label">ID:</label>
									<select name="id" class="custom-select navbar navbar-grey sticky-top bg-grey ">
										<option value="000001">000001</option>
										<option value="000002">000002</option>
										<option value="000003">000003</option>
										<option value="000004">000004</option> 
									</select>        
								</div>
								<div class="form-group">
									<label for="message-text" class="control-label">Token:</label>
										<input type="text" class="form-control" name="token">
								</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-outline-danger" data-dismiss="modal">Fechar</button>
								<button type="submit" class="btn btn-outline-success">Enviar</button>
							</div>
							</div>
						</div>
						</div>
					</form>

					
					<div class="modal fade" id="consulta_chaves" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content bg-mattBlackLight px-3 py-3">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Resultado Chaves </h5>
								</div>
								<div class="modal-body">
									<form>
										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Loja</label>
											<input type="text" class="form-control" id="recipient-name">
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
								</div>
							</div>
						</div>
					</div>
		<script type="text/JavaScript">

			$('#consulta_chaves').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget)
				var recipient = button.data('whatever')
				var num_loja = document.getElementById('id_loja').value;
				var modal = $(this)

				//modal.find('.modal-title').text(recipient + num_loja)
				modal.find('.modal-body input').val(num_loja)
			})

		</script>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/jquery.slimscroll.min.js"></script>
        <script src="../js/script.js"></script>
        <script src="../js/feather.min.js"></script>
        <script>
            feather.replace()
        </script>
    </body>
</html>
