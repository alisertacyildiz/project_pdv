<?php
    include_once("../security/seguranca.php");
    protegePagina();
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="../img/favicon.ico">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
	
	 <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dashboard.css" rel="stylesheet">

    <script src="../js/ie-emulation-modes-warning.js"></script>
	
    <title>Support TPVs | Atualizar</title>
  </head>
  <body>
  <?php
        include_once("../security/conecta.php");
        $id = $_SESSION['usuarioID'];
        $nivel = $_SESSION['usuarioNivel'];
        $nome = (isset($_POST['nome'])) ? $_POST['nome'] : '';
        $email = (isset($_POST['email'])) ? $_POST['email'] : '';
        $login = (isset($_POST['login'])) ? $_POST['login'] : ''; 
        $data = date('Y-m-d H:i:s'); 
            
        if(!empty($id)){

            $result_usuario = "UPDATE tb_usuarios SET nome='$nome',login='$login',email='$email',data_modificacao='$data' WHERE id='$id'";
            $resultado_usuario = mysqli_query($conn, $result_usuario);
            if(mysqli_affected_rows($conn)){
                if($nivel == 1){
                    echo "<script>alert('Cadastro atualizado com sucesso!');</script>";
                    echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../admin/admin.php">';    
                }elseif($nivel == 2){
                    echo "<script>alert('Cadastro atualizado com sucesso!');</script>";
                    echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user2/user2.php">';
                }elseif($nivel == 3){
                    echo "<script>alert('Cadastro atualizado com sucesso!');</script>";
                    echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user/user.php">';
                }elseif($nivel == 4){
                    echo "<script>alert('Cadastro atualizado com sucesso!');</script>";
                    echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user3/user.php">';
                }
            }else{
                if($nivel == 1){
                    echo "<script>alert('Erro ao atualizar dados!');</script>";
                    echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../admin/admin.php">';    
                }elseif($nivel == 2){
                    echo "<script>alert('Erro ao atualizar dados!');</script>";
                    echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user2/user2.php">';
                }elseif($nivel == 3){
                    echo "<script>alert('Erro ao atualizar dados!');</script>";
                    echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user/user.php">';
                }elseif($nivel == 4){
                    echo "<script>alert('Erro ao atualizar dados!');</script>";
                    echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user3/user.php">';
                }
            }
        }else{
            if($nivel == 1){
                echo "<script>alert('Necessário selecionar um usuário!');</script>";
                echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../admin/admin.php">';    
            }elseif($nivel == 2){
                echo "<script>alert('Necessário selecionar um usuário!');</script>";
                echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user2/user2.php">';
            }elseif($nivel == 3){
                echo "<script>alert('Necessário selecionar um usuário!');</script>";
                echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user/user.php">';
            }elseif($nivel == 4){
                echo "<script>alert('Necessário selecionar um usuário!');</script>";
                echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user3/user.php">';
            }
        }

    ?>
  </body>
</html>