<?php
    require_once("../security/seguranca.php");
	protegePagina();
	
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://www.dia.com.br/static/favicon/favicon-16x16.png">

    <title>Support TPVs</title>

<?php

	include_once("../security/conecta.php");
	$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
	$nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
	$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
	$login = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING); 
	$nivel = filter_input(INPUT_POST, 'nivel', FILTER_SANITIZE_STRING);
	$data = date('Y-m-d H:i:s'); 
		
	if(!empty($id)){

		$result_usuario = "UPDATE tb_usuarios SET nome='$nome',login='$login',email='$email',nivel='$nivel',data_modificacao='$data' WHERE id='$id'";
		$resultado_usuario = mysqli_query($conn, $result_usuario);
		if(mysqli_affected_rows($conn)){
			echo "<script>alert('Cadastro atualizado com sucesso!');</script>";
			header("Location: ../admin/usuarios.php");
		}else{
			
			$_SESSION['msg'] = "<p style='color:red;'>Erro o usuário não foi apagado com sucesso</p>";
			header("Location: ../admin/usuarios.php");
		}
	}else{	
		$_SESSION['msg'] = "<p style='color:red;'>Necessário selecionar um usuário</p>";
		header("Location: ../admin/usuarios.php");
	}

?>

</html>