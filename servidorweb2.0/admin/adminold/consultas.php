<?php
    include_once("../security/seguranca.php");
	protegePagina();
?>

<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicon.ico">

    <title>Support TPVs | SATs </title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../dashboard.css" rel="stylesheet">
	<!--<link href="theme.css" rel="stylesheet">-->
	
	<script src="../js/ie-emulation-modes-warning.js"></script>
	
  
  </head>

  <body>
  <?php

			date_default_timezone_set("America/Sao_Paulo");
      		setlocale(LC_ALL, 'pt_BR');
			$num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
			//Obter a data atual
			$resultado_qnt_cadastros = mysqli_query($conn, $num_users);
			$row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
			
			$data['atual'] = date('Y-m-d H:i:s');	

			//Diminuir 20 segundos 
			$data['online'] = strtotime($data['atual'] . " - 20 seconds");
			$data['online'] = date("Y-m-d H:i:s",$data['online']);
			
			//Pesquisar os ultimos usuarios online nos 20 segundo
			$result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
			
			$resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
			$row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
			
			$qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
	?>

  <script src="../js/jquery-3.2.1.min.js"></script>
		
		<script>
		//Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
		setInterval(function(){
			//Incluir e enviar o POST para o arquivo responsável em fazer contagem
			$.post("../processo/processa_vis.php", {contar: '',}, function(data){
				$('#online').text(data);
			});
		}, 10000);
		</script>
		
  <form name="consulta" action="consultas.php" method="POST">
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="admin.php">Administrador -
		<?php  
		  echo $_SESSION['usuarioNome'];
		  ?>
	  </a>
		<select name="cb_consulta" class="custom-select navbar navbar-grey sticky-top bg-grey ">
			<option value="vazio"></option>
			<option value="loja"> Loja</option>
			<option value="sat"> SAT</option>  
		</select>        
      <input class="form-control form-control-dark w-100" name="txt_consulta"  type="text"  placeholder="Pesquisar: Loja/SAT"  aria-label="search" >
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="../security/sair.php"><span data-feather="share"></span>
          Sair</a>
        </li>
      </ul>
    </nav>
	<!--
    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                
          </div>
        </nav>
		-->
		
	</form>
        <main role="main" class="col-md-20 col-lg-20 pt-20 px-20">
		

        <!--<h2>Consultas</h2>-->
        <br>

<?php
if(empty($_POST['cb_consulta'])){
	
}else{
			$metodo_consulta = $_POST['cb_consulta'];
		if ( $metodo_consulta == "loja")
			{
				echo '<div class="page-header">';
				echo "<div class='row'>";
				echo "<div class='col-sm-6'>";
				echo '<table class="table table-striped table-bordered table-hover">';
				echo '<thead>';
				echo "<tr>";
				echo "<th>Loja</th>";
				echo "<th>QNTD PDVs</th>";
				echo "<th>Versao</th>";
				echo "<th>IP</th>";
				echo "</tr>";
																					
					$n_loja = $_POST['txt_consulta'];
					$nu_loja = str_pad($n_loja,5,'0', STR_PAD_LEFT);

						 // Conectando ao banco de dados:
						 $strconn = mysqli_connect('localhost','root','diabrasil','srvremoto') or die('Erro ao conectar ao banco de dados');
						 $sql = "SELECT Loja, Qntd_Tpvs, Versao, IP FROM tb_dados_loja WHERE Loja ='$nu_loja'";
						 $result = mysqli_query($strconn,$sql) or die("Erro ao retornar dados");
						 // Obtendo os dados por meio de um loop while
						 echo "<tbody>";
						 while ($reg = mysqli_fetch_array($result))
						 {
							$loja = $reg['Loja'];
							$qntd_tpvs = $reg['Qntd_Tpvs'];
							$versao = $reg['Versao'];
							$ip = $reg['IP'];
								echo "<center>";
								echo "<tr>";
								echo "<td>".$loja."</td>";
								echo "<td>".$qntd_tpvs."</td>";
								echo "<td>".$versao."</td>";
								echo "<td>".$ip."</td>";
								echo "</tr>";
								echo "</center>";
								
							 }
							 echo "</tbody>";	
							 mysqli_close($strconn);
							 echo "</thead>";
							 echo "</table>";
							 echo "</div>";
							 echo "</div>";
							 echo "</div>";
				
				
				  //Criando tabela e cabeçalho de dados:
				 echo '<div class="page-header">';
				 echo "<div class='row'>";
				 echo "<div class='col-sm-11'>";
				 echo '<table class="table table-striped table-bordered table-hover">';
				 echo '<thead>';
				 echo "<tr>";
				 echo "<th>SAT</th>";
				 echo "<th>Loja</th>";
				 echo "<th>Caixa</th>";
				 echo "<th>IP</th>";
				 echo "<th>MASK</th>";
				 echo "<th>GW</th>";
				 echo "<th>DNS 1</th>";
				 echo "<th>DNS 2</th>";
				 echo "<th>Firmware</th>";
				 echo "<th>Layout</th>";
				 echo "<th>Usado</th>";
				 echo "<th>Ativacao</th>";
				 echo "<th>Fim Ativacao</th>";
				 echo "</tr>";

				$nume_loja = $_POST['txt_consulta'];
				$num_loja = str_pad($nume_loja,5,'0', STR_PAD_LEFT);
				 // Conectando ao banco de dados:
				 $strcon = mysqli_connect('localhost','root','diabrasil','srvremoto') or die('Erro ao conectar ao banco de dados');
				 $sql = "SELECT sat, loja, caixa, ip, mask, gw, dns_1, dns_2, firmware, layout, disco, disco_usado, data_ativacao, data_fim_ativacao, status FROM tb_sat WHERE loja ='$num_loja' AND status='Ativo' ORDER BY caixa";
				 $resultado = mysqli_query($strcon,$sql) or die("Erro ao retornar dados");
				 // Obtendo os dados por meio de um loop while

					echo "<tbody>";
					$i=1;
					while ($registro = mysqli_fetch_array($resultado))
					{
						if($i <= $qntd_tpvs){
							$sat = $registro['sat'];
							$loja = $registro['loja'];
							$caixa = $registro['caixa'];
							$ip = $registro['ip'];
							$mascara = $registro['mask'];
							$geteway = $registro['gw'];
							$dns_1 = $registro['dns_1'];
							$dns_2 = $registro['dns_2'];
							$firmware = $registro['firmware'];
							$layout = $registro['layout'];
							$disco = $registro['disco'];
							$disco_usado = $registro['disco_usado'];
							$data_ativacao = $registro['data_ativacao'];
							$data_fim_ativacao = $registro['data_fim_ativacao'];
							$status = $registro['status'];
							echo "<tr>";
							echo "<td>".$sat."</td>";
							echo "<td>".$loja."</td>";
							echo "<td>".$caixa."</td>";
							echo "<td>".$ip."</td>";
							echo "<td>".$mascara."</td>";
							echo "<td>".$geteway."</td>";
							echo "<td>".$dns_1."</td>";
							echo "<td>".$dns_2."</td>";
							echo "<td>".$firmware."</td>";
							echo "<td>".$layout."</td>";
							echo "<td>".$disco_usado."</td>";
							echo "<td>".$data_ativacao."</td>";
							echo "<td>".$data_fim_ativacao."</td>";
							echo "</tr>";
						}
						$i++;
						
					}
						 echo "</tbody>";
						 echo "</thead>";
						 mysqli_close($strcon);
						 echo "</table>";
						 echo "</div>";
						 echo "</div>";
						 echo "</div>";
																						 
		//
				echo '<div class="page-header">';
				echo "<div class='row'>";
				echo "<div class='col-sm-5' style='float:left;'>";
				echo '<table class="table table-striped table-bordered table-hover">';
				echo '<thead>';
				echo "<tr>";
				echo "<th>Loja</th>";
				echo "<th>PDV</th>";
				echo "<th>Impressora</th>";
				echo "<th>Firmware</th>";
				echo "<th>CPU</th>";
				echo "</tr>";
																					
				$n_loja = $_POST['txt_consulta'];
				$nu_loja = str_pad($nume_loja,5,'0', STR_PAD_LEFT);

					 // Conectando ao banco de dados:
					 $strconn = mysqli_connect('localhost','root','diabrasil','srvremoto') or die('Erro ao conectar ao banco de dados');
					 $sql = "SELECT Loja, PDV, Impressora, Firmware,CPU FROM tb_consolidado_equipamentos WHERE Loja ='$nu_loja' ORDER BY PDV";
					 $result = mysqli_query($strconn,$sql) or die("Erro ao retornar dados");
					 // Obtendo os dados por meio de um loop while
					 echo "<tbody>";
					 $i=1;
					 while ($reg = mysqli_fetch_array($result))
						 {
							if($i <= $qntd_tpvs){
								$loja = $reg['Loja'];
								$pdv = $reg['PDV'];
								$imp = $reg['Impressora'];
								$frw = $reg['Firmware'];
								$cpu = $reg['CPU'];
								
								echo "<tr>";
								echo "<td>".$loja."</td>";
								echo "<td>".$pdv."</td>";
								echo "<td>".$imp."</td>";
								echo "<td>".$frw."</td>";
								echo "<td>".$cpu."</td>";
								echo "</tr>";
							}
							$i++;
						 }	
							 echo "</tbody>";
							 mysqli_close($strconn);
							 echo "</thead>";
							 echo "</table>";
							 echo "</div>";
						
							 echo "<div class='col-sm-6' style='float:left;'>";
							 echo '<table class="table table-striped table-bordered table-hover">';
							 echo '<thead>';
							 echo "<tr>";
							 echo "<th>Loja</th>";
							 echo "<th>Caixa</th>";
							 echo "<th>Cod Empresa</th>";
							 echo "<th>Cod Filial</th>";
							 echo "<th>Cod PDV</th>";
							 echo "</tr>";

						$n_loja = $_POST['txt_consulta'];
						$nu_loja = str_pad($nume_loja,5,'0', STR_PAD_LEFT);

							 // Conectando ao banco de dados:
							 $strconn = mysqli_connect('localhost','root','diabrasil','srvremoto') or die('Erro ao conectar ao banco de dados');
							 $sql = "SELECT Loja, Caixa, Cod_Empresa, Filial, Cod_Pdv FROM tb_dados_tef WHERE Loja ='$nu_loja'";
							 $result = mysqli_query($strconn,$sql) or die("Erro ao retornar dados");
							 // Obtendo os dados por meio de um loop while
							 echo "<tbody>";
							 $i=1;
							 while ($reg = mysqli_fetch_array($result))
							 {
								if($i <= $qntd_tpvs){
										$loja = $reg['Loja'];
										$caixa = $reg['Caixa'];
										$cod_empresa = $reg['Cod_Empresa'];
										$cod_filial = $reg['Filial'];
										$cod_pdv = $reg['Cod_Pdv'];
											
										echo "<tr>";
										echo "<td>".$loja."</td>";
										echo "<td>".$caixa."</td>";
										echo "<td>".$cod_empresa."</td>";
										echo "<td>".$cod_filial."</td>";
										echo "<td>".$cod_pdv."</td>";
										echo "</tr>";
								}
								$i++;
							}	
								 echo "</tbody>";
								 echo "</thead>";
								 mysqli_close($strconn);
								 echo "</table>";
								 echo "</div>";
								 echo "</div>";
								 echo "</div>";
							 
			}else{
				$metodo_consulta = $_POST['cb_consulta'];
				
				if( $metodo_consulta == "sat")
					
					{
																				
				  //Criando tabela e cabeçalho de dados:
						 echo '<div class="page-header">';
						 echo '<table class="table table-striped table-sm">';
						 echo '</div>';
						 echo '<div class="row">';
						 echo '<table class="table">';
						 echo '<thead>';
						 echo "<tr>";
						 echo "<th>SAT</th>";
						 echo "<th>Loja</th>";
						 echo "<th>Caixa</th>";
						 echo "<th>IP</th>";
						 echo "<th>MASK</th>";
						 echo "<th>GW</th>";
						 echo "<th>DNS 1</th>";
						 echo "<th>DNS 2</th>";
						 echo "<th>Firmware</th>";
						 echo "<th>Layout</th>";
						 echo "<th>Usado</th>";
						 echo "<th>Ativacao</th>";
						 echo "<th>Fim Ativacao</th>";
						 echo "</tr>";
						 echo "</thead>";	 
								$nume_sat = $_POST['txt_consulta'];
								$num_sat = str_pad($nume_sat,9,'0', STR_PAD_LEFT);
								 // Conectando ao banco de dados:
								 $strcon = mysqli_connect('localhost','root','diabrasil','srvremoto') or die('Erro ao conectar ao banco de dados');
								 $sql = "SELECT sat, loja, caixa, ip, mask, gw, dns_1, dns_2, firmware, layout, disco, disco_usado, data_ativacao, data_fim_ativacao, status FROM tb_sat WHERE sat ='$num_sat' AND Status = 'Ativo' ORDER BY loja ";
								 $resultado = mysqli_query($strcon,$sql) or die("Erro ao retornar dados");
								 // Obtendo os dados por meio de um loop while
								 while ($registro = mysqli_fetch_array($resultado))
								 {
									$sat = $registro['sat'];
									$loja = $registro['loja'];
									$caixa = $registro['caixa'];
									$ip = $registro['ip'];
									$mascara = $registro['mask'];
									$geteway = $registro['gw'];
									$dns_1 = $registro['dns_1'];
									$dns_2 = $registro['dns_2'];
									$firmware = $registro['firmware'];
									$layout = $registro['layout'];
									$disco = $registro['disco'];
									$disco_usado = $registro['disco_usado'];
									$data_ativacao = $registro['data_ativacao'];
									$data_fim_ativacao = $registro['data_fim_ativacao'];
									$status = $registro['status'];
									echo "<tbody>";
									echo "<tr>";
									echo "<td>".$sat."</td>";
									echo "<td>".$loja."</td>";
									echo "<td>".$caixa."</td>";
									echo "<td>".$ip."</td>";
									echo "<td>".$mascara."</td>";
									echo "<td>".$geteway."</td>";
									echo "<td>".$dns_1."</td>";
									echo "<td>".$dns_2."</td>";
									echo "<td>".$firmware."</td>";
									echo "<td>".$layout."</td>";
									echo "<td>".$disco_usado."</td>";
									echo "<td>".$data_ativacao."</td>";
									echo "<td>".$data_fim_ativacao."</td>";
									echo "</tr>";
									echo "</tbody>";
									}
									 mysqli_close($strcon);
									 echo "</table>";
									 echo "</div>";
					 }
			}
	}	
?>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-slim.min.js"><\/script>')</script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="../js/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

    <!-- Graphs 
    <script src="js/Chart.min.js"></script>
    <script>
      var ctx = document.getElementById("myChart");
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
          datasets: [{
            data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
            lineTension: 0,
            backgroundColor: 'transparent',
            borderColor: '#007bff',
            borderWidth: 4,
            pointBackgroundColor: '#007bff'
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: false
              }
            }]
          },
          legend: {
            display: false,
          }
        }
      });
    </script>
	-->
  </body>
</html>
