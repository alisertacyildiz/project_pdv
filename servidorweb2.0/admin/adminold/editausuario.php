<?php
    require_once("../security/seguranca.php");
	protegePagina();
	include_once("../security/conecta.php");
	$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
	$result_usuario = "SELECT * FROM tb_usuarios WHERE id = '$id'";
	$resultado_usuario = mysqli_query($conn, $result_usuario);
	$row_usuario = mysqli_fetch_assoc($resultado_usuario);
?>

<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicon.ico">
	
    <title>Support TPVs | Edit User</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dashboard.css" rel="stylesheet">
	
		
  </head>

  <body>
  <?php
      date_default_timezone_set("America/Sao_Paulo");
      setlocale(LC_ALL, 'pt_BR');
      $num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
      //Obter a data atual
      $resultado_qnt_cadastros = mysqli_query($conn, $num_users);
      $row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
      
      $data['atual'] = date('Y-m-d H:i:s'); 

      //Diminuir 20 segundos 
      $data['online'] = strtotime($data['atual'] . " - 20 seconds");
      $data['online'] = date("Y-m-d H:i:s",$data['online']);
      
      //Pesquisar os ultimos usuarios online nos 20 segundo
      $result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
      
      $resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
      $row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
      
      $qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
    ?>
  <script src="../js/jquery-3.2.1.min.js"></script>
		
		<script>
		//Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
		setInterval(function(){
			//Incluir e enviar o POST para o arquivo responsável em fazer contagem
			$.post("../processo/processa_vis.php", {contar: '',}, function(data){
				$('#online').text(data);
			});
		}, 10000);
		</script>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="admin.php">Admistrativo -
	  	<?php  
		    echo $_SESSION['usuarioNome'];
		  ?>
    </a>
    <ul class="navbar-nav px-3">
      <li class="nav-item text-nowrap">
        <a class="nav-link" href="../security/sair.php"><span data-feather="share"></span>
          Sair
        </a>
      </li>
    </ul>
  </nav>
  <div class="container-fluid">
    <div class="row">
      <nav class="col-sm-2 d-none d-sm-block bg-light sidebar">
        <div class="sidebar-sticky">
          <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link" href="admin.php">
                <span data-feather="home"></span>
                  Home<span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="consultas.php">
                <span data-feather="search"></span>
                Consultas
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="sats.php?pagina=1">
                <span data-feather="camera"></span>
                SATs
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="list_lojas.php?pagina=1">
                <span data-feather="list"></span>
                Cadastro de Lojas
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="usuarios.php">
                <span data-feather="users"></span>
                Usuarios<span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#?" data-toggle="modal" data-target="#editaSenha">
                <span data-feather="lock"></span>
                Alterar Senha
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="scripts.php">
                <span data-feather="file-text"></span>
                Scripts
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="nivel.php">
                <span data-feather="layers"></span>
                Niveis de Acesso
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="srv_chaves.php">
                <span data-feather="server"></span>
                Servidor de Chaves
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tabelas2.php">
                <span data-feather="grid"></span>
                Tabelas
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="control.php">
                <span data-feather="settings"></span>
                Painel de Controle
              </a>
            </li>
          </ul>       
        </div>
      </nav>
    </div>
  </div>
	<?php
		if(isset($_SESSION['msg'])){
			echo $_SESSION['msg'];
			unset($_SESSION['msg']);
		}
	?>	
	<form name="editar" action="../processo/editar_user.php" method="POST">
	<main role="main" class="col-md-1 ml-sm-auto col-lg-10 pt-3 px-4">
    <h4>Novo Usuario</h4>
	</main>
	<nav class="col-md-1 ml-sm-auto col-lg-10 pt-3 px-4">
	<div class="page-header">
	<label for="text">ID </label>
			<input for="text" class="form-control" name="id" value="<?php echo $row_usuario['id'] ?>">
	<label for="text">Nome </label>
              <input type="text" class="form-control" name="nome" value="<?php echo $row_usuario['nome']; ?>" >
	<label for="email">E-mail </label>
              <input type="email" class="form-control" name="email" value="<?php echo $row_usuario['email']; ?>">
	<label for="text">Login </label>
              <input type="text" class="form-control" name="login" value="<?php echo $row_usuario['login']; ?>">
  <label for="text">Nível de acesso </label>   
			  <input type="text" class="form-control" name="nivel" value="<?php echo $row_usuario['nivel']; ?>">
	</div>      
	<br>
	<nav class="col-md-9 ml-sm-auto col-lg-1 pt-6 px-4">
		<a href="">
		  <button type="submit" class="btn btn-xs btn-primary">Salvar</button>
		</a>
		</nav>
     	
	</nav>
	</form>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-slim.min.js"><\/script>')</script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="../js/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

    <!-- Graphs 
    <script src="js/Chart.min.js"></script>
    <script>
      var ctx = document.getElementById("myChart");
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
          datasets: [{
            data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
            lineTension: 0,
            backgroundColor: 'transparent',
            borderColor: '#007bff',
            borderWidth: 4,
            pointBackgroundColor: '#007bff'
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: false
              }
            }]
          },
          legend: {
            display: false,
          }
        }
      });
    </script>
	-->
  </body>
</html>
