<?php
    include_once("../security/seguranca.php");
    protegePagina();
    include_once("../security/conecta.php");
	$id = $_SESSION['usuarioID'];
	$result_usuario = "SELECT * FROM tb_usuarios WHERE id = '$id'";
	$resultado_usuario = mysqli_query($conn, $result_usuario);
	$row_usuario = mysqli_fetch_assoc($resultado_usuario);
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie-edge" />
        <link rel="icon" href="../img/favicon.ico" />
        <title>Suport TPVs | Admin</title>
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css//material-icons.min.css" />
        <link rel="stylesheet" href="../css/style.css" />
    </head>

    <body>
        <?php
            date_default_timezone_set("America/Sao_Paulo");
            setlocale(LC_ALL, 'pt_BR');
            $num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
            //Obter a data atual
            $resultado_qnt_cadastros = mysqli_query($conn, $num_users);
            $row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
            
            $data['atual'] = date('Y-m-d H:i:s'); 

            //Diminuir 20 segundos 
            $data['online'] = strtotime($data['atual'] . " - 20 seconds");
            $data['online'] = date("Y-m-d H:i:s",$data['online']);
            
            //Pesquisar os ultimos usuarios online nos 20 segundo
            $result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
            
            $resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
            $row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
            
            $qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
            $qnt_perc = round((($row_qnt_visitas['online'] / $row_qnt_cadastros['cadastrado'])*100),2);
        ?>    
        <script src="../js/jquery-3.2.1.min.js"></script>    
        <script>
            //Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
            setInterval(function(){
            //Incluir e enviar o POST para o arquivo responsável em fazer contagem
            $.post("../processo/processa_vis.php", {contar: '',}, function(data){
                $('#online').text(data);
            });
            }, 10000);
        </script>
        <nav class="navbar navbar-expand-lg navbar-dark bg-mattBlackLight fixed-top">
            <button class="navbar-toggler sideMenuToggler" type="button">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="admin.php">
                Admin - <?php echo $_SESSION['usuarioNome'];?>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons icon">
                                person
                            </i>
                            <span class="text">
                                Account
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaDadosPessoais">
                                <span data-feather="info"></span> Pefil</a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaSenha">
                                <span data-feather="lock"></span> Alterar senha</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="../security/sair.php">
                                <span data-feather="share"></span> Log Out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="wrapper d-flex">
            <div class="sideMenu bg-mattBlackLight">
                <div class="sidebar">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="admin.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    home
                                </i>
                                <span class="text">
                                    Home
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="consultas.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    search
                                </i>
                                <span class="text">
                                    Consultas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="srv_chaves.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    dns
                                </i>
                                <span class="text">
                                    Servidor de Chaves
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="sats.php?pagina=1" class="nav-link px-2">
                                <i class="material-icons icon">
                                    camera_alt
                                </i>
                                <span class="text">
                                    SATs
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="list_lojas.php?pagina=1" class="nav-link px-2">
                                <i class="material-icons icon">
                                    pages
                                </i>
                                <span class="text">
                                    Cadastro de Lojas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="usuarios.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    supervisor_account
                                </i>
                                <span class="text">
                                    Usuários
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="niveis.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    layers
                                </i>
                                <span class="text">
                                    Níveis
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="tabelas.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    dashboard
                                </i>
                                <span class="text">
                                    Tabelas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="divergencia.php" class="nav-link px-2 active">
                                <i class="material-icons icon">
                                    bug_report
                                </i>
                                <span class="text">
                                    Divergentes
                                </span>
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a href="jobs.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    android
                                </i>
                                <span class="text">
                                    Robos
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="control.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    settings
                                </i>
                                <span class="text">
                                    Settings
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="scripts.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    description
                                </i>
                                <span class="text">
                                    Scripts
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link px-2 sideMenuToggler">
                                <i class="material-icons icon expandView ">
                                    view_list
                                </i>
                                <span class="text">
                                    Ocultar
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="content">
                <main>
                    <form name="frm_senha" action="../processo/salva_senha.php" method="POST">
                        <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content bg-mattBlackLight px-3 py-3">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>  
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                                                <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                                                <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-outline-primary">Confirma</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form name="frm_dados_pessoais" action="../processo/atualizar_usuario.php" method="POST">
                        <div class="modal fade" id="editaDadosPessoais" tabindex="-1" role="dialog" aria-labelledby="editaDadosPessoaisLabel" aria-hidden="true">    
                            <div class="modal-dialog" role="document">          
                                <!-- Modal content-->      
                                <div class="modal-content bg-mattBlackLight">        
                                    <div class="modal-header">          
                                        <h4 class="modal-title texto-modal text-center">Dados Pessoais</h4>        
                                    </div>        
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="text">ID </label>
                                                <input for="text" class="form-control" name="id" value="<?php echo $row_usuario['id'] ?>" readonly>
                                                <label for="text">Nome </label>
                                                <input type="text" class="form-control" name="nome" value="<?php echo $row_usuario['nome']; ?>" >
                                                <label for="email">E-mail </label>
                                                <input type="email" class="form-control" name="email" value="<?php echo $row_usuario['email']; ?>">
                                                <label for="text">Login </label>
                                                <input type="text" class="form-control" name="login" value="<?php echo $row_usuario['login']; ?>" readonly>
                                            </div>
                                        </form>        
                                    </div>        
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-outline-success">Atualizar</button>        
                                    </div>      
                                </div> 
                            </div>
                        </div>
                    </form>
                    <div class="container-fluid">
                        <div class="my-3">
                        </div>
                        <nav class="bg-mattBlackLight px-3 py-2">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                                    MBytes != 0
                                </a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
                                    IPs S@T
                                </a>
                                <a class="nav-item nav-link" id="nav-profileI-tab" data-toggle="tab" href="#nav-profileI" role="tab" aria-controls="nav-profile" aria-selected="false">
                                    S@Ts S/ Conn SEFAZ
                                </a>
                                <a class="nav-item nav-link" id="nav-profileII-tab" data-toggle="tab" href="#nav-profileII" role="tab" aria-controls="nav-profile" aria-selected="false">
                                    S@Ts Erro WAN
                                </a>
                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">
                                    TEF
                                </a>
                            </div>
                        </nav>
                        <div class="tab-content bg-mattBlackLight px-3 py-2" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <div class="row">
                                    <div class="col-sm-6">
                                    </div>
                                    <div class="col-sm-5">
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="../export/export_sat_errombytes.php?errdisk=errodisco">        
                                            <button class="btn btn-sm btn-outline-danger">
                                                Export
                                            </button>
                                        </a>
                                    </div>
                                    <div class="col-sm-12 py-2">
                                        <h6>S@Ts com MBytes != 0</h6>
                                        <span id="conteudo"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <div class="row">
                                    <div class="col-sm-6">
                                    </div>
                                    <div class="col-sm-5">
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="../export/export_sat_erroip.php?errip=erroip">        
                                            <button class="btn btn-sm btn-outline-danger">
                                                Export
                                            </button>
                                        </a>
                                    </div>
                                    <div class="col-sm-12">
                                        <h6>IPs distintos do número do caixa</h6>
                                        <span id="conteudo_I"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-profileI" role="tabpanel" aria-labelledby="nav-profileI-tab">
                                <div class="row">
                                    <div class="col-sm-6">
                                    </div>
                                    <div class="col-sm-5">
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="../export/export_sat_errosefaz.php?errsefaz=errsefaz">        
                                            <button class="btn btn-sm btn-outline-danger">
                                                Export
                                            </button>
                                        </a>
                                    </div>
                                    <div class="col-sm-12">
                                        <h6>S@Ts s/ comunicação com a SEFAZ a mais de 5 dias</h6>
                                        <span id="conteudo_IV"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-profileII" role="tabpanel" aria-labelledby="nav-profileII-tab">
                                <div class="row">
                                    <div class="col-sm-6">
                                    </div>
                                    <div class="col-sm-5">
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="../export/export_sat_errowan.php?errwan=errwan">        
                                            <button class="btn btn-sm btn-outline-danger">
                                                Export
                                            </button>
                                        </a>
                                    </div>
                                    <div class="col-sm-12">
                                        <h6>S@Ts com porta WAN não conectada</h6>
                                        <span id="conteudo_V"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                <div class="row">
                                    <div class="col-sm-6">
                                    </div>
                                    <div class="col-sm-5">
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="../export/export_erro_tef_pdv.php?errtefpdv=dif_pdv">        
                                            <button class="btn btn-sm btn-outline-danger">
                                                Export
                                            </button>
                                        </a>
                                    </div>
                                    <div class="col-sm-12">
                                        <h6>TEF: Número do Caixa != Codigo PDV</h6>
                                        <span id="conteudo_II"></span>
                                    </div>
                                    <div class="col-sm-6">
                                    </div>
                                    <div class="col-sm-5">
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="../export/export_erro_tef_loja.php?errtefloja=dif_loja">        
                                            <button class="btn btn-sm btn-outline-danger">
                                                Export
                                            </button>
                                        </a>
                                    </div>
                                    <div class="col-sm-12">
                                        <h6>TEF: Número de Loja != Codigo Filial</h6>
                                        <span id="conteudo_III"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                        var qnt_result_pg = 10; //quantidade de registro por página
                        var pagina = 1; //página inicial
                        $(document).ready(function () {
                            lista_disk_sat(pagina, qnt_result_pg); //Chamar a função para listar os registros
                        });
                        
                        function lista_disk_sat(pagina, qnt_result_pg){
                            var dados = {
                                pagina: pagina,
                                qnt_result_pg: qnt_result_pg
                            }
                            $.post('../listas/lista_sat_disco.php', dados , function(retorna){
                                //Subtitui o valor no seletor id="conteudo"
                                $("#conteudo").html(retorna);
                            });
                        }
                    </script>
                    <script>
                        var qnt_result_pg = 10; //quantidade de registro por página
                        var pagina = 1; //página inicial
                        $(document).ready(function () {
                            lista_ip_sat(pagina, qnt_result_pg); //Chamar a função para listar os registros
                        });
                        
                        function lista_ip_sat(pagina, qnt_result_pg){
                            var dados = {
                                pagina: pagina,
                                qnt_result_pg: qnt_result_pg
                            }
                            $.post('../listas/lista_sat_ip.php', dados , function(retorna){
                                //Subtitui o valor no seletor id="conteudo"
                                $("#conteudo_I").html(retorna);
                            });
                        }
                    </script>
                    <script>
                        var qnt_result_pg = 10; //quantidade de registro por página
                        var pagina = 1; //página inicial
                        $(document).ready(function () {
                            lista_tef_pdv(pagina, qnt_result_pg); //Chamar a função para listar os registros
                        });
                        
                        function lista_tef_pdv(pagina, qnt_result_pg){
                            var dados = {
                                pagina: pagina,
                                qnt_result_pg: qnt_result_pg
                            }
                            $.post('../listas/lista_tef_pdv.php', dados , function(retorna){
                                //Subtitui o valor no seletor id="conteudo"
                                $("#conteudo_II").html(retorna);
                            });
                        }
                    </script>
                    <script>
                        var qnt_result_pg = 10; //quantidade de registro por página
                        var pagina = 1; //página inicial
                        $(document).ready(function () {
                            lista_tef_loja(pagina, qnt_result_pg); //Chamar a função para listar os registros
                        });
                        
                        function lista_tef_loja(pagina, qnt_result_pg){
                            var dados = {
                                pagina: pagina,
                                qnt_result_pg: qnt_result_pg
                            }
                            $.post('../listas/lista_tef_loja.php', dados , function(retorna){
                                //Subtitui o valor no seletor id="conteudo"
                                $("#conteudo_III").html(retorna);
                            });
                        }
                    </script>
                    <script>
                        var qnt_result_pg = 10; //quantidade de registro por página
                        var pagina = 1; //página inicial
                        $(document).ready(function () {
                            lista_sat_com_sefaz(pagina, qnt_result_pg); //Chamar a função para listar os registros
                        });
                        
                        function lista_sat_com_sefaz(pagina, qnt_result_pg){
                            var dados = {
                                pagina: pagina,
                                qnt_result_pg: qnt_result_pg
                            }
                            $.post('../listas/lista_sat_com_sefaz.php', dados , function(retorna){
                                //Subtitui o valor no seletor id="conteudo"
                                $("#conteudo_IV").html(retorna);
                            });
                        }
                    </script>
                    <script>
                        var qnt_result_pg = 10; //quantidade de registro por página
                        var pagina = 1; //página inicial
                        $(document).ready(function () {
                            lista_sat_st_wan(pagina, qnt_result_pg); //Chamar a função para listar os registros
                        });
                        
                        function lista_sat_st_wan(pagina, qnt_result_pg){
                            var dados = {
                                pagina: pagina,
                                qnt_result_pg: qnt_result_pg
                            }
                            $.post('../listas/lista_sat_st_wan.php', dados , function(retorna){
                                //Subtitui o valor no seletor id="conteudo"
                                $("#conteudo_V").html(retorna);
                            });
                        }
                    </script>
                </main>
            </div>
        </div>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/jquery.slimscroll.min.js"></script>
        <script src="../js/script.js"></script>
        <script src="../js/feather.min.js"></script>
        <script>
            feather.replace()
        </script>
    </body>
</html>