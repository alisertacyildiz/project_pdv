<?php
    include_once("../security/seguranca.php");
    protegePagina();
    $id = $_SESSION['usuarioID'];
	$result_usuario = "SELECT * FROM tb_usuarios WHERE id = '$id'";
	$resultado_usuario = mysqli_query($conn, $result_usuario);
	$row_usuario = mysqli_fetch_assoc($resultado_usuario);
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie-edge" />
        <link rel="icon" href="../img/favicon.ico" />
        <title>Suport TPVs | User</title>
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css//material-icons.min.css" />
        <link rel="stylesheet" href="../css/style.css" />
    </head>

    <body>
        <?php
            date_default_timezone_set("America/Sao_Paulo");
            setlocale(LC_ALL, 'pt_BR');
            $num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
            //Obter a data atual
            $resultado_qnt_cadastros = mysqli_query($conn, $num_users);
            $row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
            
            $data['atual'] = date('Y-m-d H:i:s'); 

            //Diminuir 20 segundos 
            $data['online'] = strtotime($data['atual'] . " - 20 seconds");
            $data['online'] = date("Y-m-d H:i:s",$data['online']);
            
            //Pesquisar os ultimos usuarios online nos 20 segundo
            $result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
            
            $resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
            $row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
            
            $qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
            $qnt_perc = round((($row_qnt_visitas['online'] / $row_qnt_cadastros['cadastrado'])*100),2);
        ?>    
        <script src="../js/jquery-3.2.1.min.js"></script>    
        <script>
            //Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
            setInterval(function(){
            //Incluir e enviar o POST para o arquivo responsável em fazer contagem
            $.post("../processo/processa_vis.php", {contar: '',}, function(data){
                $('#online').text(data);
            });
            }, 10000);
        </script>
        <nav class="navbar navbar-expand-lg navbar-dark bg-mattBlackLight fixed-top">
            <button class="navbar-toggler sideMenuToggler" type="button">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="user2.php">
                User - <?php echo $_SESSION['usuarioNome'];?>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons icon">
                                person
                            </i>
                            <span class="text">
                                Account
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaDadosPessoais">
                                <span data-feather="info"></span> Pefil</a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaSenha">
                                <span data-feather="lock"></span> Alterar senha</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="../security/sair.php">
                                <span data-feather="share"></span> Log Out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="wrapper d-flex">
            <div class="sideMenu bg-mattBlackLight">
                <div class="sidebar">
				<ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="user2.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    home
                                </i>
                                <span class="text">
                                    Home
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="consultas.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    search
                                </i>
                                <span class="text">
                                    Consultas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="srv_chaves.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    dns
                                </i>
                                <span class="text">
                                    Servidor de Chaves
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="sats.php?pagina=1" class="nav-link px-2">
                                <i class="material-icons icon">
                                    camera_alt
                                </i>
                                <span class="text">
                                    SATs
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="list_lojas.php?pagina=1" class="nav-link px-2">
                                <i class="material-icons icon">
                                    pages
                                </i>
                                <span class="text">
                                    Cadastro de Lojas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="usuarios.php" class="nav-link px-2 active">
                                <i class="material-icons icon">
                                    supervisor_account
                                </i>
                                <span class="text">
                                    Usuários
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="divergencia.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    bug_report
                                </i>
                                <span class="text">
                                    Divergentes
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="scripts.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    description
                                </i>
                                <span class="text">
                                    Scripts
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="tabelas.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    dashboard
                                </i>
                                <span class="text">
                                    Tabelas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link px-2 sideMenuToggler">
                                <i class="material-icons icon expandView ">
                                    view_list
                                </i>
                                <span class="text">
                                    Ocultar
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="content">

                <main>
                    <form name="frm_senha" action="../processo/salva_senha.php" method="POST">
                        <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content bg-mattBlackLight px-3 py-3">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>  
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                                                <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                                                <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-primary">Confirma</button>
                                    </div>
                                </div>
                            </div>
                        </div>
					</form>
					<form name="frm_dados_pessoais" action="../processo/atualizar_usuario.php" method="POST">
                        <div class="modal fade" id="editaDadosPessoais" tabindex="-1" role="dialog" aria-labelledby="editaDadosPessoaisLabel" aria-hidden="true">    
                            <div class="modal-dialog" role="document">          
                                <!-- Modal content-->      
                                <div class="modal-content bg-mattBlackLight">        
                                    <div class="modal-header">          
                                        <h4 class="modal-title texto-modal text-center">Dados Pessoais</h4>        
                                    </div>        
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="text">ID </label>
                                                <input for="text" class="form-control" name="id" value="<?php echo $row_usuario['id'] ?>" readonly>
                                                <label for="text">Nome </label>
                                                <input type="text" class="form-control" name="nome" value="<?php echo $row_usuario['nome']; ?>" >
                                                <label for="email">E-mail </label>
                                                <input type="email" class="form-control" name="email" value="<?php echo $row_usuario['email']; ?>">
                                                <label for="text">Login </label>
                                                <input type="text" class="form-control" name="login" value="<?php echo $row_usuario['login']; ?>" readonly>
                                            </div>
                                        </form>        
                                    </div>        
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-outline-success">Atualizar</button>        
                                    </div>      
                                </div> 
                            </div>
                        </div>
                    </form>
                <div class="container-fluid">
						<div class="my-3">
                        </div>
						<div class="row">
                        <div class="col-md-4 my-3">
                            <div class="bg-mattBlackLight px-3 py-3">
                                <h5 class="mb-2">Dados Usuários</h5>
                            </div>
                        </div>  
                        <div class="col-md-12">
                            <div class="bg-mattBlackLight px-3 py-3">
                                <div class="table-responsive">
                                    <table class="table table-sm table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nome</th>
                                            <th>Login</th>
                                            <th>E-mail</th>
                                            <th>Nível</th>
                                            <th>Data Criação</th>
                                            <th>Ações</th>
                                        </tr>
                                        </thead>
                                            <?php
                                                // Conectando ao banco de dados:
                                                    include_once("../security/conecta.php");
                                                $sql = "SELECT * FROM tb_usuarios ";
                                                $resultado = mysqli_query($conn,$sql) or die("Erro ao retornar dados");
                                                // Obtendo os dados por meio de um loop while
                                                echo "<tbody>";
                                                while ($registro = mysqli_fetch_array($resultado))
                                                {
                                                    $id = $registro['id'];
                                                    $nome = $registro['nome'];
                                                    $login = $registro['login'];
                                                    $email = $registro['email'];
                                                    $nivel = $registro['nivel'];
                                                    $data_criacao = $registro['data_criacao'];
                                                    $data_modificacao = $registro['data_modificacao'];
                                                    
                                                    echo "<tr>";
                                                    echo "<td>".$id."</td>";

                                                    $data_user['atual'] = date('Y-m-d H:i:s');  
                                                    //Diminuir 20 segundos 
                                                    $data_user['online'] = strtotime($data_user['atual'] . " - 20 seconds");
                                                    $data_user['online'] = date("Y-m-d H:i:s",$data_user['online']);
                                                    
                                                    //Pesquisar os ultimos usuarios online nos 20 segundo
                                                    $result_user_online = "SELECT * FROM tb_visitas WHERE id_usuario = '" . $registro['id'] . "' AND data_final >= '" . $data_user['online'] . "'";
                                                    
                                                    $resultado_user_online = mysqli_query($conn, $result_user_online);
                                                    $row_user_online = mysqli_fetch_assoc($resultado_user_online);
                                                    
                                                    if (!empty($row_user_online))
                                                    {
                                                    echo "<td><img width='10px' height='10px' src='../img/online.png' />".$nome."</td>";
                                                    }else{
                                                    echo "<td><img width='10px' height='10px' src='../img/offline.png' />".$nome."</td>";
                                                    }
                                                    echo "<td>".$login."</td>";
                                                    echo "<td>".$email."</td>";
                                                    echo "<td>".$nivel."</td>";
                                                    echo "<td>".$data_criacao."</td>";
                                                    echo "<td><button type='button' class='btn btn-sm btn-outline-info' data-toggle='modal' data-target='#usuariosModal".$registro['id']."'>Visualizar</button><a href='../processo/reset_user.php?id=".$registro['id']."'><button type='button' class='btn btn-sm btn-outline-warning'>Reset</button></a>
                                                        </td>";
                                                    echo "</tr>";
                                                    echo '<div class="modal fade" id="usuariosModal'.$registro['id'].'" tabindex="-1" role="dialog" aria-labelledby="usuariosModalLabel" aria-hidden="true">';
                                                    echo '<div class="modal-dialog" role="document">';
                                                    echo '<div class="modal-content bg-mattBlackLight px-3 py-3">';
                                                        echo '<div class="modal-header">';
                                                        echo '<h5 class="modal-title" id="usuariosModalLabel">ID Usuário: '.$registro['id'].'</h5>';
                                                        echo '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
                                                            echo '<span aria-hidden="true">&times;</span>';
                                                        echo '</button>';
                                                        echo '</div>';
                                                        echo '<div class="modal-body">';
                                                        echo '<form>';
                                                            echo '<div class="form-group">';
                                                            echo '<label for="recipient-name" class="col-form-label">Nome</label>'; 
                                                            echo "</br>"; 
                                                            echo $registro['nome'];
                                                            echo '</div>'; 
                                                            echo '<div class="form-group">';
                                                            echo '<label for="recipient-name" class="col-form-label">Login</label>'; 
                                                            echo "</br>";
                                                            echo $registro['login'];
                                                            echo '</div>'; 
                                                            echo '<div class="form-group">';
                                                            echo '<label for="recipient-name" class="col-form-label">E-mail</label>';
                                                            echo "</br>";
                                                            echo $registro['email'];
                                                            echo '</div>'; 
                                                            echo '<div class="form-group">';
                                                            echo '<label for="recipient-name" class="col-form-label">Nível</label>';
                                                            echo "</br>";
                                                            echo $registro['nivel'];
                                                            echo '</div>';
                                                            echo '<div class="form-group">';
                                                            echo '<label for="recipient-name" class="col-form-label">Data Criação</label>';
                                                            echo "</br>";
                                                            echo $registro['data_criacao'];
                                                            echo '</div>';
                                                            echo '<div class="form-group">';
                                                            echo '<label for="recipient-name" class="col-form-label">Data Modificação</label>'; 
                                                            echo "</br>";
                                                            echo $registro['data_modificacao'];
                                                            echo '</div>';
                                                        echo '</form>';
                                                        echo '</div>';
                                                        echo '<div class="modal-footer">';
                                                        echo '<button type="button" class="btn btn-sm btn-success" data-dismiss="modal">Fechar</button>';
                                                        echo '</div>';
                                                    echo '</div>';
                                                    echo '</div>';
                                                echo '</div>';
                                                }

                                                    echo "</tbody>";
                                                
                                            ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/jquery.slimscroll.min.js"></script>
        <script src="../js/script.js"></script>
        <script src="../js/feather.min.js"></script>
        <script>
            feather.replace()
        </script>
    </body>
</html>
