<?php
    include_once("../security/seguranca.php");
    protegePagina();
    $id = $_SESSION['usuarioID'];
	$result_usuario = "SELECT * FROM tb_usuarios WHERE id = '$id'";
	$resultado_usuario = mysqli_query($conn, $result_usuario);
	$row_usuario = mysqli_fetch_assoc($resultado_usuario);
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie-edge" />
        <link rel="icon" href="../img/favicon.ico" />
        <title>Suport TPVs | User</title>
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css//material-icons.min.css" />
        <link rel="stylesheet" href="../css/style.css" />
    </head>

    <body>
        <?php
            date_default_timezone_set("America/Sao_Paulo");
            setlocale(LC_ALL, 'pt_BR');
            $num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
            //Obter a data atual
            $resultado_qnt_cadastros = mysqli_query($conn, $num_users);
            $row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
            
            $data['atual'] = date('Y-m-d H:i:s'); 

            //Diminuir 20 segundos 
            $data['online'] = strtotime($data['atual'] . " - 20 seconds");
            $data['online'] = date("Y-m-d H:i:s",$data['online']);
            
            //Pesquisar os ultimos usuarios online nos 20 segundo
            $result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
            
            $resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
            $row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
            
            $qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
            $qnt_perc = round((($row_qnt_visitas['online'] / $row_qnt_cadastros['cadastrado'])*100),2);
        ?>    
        <script src="../js/jquery-3.2.1.min.js"></script>    
        <script>
            //Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
            setInterval(function(){
            //Incluir e enviar o POST para o arquivo responsável em fazer contagem
            $.post("../processo/processa_vis.php", {contar: '',}, function(data){
                $('#online').text(data);
            });
            }, 10000);
        </script>
        <nav class="navbar navbar-expand-lg navbar-dark bg-mattBlackLight fixed-top">
            <button class="navbar-toggler sideMenuToggler" type="button">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="user2.php">
                User - <?php echo $_SESSION['usuarioNome'];?>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons icon">
                                person
                            </i>
                            <span class="text">
                                Account
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaDadosPessoais">
                                <span data-feather="info"></span> Pefil</a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaSenha">
                                <span data-feather="lock"></span> Alterar senha</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="../security/sair.php">
                                <span data-feather="share"></span> Log Out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="wrapper d-flex">
            <div class="sideMenu bg-mattBlackLight">
                <div class="sidebar">
				<ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="user2.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    home
                                </i>
                                <span class="text">
                                    Home
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="consultas.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    search
                                </i>
                                <span class="text">
                                    Consultas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="srv_chaves.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    dns
                                </i>
                                <span class="text">
                                    Servidor de Chaves
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="sats.php?pagina=1" class="nav-link px-2">
                                <i class="material-icons icon">
                                    camera_alt
                                </i>
                                <span class="text">
                                    SATs
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="list_lojas.php?pagina=1" class="nav-link px-2">
                                <i class="material-icons icon">
                                    pages
                                </i>
                                <span class="text">
                                    Cadastro de Lojas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="usuarios.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    supervisor_account
                                </i>
                                <span class="text">
                                    Usuários
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="divergencia.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    bug_report
                                </i>
                                <span class="text">
                                    Divergentes
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="scripts.php" class="nav-link px-2 active">
                                <i class="material-icons icon">
                                    description
                                </i>
                                <span class="text">
                                    Scripts
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="tabelas.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    dashboard
                                </i>
                                <span class="text">
                                    Tabelas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link px-2 sideMenuToggler">
                                <i class="material-icons icon expandView ">
                                    view_list
                                </i>
                                <span class="text">
                                    Ocultar
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="content">
                <main>
                    <form name="frm_senha" action="../processo/salva_senha.php" method="POST">
                        <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content bg-mattBlackLight px-3 py-3">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>  
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                                                <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                                                <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-outline-primary">Confirma</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form name="frm_dados_pessoais" action="../processo/atualizar_usuario.php" method="POST">
                        <div class="modal fade" id="editaDadosPessoais" tabindex="-1" role="dialog" aria-labelledby="editaDadosPessoaisLabel" aria-hidden="true">    
                            <div class="modal-dialog" role="document">          
                                <!-- Modal content-->      
                                <div class="modal-content bg-mattBlackLight">        
                                    <div class="modal-header">          
                                        <h4 class="modal-title texto-modal text-center">Dados Pessoais</h4>        
                                    </div>        
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="text">ID </label>
                                                <input for="text" class="form-control" name="id" value="<?php echo $row_usuario['id'] ?>" readonly>
                                                <label for="text">Nome </label>
                                                <input type="text" class="form-control" name="nome" value="<?php echo $row_usuario['nome']; ?>" >
                                                <label for="email">E-mail </label>
                                                <input type="email" class="form-control" name="email" value="<?php echo $row_usuario['email']; ?>">
                                                <label for="text">Login </label>
                                                <input type="text" class="form-control" name="login" value="<?php echo $row_usuario['login']; ?>" readonly>
                                            </div>
                                        </form>        
                                    </div>        
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-outline-success">Atualizar</button>        
                                    </div>      
                                </div> 
                            </div>
                        </div>
                    </form>
                    <div class="container-fluid">
                        <div class="my-3">
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="bg-mattBlackLight py-3 px-3">
                                    <h5>Serviços PDV</h5>
                                    <div class="table-responsive">
                                        <table class="table table-sm table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Serviço</th>
                                                    <th>Descrição</th>
                                                    <th>Ações</th>
                                                </tr>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            1
                                                        </td>
                                                        <td>
                                                            CUPS
                                                        </td>
                                                        <td>
                                                            Serviço de gerenciamente de impressão remota Linux.
                                                        </td>
                                                        <td>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-success">
                                                                    Start
                                                                </button>
                                                            </a>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-warning">
                                                                    Restart
                                                                </button>
                                                            </a>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-danger">
                                                                    Stop
                                                                </button>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            2
                                                        </td>
                                                        <td>
                                                            WSD
                                                        </td>
                                                        <td>
                                                            Serviço controla as ações relacionadas a comunicação com o servidores de ClubDIA% e Servidor de Cliente.
                                                        </td>
                                                        <td>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-success">
                                                                    Start
                                                                </button>
                                                            </a>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-warning">
                                                                    Restart
                                                                </button>
                                                            </a>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-danger">
                                                                    Stop
                                                                </button>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            3
                                                        </td>
                                                        <td>
                                                            WSD_SSL
                                                        </td>
                                                        <td>
                                                            Serviço controla as ações relacionadas a comunicação com o CWS, concentrador que faz a ponte entre o DIA%\LOJA e a CISS.
                                                        </td>
                                                        <td>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-success">
                                                                    Start
                                                                </button>
                                                            </a>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-warning">
                                                                    Restart
                                                                </button>
                                                            </a>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-danger">
                                                                    Stop
                                                                </button>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            4
                                                        </td>
                                                        <td>
                                                            Servidor Daruma
                                                        </td>
                                                        <td>
                                                            Serviço controla as ações relacionadas a comunicação com Debian,executa requisições sockets entre os ambientes do SO. 
                                                        </td>
                                                        <td>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-success">
                                                                    Start
                                                                </button>
                                                            </a>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-warning">
                                                                    Restart
                                                                </button>
                                                            </a>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-danger">
                                                                    Stop
                                                                </button>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="my-3">
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="bg-mattBlackLight py-3 px-3">
                                    <h5>Programas Remotos</h5>
                                    <div class="table-responsive">
                                        <table class="table table-sm table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Programa</th>
                                                    <th>Descrição</th>
                                                    <th>Ações</th>
                                                </tr>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            1
                                                        </td>
                                                        <td>
                                                            Cópiar venda
                                                        </td>
                                                        <td>
                                                            Gerar os arquivos de venda de uma loja para que seja enviado de forma manual, no próximo Gestor Noite.
                                                        </td>
                                                        <td>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-success" data-toggle="modal" data-target="#getSale">
                                                                    Start
                                                                </button>
                                                            </a>
                                                            <a href="#?">
                                                                <button type="button" class="btn btn-sm btn-outline-warning" data-toggle="modal" data-target="#downSale">
                                                                    Files
                                                                </button>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form name="frm_copy_venda" action="scripts.php" method="POST">
                        <div class="modal fade" id="getSale" tabindex="-1" role="dialog" aria-labelledby="getSaleLabel" aria-hidden="true">    
                            <div class="modal-dialog" role="document">          
                                <!-- Modal content-->      
                                <div class="modal-content bg-mattBlackLight">        
                                    <div class="modal-header">          
                                        <h4 class="modal-title texto-modal text-center">Programas Remotos - Copiar Venda</h4>
                                    </div>        
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="text"><p>Esta opção permite salvar as vendas não transmitidas no último Gestor Noite.</p><p>IMPORTANTE: Para executar este processo a Máster precisa estar em Selecione Programa.</p></label>
                                                <label for="text">Informe o número da loja:</label>
                                                <input for="text" class="form-control" name="id-num-loja">
                                            </div>
                                        </form>        
                                    </div>        
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-outline-success">Gerar</button>
                                    </div>      
                                </div> 
                            </div>
                        </div>
                        <?php
                            include_once("../processo_bash/get_sale.php");
                            get_sale();
                        ?>
                    </form>
                    <form name="frm_down_venda" action="scripts.php" method="POST">
                        <div class="modal fade" id="downSale" tabindex="-1" role="dialog" aria-labelledby="downSaleLabel" aria-hidden="true">    
                            <div class="modal-dialog" role="document">          
                                <!-- Modal content-->      
                                <div class="modal-content bg-mattBlackLight">        
                                    <div class="modal-header">          
                                        <h4 class="modal-title texto-modal text-center">Programas Remotos - Download Arquivos de Venda</h4>        
                                    </div>        
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover table-sm">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Arquivo de venda</th>
                                                                <th>Ações</th>
                                                            </tr>
                                                        </thead>
                                                        <?php
                                                            echo "<tbody>";
                                                            $path='/var/www/html/resorces/getsale/';
                                                            $diretorio = dir($path);
                                                                while($arquivo = $diretorio -> read()){	
                                                                    if ($arquivo != '..' && $arquivo != '.'){
                                                                        echo "<tr>";
                                                                        echo "<td><center><span data-feather='briefcase'></span></center></td>";
                                                                        echo "<td>".$arquivo."</td>";
                                                                        echo "<td><a style='color: white;' href='../getsale/".$arquivo."'><span data-feather='download'></span></a>
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;<a style='color: white;' href='../processo/delete_filesale.php?delsale=".$arquivo."'><span data-feather='trash-2'></span></a></td>";
                                                                        echo "</tr>";		
                                                                    }
                                                                }
                                                                $diretorio -> close();
                                                            echo "</tbody>";
                                                        ?>
                                                    </table>
                                                </div>
                                            </div>
                                        </form>        
                                    </div>        
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Fechar</button>     
                                    </div>      
                                </div> 
                            </div>
                        </div>
                    </form>
                </main>
            </div>
        </div>
        <script>
           $(document).ready(function() {
                $('.btn-outline-success').click(function(){
                    $('#aguarde, #blanket').css('display','block');
                });
            });
        </script>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/jquery.slimscroll.min.js"></script>
        <script src="../js/script.js"></script>
        <script src="../js/feather.min.js"></script>
        <script>
        feather.replace()
        </script>
    </body>

    </html