<?php
    include_once("../security/seguranca.php");
    protegePagina();
    $id = $_SESSION['usuarioID'];
	$result_usuario = "SELECT * FROM tb_usuarios WHERE id = '$id'";
	$resultado_usuario = mysqli_query($conn, $result_usuario);
	$row_usuario = mysqli_fetch_assoc($resultado_usuario);
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie-edge" />
        <link rel="icon" href="../img/favicon.ico" />
        <title>Suport TPVs | User</title>
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css//material-icons.min.css" />
        <link rel="stylesheet" href="../css/style.css" />
    </head>

    <body>
        <?php
            date_default_timezone_set("America/Sao_Paulo");
            setlocale(LC_ALL, 'pt_BR');
            $num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
            //Obter a data atual
            $resultado_qnt_cadastros = mysqli_query($conn, $num_users);
            $row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
            
            $data['atual'] = date('Y-m-d H:i:s'); 

            //Diminuir 20 segundos 
            $data['online'] = strtotime($data['atual'] . " - 20 seconds");
            $data['online'] = date("Y-m-d H:i:s",$data['online']);
            
            //Pesquisar os ultimos usuarios online nos 20 segundo
            $result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
            
            $resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
            $row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
            
            $qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
            $qnt_perc = round((($row_qnt_visitas['online'] / $row_qnt_cadastros['cadastrado'])*100),2);
        ?>    
        <script src="../js/jquery-3.2.1.min.js"></script>    
        <script>
            //Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
            setInterval(function(){
            //Incluir e enviar o POST para o arquivo responsável em fazer contagem
            $.post("../processo/processa_vis.php", {contar: '',}, function(data){
                $('#online').text(data);
            });
            }, 10000);
        </script>
        <nav class="navbar navbar-expand-lg navbar-dark bg-mattBlackLight fixed-top">
            <button class="navbar-toggler sideMenuToggler" type="button">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="user2.php">
                User - <?php echo $_SESSION['usuarioNome'];?>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons icon">
                                person
                            </i>
                            <span class="text">
                                Account
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaDadosPessoais">
                                <span data-feather="info"></span> Pefil</a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaSenha">
                                <span data-feather="lock"></span> Alterar senha</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="../security/sair.php">
                                <span data-feather="share"></span> Log Out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="wrapper d-flex">
            <div class="sideMenu bg-mattBlackLight">
                <div class="sidebar">
				<ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="user2.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    home
                                </i>
                                <span class="text">
                                    Home
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="consultas.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    search
                                </i>
                                <span class="text">
                                    Consultas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="srv_chaves.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    dns
                                </i>
                                <span class="text">
                                    Servidor de Chaves
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="sats.php?pagina=1" class="nav-link px-2">
                                <i class="material-icons icon">
                                    camera_alt
                                </i>
                                <span class="text">
                                    SATs
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="list_lojas.php?pagina=1" class="nav-link px-2">
                                <i class="material-icons icon">
                                    pages
                                </i>
                                <span class="text">
                                    Cadastro de Lojas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="usuarios.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    supervisor_account
                                </i>
                                <span class="text">
                                    Usuários
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="divergencia.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    bug_report
                                </i>
                                <span class="text">
                                    Divergentes
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="scripts.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    description
                                </i>
                                <span class="text">
                                    Scripts
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="tabelas.php" class="nav-link px-2 active">
                                <i class="material-icons icon">
                                    dashboard
                                </i>
                                <span class="text">
                                    Tabelas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link px-2 sideMenuToggler">
                                <i class="material-icons icon expandView ">
                                    view_list
                                </i>
                                <span class="text">
                                    Ocultar
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="content">

                <main>
                    <form name="frm_senha" action="../processo/salva_senha.php" method="POST">
                        <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content bg-mattBlackLight px-3 py-3">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>  
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                                                <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                                                <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-primary">Confirma</button>
                                    </div>
                                </div>
                            </div>
                        </div>
					</form>
					<form name="frm_dados_pessoais" action="../processo/atualizar_usuario.php" method="POST">
                        <div class="modal fade" id="editaDadosPessoais" tabindex="-1" role="dialog" aria-labelledby="editaDadosPessoaisLabel" aria-hidden="true">    
                            <div class="modal-dialog" role="document">          
                                <!-- Modal content-->      
                                <div class="modal-content bg-mattBlackLight">        
                                    <div class="modal-header">          
                                        <h4 class="modal-title texto-modal text-center">Dados Pessoais</h4>        
                                    </div>        
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="text">ID </label>
                                                <input for="text" class="form-control" name="id" value="<?php echo $row_usuario['id'] ?>" readonly>
                                                <label for="text">Nome </label>
                                                <input type="text" class="form-control" name="nome" value="<?php echo $row_usuario['nome']; ?>" >
                                                <label for="email">E-mail </label>
                                                <input type="email" class="form-control" name="email" value="<?php echo $row_usuario['email']; ?>">
                                                <label for="text">Login </label>
                                                <input type="text" class="form-control" name="login" value="<?php echo $row_usuario['login']; ?>" readonly>
                                            </div>
                                        </form>        
                                    </div>        
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-outline-success">Atualizar</button>        
                                    </div>      
                                </div> 
                            </div>
                        </div>
                    </form>
                <div class="container-fluid">
                    <div class="my-3">
                    </div>
                    <main>
                        <div class="row">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6">
                                <div class="bg-mattBlackLight  px-3 py-2">
                                    <h4>UPDATE BANCOS</h4>
                                </div>
                                <div class="bg-mattBlackLight  px-3 py-2">
                                    <div class="table-responsive">
                                        <table class="table table-sm table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>BANCO</th>
                                                    <th>DATA ATUALIZAÇÃO</th>
                                                    <th>STATUS</th>
                                                </tr>
                                            </thead>
                                            <?php
                                                $sql_update_banco = "SELECT id, banco, data_ult_atualizacao, data_atualizacao, status FROM tb_updates_banco";
                                                $query_update_banco = mysqli_query($conn, $sql_update_banco);
                                                echo "<tbody>";
                                                while ($result_update_banco = mysqli_fetch_array($query_update_banco))
                                                {
                                                    $idpudtbd = $result_update_banco['id'];
                                                    $bancoupdtbd = $result_update_banco['banco'];
                                                    $dtultatualupdtbd = $result_update_banco['data_ult_atualizacao'];
                                                    $dtatualupdtbd = $result_update_banco['data_atualizacao'];
                                                    $statusupdtbd = $result_update_banco['status'];

                                                    echo "<tr>";
                                                        echo "<td>".$idpudtbd."</td>";
                                                        echo "<td>".$bancoupdtbd."</td>";
                                                        echo "<td>".$dtatualupdtbd."</td>";
                                                        if($statusupdtbd == "Atualizado")
                                                        {
                                                            echo "<td><img width='15px' height='15px' src='../img/atualizado.png' /> ".$statusupdtbd."</td>";
                                                        }else if ($statusupdtbd == "Executando"){
                                                            echo "<td><img width='15px' height='15px' src='../img/alerta.png' /> ".$statusupdtbd."</td>";
                                                        }else{
                                                            echo "<td><img width='15px' height='15px' src='../img/desatualizado.png' /> ".$statusupdtbd."</td>";
                                                        }
                                                    echo "</tr>";
                                                }
                                                echo "</tbody>";
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="my-3">
                        </div>
                        <div class="my-3">
                            <div class="bg-mattBlackLight px-3 py-1">
                                <h4>Versões de PDV</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="bg-mattBlackLight px-3 py-2">
                                    <div class="table-responsive">
                                        <table class="table table-sm table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Versão</th>
                                                    <th>Quantidade de Lojas</th>
                                                    <th>Visualizar</th>
                                                </tr>
                                            </thead>
                                            <?php
                                                $sql_group_versao = "SELECT versao, COUNT(id) AS qntd FROM tb_dados_loja WHERE data BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() GROUP BY versao" ;
                                                $query_group_versao = mysqli_query($conn, $sql_group_versao);
                                                echo "<tbody>";
                                                $i=1;
                                                while ($result_group_versao = mysqli_fetch_array($query_group_versao))
                                                {
                                                    $vazio = $i;
                                                    $versao = $result_group_versao['versao'];
                                                    $qntd = $result_group_versao['qntd'];

                                                    echo "<tr data-toggle='modal' data-target='#modal_dados_loja".$vazio."'>";
                                                        echo "<td>".$vazio."</td>";
                                                        echo "<td>".$versao."</td>";
                                                        echo "<td>".$qntd."</td>";
                                                        echo "<td><button type='button' class='btn btn-outline-info btn-sm' data-toggle='modal' data-target='#modal_dados_loja".$vazio."'>Visualizar</button></a></td>";
                                                    echo "</tr>";
                                                    echo "<div class='modal fade' id='modal_dados_loja".$vazio."' tabindex='-1' role='dialog' aria-hidden='true'>";
                                                        echo "<div class='modal-dialog modal-dialog-scrollable modal-lg'>";
                                                            echo "<div class='modal-content bg-mattBlackLight px-3 py-3'>";
                                                                echo "<div class='modal-header'>";
                                                                    echo "<h5>Dados da Loja - ".$result_group_versao['versao']."</h5>";
                                                                    echo "<button type='button' class='close' data-dismiss='modal'>&times</button>";
                                                                echo "</div>";
                                                                echo "<div class='modal-header'>";
                                                                    echo '<a href="../export/export_dados_loja.php?vs='.$result_group_versao['versao'].'"><button type="submit" class="btn btn-outline-danger">Exportar</button></a>';
                                                                echo "</div>";
                                                                echo "<div class='modal-body'>";
                                                                    echo "<form>";
                                                                        echo '<div class="form-group">';
                                                                            echo '<div class="row">';
                                                                                echo '<div class="col-md-2">Loja</div>';
                                                                                echo '<div class="col-md-5">Versão</div>';
                                                                                echo '<div class="col-md-3">Qntd PDVs</div>';
                                                                                echo '<div class="col-md-1">IP</div>';
                                                                            echo "</div>";
                                                                            echo '<div class="row">';
                                                                                $sql_versao = 'SELECT * FROM tb_dados_loja WHERE Versao = "'.$result_group_versao['versao'].'"' ;
                                                                                $query_versao = mysqli_query($conn, $sql_versao);
                                                                                while ($result_versao = mysqli_fetch_array($query_versao))
                                                                                {
                                                                                    $nloja = $result_versao['Loja'];
                                                                                    $nversao = $result_versao['Versao'];
                                                                                    $ntpvs = $result_versao['Qntd_Tpvs'];
                                                                                    $nip = $result_versao['IP'];

                                                                                    echo '<div class="col-md-2">'.$nloja.'</div>';
                                                                                    echo '<div class="col-md-5">'.$nversao.'</div>';
                                                                                    echo '<div class="col-md-3">'.$ntpvs.'</div>';
                                                                                    echo '<div class="col-md-1">'.$nip.'</div>'; 
                                                                                }
                                                                            echo '</div>';
                                                                        echo '</div>';
                                                                    echo "</form>";
                                                                echo "</div>";
                                                            echo "</div>";
                                                        echo "</div>";
                                                    echo "</div>";
                                                    $i++;

                                                }
                                                echo "</tbody>";
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="my-3">
                        </div>
                        <div class="my-3">
                            <div class="bg-mattBlackLight px-3 py-1">
                                <h4>Dados dos S@Ts</h4>
                            </div>
                        </div>
                        <div class='row'>
                            <div class="col-sm-4">
                                <div class="bg-mattBlackLight px-3 py-2">
                                    <div class="table-responsive">
                                        <table class="table table-sm table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Firmware</th>
                                                    <th>Quantidade S@Ts</th>
                                                    <th>Visualizar</th>
                                                </tr>
                                            </thead>
                                            <?php
                                                $sql_group_fwsat = "SELECT firmware, COUNT(id) AS qntd FROM tb_sat WHERE Status = 'Ativo' GROUP BY firmware" ;
                                                $query_group_fwsat = mysqli_query($conn, $sql_group_fwsat);
                                                echo "<tbody>";
                                                $i=1;
                                                while ($result_group_fwsat = mysqli_fetch_array($query_group_fwsat))
                                                {
                                                    $vazio = $i;
                                                    $fwsatversao = $result_group_fwsat['firmware'];
                                                    $fwsatqntd = $result_group_fwsat['qntd'];

                                                    echo "<tr data-toggle='modal' data-target='#modal_dados_fwsat".$vazio."'>";
                                                        echo "<td>".$vazio."</td>";
                                                        echo "<td>".$fwsatversao."</td>";
                                                        echo "<td>".$fwsatqntd."</td>";
                                                        echo "<td><button type='button' class='btn btn-outline-info btn-sm' data-toggle='modal' data-target='#modal_dados_fwsat".$vazio."'>Visualizar</button></a></td>";
                                                    echo "</tr>";
                                                    echo "<div class='modal fade' id='modal_dados_fwsat".$vazio."' tabindex='-1' role='dialog' aria-hidden='true'>";
                                                        echo "<div class='modal-dialog modal-dialog-scrollable modal-lg'>";
                                                            echo "<div class='modal-content bg-mattBlackLight px-3 py-3'>";
                                                                echo "<div class='modal-header'>";
                                                                    echo "<h5>Dados S@T - ".$result_group_fwsat['firmware']."</h5>";
                                                                    echo "<button type='button' class='close' data-dismiss='modal'>&times</button>";
                                                                echo "</div>";
                                                                echo "<div class='modal-header'>";
                                                                    echo '<a href="../export/export_sat_fw.php?fw='.$result_group_fwsat['firmware'].'"><button type="submit" class="btn btn-outline-danger">Exportar</button></a>';
                                                                echo "</div>";
                                                                echo "<div class='modal-body'>";
                                                                    echo "<form>";
                                                                        echo '<div class="form-group">';
                                                                            echo '<div class="row">';
                                                                                echo '<div class="col-md-2">S@T</div>';
                                                                                echo '<div class="col-md-3">Firmware</div>';
                                                                                echo '<div class="col-md-2">Loja</div>';
                                                                                echo '<div class="col-md-2">PDV</div>';
                                                                                echo '<div class="col-md-2">IP</div>';
                                                                            echo "</div>";
                                                                            echo '<div class="row">';
                                                                                $sql_fwsatversao = 'SELECT * FROM tb_sat WHERE firmware = "'.$result_group_fwsat['firmware'].'" AND Status = "Ativo"' ;
                                                                                $query_fwsatversao = mysqli_query($conn, $sql_fwsatversao);
                                                                                while ($result_fwsatversao = mysqli_fetch_array($query_fwsatversao))
                                                                                {
                                                                                    $fwsatnloja = $result_fwsatversao['sat'];
                                                                                    $fwsatnversao = $result_fwsatversao['firmware'];
                                                                                    $fwsatntpvs = $result_fwsatversao['loja'];
                                                                                    $fwsatntpv = $result_fwsatversao['caixa'];
                                                                                    $fwsatnip = $result_fwsatversao['ip'];

                                                                                    echo '<div class="col-md-2">'.$fwsatnloja.'</div>';
                                                                                    echo '<div class="col-md-3">'.$fwsatnversao.'</div>';
                                                                                    echo '<div class="col-md-2">'.$fwsatntpvs.'</div>';
                                                                                    echo '<div class="col-md-2">'.$fwsatntpv.'</div>';
                                                                                    echo '<div class="col-md-2">'.$fwsatnip.'</div>'; 
                                                                                }
                                                                            echo '</div>';
                                                                        echo '</div>';
                                                                    echo "</form>";
                                                                echo "</div>";
                                                            echo "</div>";
                                                        echo "</div>";
                                                    echo "</div>";
                                                    $i++;

                                                }
                                                echo "</tbody>";
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="bg-mattBlackLight px-3 py-2">
                                    <div class="table-responsive">
                                        <table class="table table-sm table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Modelo S@T</th>
                                                    <th>Quantidade S@Ts</th>
                                                    <th>Visualizar</th>
                                                </tr>
                                            </thead>
                                            <?php
                                                $sql_group_mdlsat = "SELECT modelo_sat, COUNT(id) AS qntd FROM tb_sat WHERE Status = 'Ativo' GROUP BY modelo_sat" ;
                                                $query_group_mdlsat = mysqli_query($conn, $sql_group_mdlsat);
                                                echo "<tbody>";
                                                $i=1;
                                                while ($result_group_mdlsat = mysqli_fetch_array($query_group_mdlsat))
                                                {
                                                    $vazio = $i;
                                                    $mdlsatversao = $result_group_mdlsat['modelo_sat'];
                                                    $mdlsatqntd = $result_group_mdlsat['qntd'];

                                                    echo "<tr data-toggle='modal' data-target='#modal_dados_mdlsat".$vazio."'>";
                                                        echo "<td>".$vazio."</td>";
                                                        echo "<td>".$mdlsatversao."</td>";
                                                        echo "<td>".$mdlsatqntd."</td>";
                                                        echo "<td><button type='button' class='btn btn-outline-info btn-sm' data-toggle='modal' data-target='#modal_dados_mdlsat".$vazio."'>Visualizar</button></a></td>";
                                                    echo "</tr>";
                                                    echo "<div class='modal fade' id='modal_dados_mdlsat".$result_group_mdlsat['modelo_sat']."' tabindex='-1' role='dialog' aria-hidden='true'>";
                                                        echo "<div class='modal-dialog modal-dialog-scrollable modal-lg'>";
                                                            echo "<div class='modal-content bg-mattBlackLight px-3 py-3'>";
                                                                echo "<div class='modal-header'>";
                                                                    echo "<h5>Dados Modelo S@T - ".$result_group_mdlsat['modelo_sat']."</h5>";
                                                                    echo "<button type='button' class='close' data-dismiss='modal'>&times</button>";
                                                                echo "</div>";
                                                                echo "<div class='modal-header'>";
                                                                    echo '<a href="../export/export_sat_mdl.php?mdl='.$result_group_mdlsat['modelo_sat'].'"><button type="submit" class="btn btn-outline-danger">Exportar</button></a>';
                                                                echo "</div>";
                                                                echo "<div class='modal-body'>";
                                                                    echo "<form>";
                                                                        echo '<div class="form-group">';
                                                                            echo '<div class="row">';
                                                                                echo '<div class="col-md-2">S@T</div>';
                                                                                echo '<div class="col-md-3">Firmware</div>';
                                                                                echo '<div class="col-md-2">Loja</div>';
                                                                                echo '<div class="col-md-2">PDV</div>';
                                                                                echo '<div class="col-md-2">IP</div>';
                                                                            echo "</div>";
                                                                            echo '<div class="row">';
                                                                                $sql_mdlsatversao = 'SELECT * FROM tb_sat WHERE modelo_sat = "'.$result_group_mdlsat['modelo_sat'].'" AND Status = "Ativo" ORDER BY sat' ;
                                                                                $query_mdlsatversao = mysqli_query($conn, $sql_mdlsatversao);
                                                                                while ($result_mdlsatversao = mysqli_fetch_array($query_mdlsatversao))
                                                                                {
                                                                                    $mdlsatnloja = $result_mdlsatversao['sat'];
                                                                                    $mdlsatnversao = $result_mdlsatversao['firmware'];
                                                                                    $mdlsatntpvs = $result_mdlsatversao['loja'];
                                                                                    $mdlsatntpv = $result_mdlsatversao['caixa'];
                                                                                    $dmlsatnip = $result_mdlsatversao['ip'];

                                                                                    echo '<div class="col-md-2">'.$mdlsatnloja.'</div>';
                                                                                    echo '<div class="col-md-3">'.$mdlsatnversao.'</div>';
                                                                                    echo '<div class="col-md-2">'.$mdlsatntpvs.'</div>';
                                                                                    echo '<div class="col-md-2">'.$mdlsatntpv.'</div>';
                                                                                    echo '<div class="col-md-2">'.$dmlsatnip.'</div>'; 
                                                                                }
                                                                            echo '</div>';
                                                                        echo '</div>';
                                                                    echo "</form>";
                                                                echo "</div>";
                                                            echo "</div>";
                                                        echo "</div>";
                                                    echo "</div>";
                                                    $i++;

                                                }
                                                echo "</tbody>";
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="bg-mattBlackLight px-3 py-2">
                                    <div class="table-responsive">
                                        <table class="table table-sm table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Modelo S@T</th>
                                                    <th>Ano</th>
                                                    <th>Quantidade</th>
                                                    <th>Visualizar</th>
                                                </tr>
                                            </thead>
                                            <?php
                                                $sql_group_dtsat = "SELECT YEAR(data_fim_ativacao) AS ano,modelo_sat, COUNT(id) AS qntd FROM tb_sat WHERE Status = 'Ativo' GROUP BY YEAR(data_fim_ativacao)" ;
                                                $query_group_dtsat = mysqli_query($conn, $sql_group_dtsat);
                                                echo "<tbody>";
                                                $i=1;
                                                while ($result_group_dtsat = mysqli_fetch_array($query_group_dtsat))
                                                {
                                                    $vazio = $i;
                                                    $dtsatversao = $result_group_dtsat['modelo_sat'];
                                                    $dtsatano = $result_group_dtsat['ano'];
                                                    $dtsatqntd = $result_group_dtsat['qntd'];

                                                    echo "<tr data-toggle='modal' data-target='#modal_dados_dtsat".$result_group_dtsat['ano']."'>";
                                                        echo "<td>".$vazio."</td>";
                                                        echo "<td>".$dtsatversao."</td>";
                                                        echo "<td>".$dtsatano."</td>";
                                                        echo "<td>".$dtsatqntd."</td>";
                                                        echo "<td><button type='button' class='btn btn-outline-info btn-sm' data-toggle='modal' data-target='#modal_dados_dtsat".$result_group_dtsat['ano']."'>Visualizar</button></a></td>";
                                                    echo "</tr>";
                                                    echo "<div class='modal fade' id='modal_dados_dtsat".$result_group_dtsat['ano']."' tabindex='-1' role='dialog' aria-hidden='true'>";
                                                        echo "<div class='modal-dialog modal-dialog-scrollable modal-lg'>";
                                                            echo "<div class='modal-content bg-mattBlackLight px-3 py-3'>";
                                                                echo "<div class='modal-header'>";
                                                                    echo "<h5>Dados Modelo S@T - ".$result_group_dtsat['ano']."</h5>";
                                                                    echo "<button type='button' class='close' data-dismiss='modal'>&times</button>";
                                                                echo "</div>";
                                                                echo "<div class='modal-header'>";
                                                                    echo '<a href="../export/export_sat_vencimento.php?venc='.$result_group_dtsat['ano'].'"><button type="submit" class="btn btn-outline-danger">Exportar</button></a>';
                                                                echo "</div>";
                                                                echo "<div class='modal-body'>";
                                                                    echo "<form>";
                                                                        echo '<div class="form-group">';
                                                                            echo '<div class="row">';
                                                                                echo '<div class="col-md-2">S@T</div>';
                                                                                echo '<div class="col-md-3">Firmware</div>';
                                                                                echo '<div class="col-md-2">Loja</div>';
                                                                                echo '<div class="col-md-2">PDV</div>';
                                                                                echo '<div class="col-md-2">IP</div>';
                                                                            echo "</div>";
                                                                            echo '<div class="row">';
                                                                                $sql_fwsatversao = 'SELECT * FROM tb_sat WHERE YEAR(data_fim_ativacao) = "'.$result_group_dtsat['ano'].'" AND Status = "Ativo"' ;
                                                                                $query_fwsatversao = mysqli_query($conn, $sql_fwsatversao);
                                                                                while ($result_fwsatversao = mysqli_fetch_array($query_fwsatversao))
                                                                                {
                                                                                    $fwsatnloja = $result_fwsatversao['sat'];
                                                                                    $fwsatnversao = $result_fwsatversao['firmware'];
                                                                                    $fwsatntpvs = $result_fwsatversao['loja'];
                                                                                    $fwsatntpv = $result_fwsatversao['caixa'];
                                                                                    $fwsatnip = $result_fwsatversao['ip'];

                                                                                    echo '<div class="col-md-2">'.$fwsatnloja.'</div>';
                                                                                    echo '<div class="col-md-3">'.$fwsatnversao.'</div>';
                                                                                    echo '<div class="col-md-2">'.$fwsatntpvs.'</div>';
                                                                                    echo '<div class="col-md-2">'.$fwsatntpv.'</div>';
                                                                                    echo '<div class="col-md-2">'.$fwsatnip.'</div>'; 
                                                                                }
                                                                            echo '</div>';
                                                                        echo '</div>';
                                                                    echo "</form>";
                                                                echo "</div>";
                                                            echo "</div>";
                                                        echo "</div>";
                                                    echo "</div>";
                                                    $i++;

                                                }
                                                echo "</tbody>";
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="my-3">
                        </div>
                        <div class="my-3">
                            <div class="bg-mattBlackLight px-3 py-1">
                                <h4>Dados Impressoras</h4>
                            </div>
                        </div>
                        <div class='row'>
                            <div class="col-sm-4">
                                <div class="bg-mattBlackLight px-3 py-2">
                                    <div class="table-responsive">
                                        <table class="table table-sm table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Impressora</th>
                                                    <th>Quantidade</th>
                                                    <th>Visualizar</th>
                                                </tr>
                                            </thead>
                                            <?php
                                                $sql_group_impressora = "SELECT impressora, COUNT(id) AS qntd FROM tb_consolidado_equipamentos WHERE Data_Modificacao BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() GROUP BY impressora" ;
                                                $query_group_impressora = mysqli_query($conn, $sql_group_impressora);
                                                echo "<tbody>";
                                                $i=1;
                                                while ($result_group_impressora = mysqli_fetch_array($query_group_impressora))
                                                {
                                                    $vazio = $i;
                                                    $mdlprinter = $result_group_impressora['impressora'];
                                                    $qntdprinter = $result_group_impressora['qntd'];

                                                    echo "<tr data-toggle='modal' data-target='#modal_dados_mdlprt".$vazio."'>";
                                                        echo "<td>".$vazio."</td>";
                                                        echo "<td>".$mdlprinter."</td>";
                                                        echo "<td>".$qntdprinter."</td>";
                                                        echo "<td><button type='button' class='btn btn-sm btn-outline-info' data-toggle='modal' data-target='#modal_dados_mdlprt".$vazio."'>Visualizar</button></a></td>";
                                                    echo "</tr>";
                                                    echo "<div class='modal fade' id='modal_dados_mdlprt".$vazio."' tabindex='-1' role='dialog' aria-hidden='true'>";
                                                        echo "<div class='modal-dialog modal-dialog-scrollable modal-lg'>";
                                                            echo "<div class='modal-content bg-mattBlackLight px-3 py-3'>";
                                                                echo "<div class='modal-header'>";
                                                                    echo "<h5>Dados Impressora - ".$result_group_impressora['impressora']."</h5>";
                                                                    echo "<button type='button' class='close' data-dismiss='modal'>&times</button>";
                                                                echo "</div>";
                                                                echo "<div class='modal-header'>";
                                                                    echo '<a href="../export/export_impressora_mdl.php?mdl='.$result_group_impressora['impressora'].'"><button type="submit" class="btn btn-outline-danger">Exportar</button></a>';
                                                                echo "</div>";
                                                                echo "<div class='modal-body'>";
                                                                    echo "<form>";
                                                                        echo '<div class="form-group">';
                                                                            echo '<div class="row">';
                                                                                echo '<div class="col-md-2">Impressora</div>';
                                                                                echo '<div class="col-md-3">Firmware</div>';
                                                                                echo '<div class="col-md-2">Loja</div>';
                                                                                echo '<div class="col-md-1">PDV</div>';
                                                                                echo '<div class="col-md-3">Última Atualização</div>';
                                                                            echo "</div>";
                                                                            echo '<div class="row">';
                                                                                $sql_printerversao = 'SELECT impressora, firmware, loja, pdv, data_modificacao FROM tb_consolidado_equipamentos WHERE Data_Modificacao BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY)AND CURDATE() AND impressora = "'.$result_group_impressora['impressora'].'" ORDER BY data_modificacao DESC' ;
                                                                                $query_printerversao = mysqli_query($conn, $sql_printerversao);
                                                                                while ($result_printerversao = mysqli_fetch_array($query_printerversao))
                                                                                {
                                                                                    $printernprinter = $result_printerversao['impressora'];
                                                                                    $printernversao = $result_printerversao['firmware'];
                                                                                    $printernloja = $result_printerversao['loja'];
                                                                                    $printerntpv = $result_printerversao['pdv'];
                                                                                    $printerndt = $result_printerversao['data_modificacao'];

                                                                                    echo '<div class="col-md-2">'.$printernprinter.'</div>';
                                                                                    echo '<div class="col-md-3">'.$printernversao.'</div>';
                                                                                    echo '<div class="col-md-2">'.$printernloja.'</div>';
                                                                                    echo '<div class="col-md-1">'.$printerntpv.'</div>';
                                                                                    echo '<div class="col-md-3">'.$printerndt.'</div>'; 
                                                                                }
                                                                            echo '</div>';
                                                                        echo '</div>';
                                                                    echo "</form>";
                                                                echo "</div>";
                                                            echo "</div>";
                                                        echo "</div>";
                                                    echo "</div>";
                                                    $i++;

                                                }
                                                echo "</tbody>";
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="bg-mattBlackLight px-3 py-2">
                                    <div class="table-responsive">
                                        <table class="table table-sm table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Firmware</th>
                                                    <th>Quantidade</th>
                                                    <th>Visualizar</th>
                                                </tr>
                                            </thead>
                                            <?php
                                                $sql_group_fwimpressora = "SELECT firmware, COUNT(id) AS qntd FROM tb_consolidado_equipamentos WHERE Data_Modificacao BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() GROUP BY firmware" ;
                                                $query_group_fwimpressora = mysqli_query($conn, $sql_group_fwimpressora);
                                                echo "<tbody>";
                                                $i=1;
                                                while ($result_group_fwimpressora = mysqli_fetch_array($query_group_fwimpressora))
                                                {
                                                    $vazio = $i;
                                                    $fwprinter = $result_group_fwimpressora['firmware'];
                                                    $fwqntdprinter = $result_group_fwimpressora['qntd'];

                                                    echo "<tr data-toggle='modal' data-target='#modal_dados_fwprt".$vazio."'>";
                                                        echo "<td>".$vazio."</td>";
                                                        echo "<td>".$fwprinter."</td>";
                                                        echo "<td>".$fwqntdprinter."</td>";
                                                        echo "<td><button type='button' class='btn btn-sm btn-outline-info' data-toggle='modal' data-target='#modal_dados_fwprt".$vazio."'>Visualizar</button></a></td>";
                                                    echo "</tr>";
                                                    echo "<div class='modal fade' id='modal_dados_fwprt".$vazio."' tabindex='-1' role='dialog' aria-hidden='true'>";
                                                        echo "<div class='modal-dialog modal-dialog-scrollable modal-lg'>";
                                                            echo "<div class='modal-content bg-mattBlackLight px-3 py-3'>";
                                                                echo "<div class='modal-header'>";
                                                                    echo "<h5>Dados Impressora - ".$result_group_fwimpressora['firmware']."</h5>";
                                                                    echo "<button type='button' class='close' data-dismiss='modal'>&times</button>";
                                                                echo "</div>";
                                                                echo "<div class='modal-header'>";
                                                                    echo '<a href="../export/export_impressora_fw.php?mdl='.$result_group_fwimpressora['firmware'].'"><button type="submit" class="btn btn-outline-danger">Exportar</button></a>';
                                                                echo "</div>";
                                                                echo "<div class='modal-body'>";
                                                                    echo "<form>";
                                                                        echo '<div class="form-group">';
                                                                            echo '<div class="row">';
                                                                                echo '<div class="col-md-2">Impressora</div>';
                                                                                echo '<div class="col-md-3">Firmware</div>';
                                                                                echo '<div class="col-md-2">Loja</div>';
                                                                                echo '<div class="col-md-1">PDV</div>';
                                                                                echo '<div class="col-md-3">Última Atualização</div>';
                                                                            echo "</div>";
                                                                            echo '<div class="row">';
                                                                                $sql_fwprinterversao = 'SELECT impressora, firmware, loja, pdv, data_modificacao FROM tb_consolidado_equipamentos WHERE Data_Modificacao BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() AND firmware = "'.$result_group_fwimpressora['firmware'].'" ORDER BY data_modificacao DESC' ;
                                                                                $query_fwprinterversao = mysqli_query($conn, $sql_fwprinterversao);
                                                                                while ($result_fwprinterversao = mysqli_fetch_array($query_fwprinterversao))
                                                                                {
                                                                                    $fwprinternprinter = $result_fwprinterversao['impressora'];
                                                                                    $fwprinternversao = $result_fwprinterversao['firmware'];
                                                                                    $fwprinternloja = $result_fwprinterversao['loja'];
                                                                                    $fwprinterntpv = $result_fwprinterversao['pdv'];
                                                                                    $fwprinterndt = $result_fwprinterversao['data_modificacao'];

                                                                                    echo '<div class="col-md-2">'.$fwprinternprinter.'</div>';
                                                                                    echo '<div class="col-md-3">'.$fwprinternversao.'</div>';
                                                                                    echo '<div class="col-md-2">'.$fwprinternloja.'</div>';
                                                                                    echo '<div class="col-md-1">'.$fwprinterntpv.'</div>';
                                                                                    echo '<div class="col-md-3">'.$fwprinterndt.'</div>'; 
                                                                                }
                                                                            echo '</div>';
                                                                        echo '</div>';
                                                                    echo "</form>";
                                                                echo "</div>";
                                                            echo "</div>";
                                                        echo "</div>";
                                                    echo "</div>";
                                                    $i++;

                                                }
                                                echo "</tbody>";
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        <script src="../js/popper.min.js"></script>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/jquery.slimscroll.min.js"></script>
        <script src="../js/script.js"></script>
        <script src="../js/feather.min.js"></script>
        <script>
            feather.replace()
        </script>

        <script src="../js/bootstrap.min.js"></script>
    </body>
</html>
