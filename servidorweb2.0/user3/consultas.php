<?php
    include_once("../security/seguranca.php");
	protegePagina();
	$id = $_SESSION['usuarioID'];
	$result_usuario = "SELECT * FROM tb_usuarios WHERE id = '$id'";
	$resultado_usuario = mysqli_query($conn, $result_usuario);
	$row_usuario = mysqli_fetch_assoc($resultado_usuario);
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie-edge" />
        <link rel="icon" href="../img/favicon.ico" />
        <title>Suport TPVs | User</title>
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css//material-icons.min.css" />
        <link rel="stylesheet" href="../css/style.css" />
    </head>

    <body>
        <?php
            date_default_timezone_set("America/Sao_Paulo");
            setlocale(LC_ALL, 'pt_BR');
            $num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
            //Obter a data atual
            $resultado_qnt_cadastros = mysqli_query($conn, $num_users);
            $row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
            
            $data['atual'] = date('Y-m-d H:i:s'); 

            //Diminuir 20 segundos 
            $data['online'] = strtotime($data['atual'] . " - 20 seconds");
            $data['online'] = date("Y-m-d H:i:s",$data['online']);
            
            //Pesquisar os ultimos usuarios online nos 20 segundo
            $result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
            
            $resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
            $row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
            
            $qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
            $qnt_perc = round((($row_qnt_visitas['online'] / $row_qnt_cadastros['cadastrado'])*100),2);
        ?>    
        <script src="../js/jquery-3.2.1.min.js"></script>    
        <script>
            //Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
            setInterval(function(){
            //Incluir e enviar o POST para o arquivo responsável em fazer contagem
            $.post("../processo/processa_vis.php", {contar: '',}, function(data){
                $('#online').text(data);
            });
            }, 10000);
        </script>
        <nav class="navbar navbar-expand-lg navbar-dark bg-mattBlackLight fixed-top">
            <button class="navbar-toggler sideMenuToggler" type="button">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="user2.php">
                User - <?php echo $_SESSION['usuarioNome'];?>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons icon">
                                person
                            </i>
                            <span class="text">
                                Account
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaDadosPessoais">
                                <span data-feather="info"></span> Pefil</a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaSenha">
                                <span data-feather="lock"></span> Alterar senha</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="../security/sair.php">
                                <span data-feather="share"></span> Log Out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="wrapper d-flex">
            <div class="sideMenu bg-mattBlackLight">
                <div class="sidebar">
				<ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="user.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    home
                                </i>
                                <span class="text">
                                    Home
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="consultas.php" class="nav-link px-2 active">
                                <i class="material-icons icon">
                                    search
                                </i>
                                <span class="text">
                                    Consultas
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="srv_chaves.php" class="nav-link px-2">
                                <i class="material-icons icon">
                                    dns
                                </i>
                                <span class="text">
                                    Servidor de Chaves
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link px-2 sideMenuToggler">
                                <i class="material-icons icon expandView ">
                                    view_list
                                </i>
                                <span class="text">
                                    Ocultar
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="content">

                <main>
                    <form name="frm_senha" action="../processo/salva_senha.php" method="POST">
                        <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content bg-mattBlackLight px-3 py-3">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>  
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                                                <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                                                <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-primary">Confirma</button>
                                    </div>
                                </div>
                            </div>
                        </div>
					</form>
					<form name="frm_dados_pessoais" action="../processo/atualizar_usuario.php" method="POST">
                        <div class="modal fade" id="editaDadosPessoais" tabindex="-1" role="dialog" aria-labelledby="editaDadosPessoaisLabel" aria-hidden="true">    
                            <div class="modal-dialog" role="document">          
                                <!-- Modal content-->      
                                <div class="modal-content bg-mattBlackLight">        
                                    <div class="modal-header">          
                                        <h4 class="modal-title texto-modal text-center">Dados Pessoais</h4>        
                                    </div>        
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="text">ID </label>
                                                <input for="text" class="form-control" name="id" value="<?php echo $row_usuario['id'] ?>" readonly>
                                                <label for="text">Nome </label>
                                                <input type="text" class="form-control" name="nome" value="<?php echo $row_usuario['nome']; ?>" >
                                                <label for="email">E-mail </label>
                                                <input type="email" class="form-control" name="email" value="<?php echo $row_usuario['email']; ?>">
                                                <label for="text">Login </label>
                                                <input type="text" class="form-control" name="login" value="<?php echo $row_usuario['login']; ?>" readonly>
                                            </div>
                                        </form>        
                                    </div>        
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-outline-success">Atualizar</button>        
                                    </div>      
                                </div> 
                            </div>
                        </div>
                    </form>
                    <div class="container-fluid">
						<form name="consulta" action="consultas.php" method="POST">
							<div class="row">
								<div class="col-md-4 my-3">
									<div class="bg-mattBlackLight px-3 py-3">
										<h6 class="mb-2">Selecione o tipo da pesquisa</h6>		
										<select name="cb_consulta" class="custom-select ">
											<option value="vazio"></option>
											<option value="loja"> Loja</option>
											<option value="sat"> SAT</option>  
										</select>
									</div>
								</div>
								<div class="col-md-4 my-3">
									<div class="bg-mattBlackLight px-3 py-3">
										<h6 class="mb-2">Informe o valor a ser pesquisado</h6>
										<input class="form-control form-control-dark w-100" name="txt_consulta"  type="text"  placeholder="Pesquisar: Loja/SAT"  aria-label="search" >
									</div>
								</div>
							</div>
						</form>
					</div>
							        
				<?php
				if(empty($_POST['cb_consulta'])){
					
				}else{
							$metodo_consulta = $_POST['cb_consulta'];
						if ( $metodo_consulta == "loja")
							{
								echo '<div class="container-fluid">';
								echo "<div class='row'>";
								echo "<div class='col-sm-6 my-3'>";
								echo '<div class="bg-mattBlackLight px-3 py-2">';
								echo '<div class="table-responsive">';
								echo '<table class="table table-sm table-striped table-bordered table-hover">';
								echo '<thead>';
								echo "<tr>";
								echo "<th>Loja</th>";
								echo "<th>QNTD PDVs</th>";
								echo "<th>Versao</th>";
								echo "<th>IP</th>";
								echo "</tr>";
																									
									$n_loja = $_POST['txt_consulta'];
									$nu_loja = str_pad($n_loja,5,'0', STR_PAD_LEFT);

										// Conectando ao banco de dados:
										$strconn = mysqli_connect('localhost','root','diabrasil','srvremoto') or die('Erro ao conectar ao banco de dados');
										$sql = "SELECT Loja, Qntd_Tpvs, Versao, IP FROM tb_dados_loja WHERE Loja ='$nu_loja'";
										$result = mysqli_query($strconn,$sql) or die("Erro ao retornar dados");
										// Obtendo os dados por meio de um loop while
										echo "<tbody>";
										while ($reg = mysqli_fetch_array($result))
										{
											$loja = $reg['Loja'];
											$qntd_tpvs = $reg['Qntd_Tpvs'];
											$versao = $reg['Versao'];
											$ip = $reg['IP'];
												echo "<center>";
												echo "<tr>";
												echo "<td>".$loja."</td>";
												echo "<td>".$qntd_tpvs."</td>";
												echo "<td>".$versao."</td>";
												echo "<td>".$ip."</td>";
												echo "</tr>";
												echo "</center>";
												
											}
											echo "</tbody>";	
											mysqli_close($strconn);
											echo "</thead>";
											echo "</table>";
											echo "</div>";
											echo "</div>";
											echo "</div>";
											echo "</div>";
											echo "</div>";
								
								
								//Criando tabela e cabeçalho de dados:
								echo '<div class="container-fluid">';
								echo "<div class='row'>";
								echo "<div class='col-sm-12'>";
								echo '<div class="bg-mattBlackLight px-3 py-3">';
								echo '<div class="table-responsive">';
								echo '<table class="table table-sm table-striped table-bordered table-hover" font-size="small">';
								echo '<thead>';
								echo "<tr>";
								echo "<th>SAT</th>";
								echo "<th>Loja</th>";
								echo "<th>PDV</th>";
								echo "<th>IP</th>";
								echo "<th>MASK</th>";
								echo "<th>GW</th>";
								echo "<th>DNS 1</th>";
								echo "<th>DNS 2</th>";
								echo "<th>Firmware</th>";
								echo "<th>Layout</th>";
								echo "<th>Usado</th>";
								echo "<th>Ativacao</th>";			
								echo "<th>Fim Ativacao</th>";
								echo "</tr>";

								$nume_loja = $_POST['txt_consulta'];
								$num_loja = str_pad($nume_loja,5,'0', STR_PAD_LEFT);
								// Conectando ao banco de dados:
								$strcon = mysqli_connect('localhost','root','diabrasil','srvremoto') or die('Erro ao conectar ao banco de dados');
								$sql = "SELECT sat, loja, caixa, ip, mask, gw, dns_1, dns_2, firmware, layout, disco, disco_usado, data_ativacao, data_fim_ativacao, status FROM tb_sat WHERE loja ='$num_loja' AND status='Ativo' ORDER BY caixa";
								$resultado = mysqli_query($strcon,$sql) or die("Erro ao retornar dados");
								// Obtendo os dados por meio de um loop while

									echo "<tbody>";
									$i=1;
									while ($registro = mysqli_fetch_array($resultado))
									{
										if($i <= $qntd_tpvs){
											$sat = $registro['sat'];
											$loja = $registro['loja'];
											$caixa = $registro['caixa'];
											$ip = $registro['ip'];
											$mascara = $registro['mask'];
											$geteway = $registro['gw'];
											$dns_1 = $registro['dns_1'];
											$dns_2 = $registro['dns_2'];
											$firmware = $registro['firmware'];
											$layout = $registro['layout'];
											$disco = $registro['disco'];
											$disco_usado = $registro['disco_usado'];
											$data_ativacao = $registro['data_ativacao'];
											$data_fim_ativacao = $registro['data_fim_ativacao'];
											$status = $registro['status'];
											echo "<tr>";
											echo "<td>".$sat."</td>";
											echo "<td>".$loja."</td>";
											echo "<td>".$caixa."</td>";
											echo "<td>".$ip."</td>";
											echo "<td>".$mascara."</td>";
											echo "<td>".$geteway."</td>";
											echo "<td>".$dns_1."</td>";
											echo "<td>".$dns_2."</td>";
											echo "<td>".$firmware."</td>";
											echo "<td>".$layout."</td>";
											echo "<td>".$disco_usado."</td>";
											echo "<td>".$data_ativacao."</td>";
											echo "<td>".$data_fim_ativacao."</td>";
											echo "</tr>";
										}
										$i++;
										
									}
										echo "</tbody>";
										echo "</thead>";
										mysqli_close($strcon);
										echo "</table>";
										echo "</div>";
										echo "</div>";
										echo "</div>";
										echo "</div>";
										echo "</div>";
																										
						//
								echo '<div class="container-fluid">';
									echo "<div class='row'>";
										echo "<div class='col-md-4 my-3'>";
											echo '<div class="bg-mattBlackLight px-3 py-3">';
												echo '<div class="table-responsive">';
													echo '<table class="table table-sm table-striped table-bordered table-hover">';
														echo '<thead>';
															echo "<tr>";
																echo "<th>Loja</th>";
																echo "<th>PDV</th>";
																echo "<th>Impressora</th>";
																echo "<th>Firmware</th>";
																echo "<th>CPU</th>";
															echo "</tr>";
																										
															$n_loja = $_POST['txt_consulta'];
															$nu_loja = str_pad($nume_loja,5,'0', STR_PAD_LEFT);

															// Conectando ao banco de dados:
															$strconn = mysqli_connect('localhost','root','diabrasil','srvremoto') or die('Erro ao conectar ao banco de dados');
															$sql = "SELECT Loja, PDV, Impressora, Firmware,CPU FROM tb_consolidado_equipamentos WHERE Loja ='$nu_loja' ORDER BY PDV";
															$result = mysqli_query($strconn,$sql) or die("Erro ao retornar dados");
															// Obtendo os dados por meio de um loop while
															echo "<tbody>";
															$i=1;
															while ($reg = mysqli_fetch_array($result))
															{
																if($i <= $qntd_tpvs){
																	$loja = $reg['Loja'];
																	$pdv = $reg['PDV'];
																	$imp = $reg['Impressora'];
																	$frw = $reg['Firmware'];
																	$cpu = $reg['CPU'];
																	
																	echo "<tr>";
																		echo "<td>".$loja."</td>";
																		echo "<td>".$pdv."</td>";
																		echo "<td>".$imp."</td>";
																		echo "<td>".$frw."</td>";
																		echo "<td>".$cpu."</td>";
																	echo "</tr>";
																}
																$i++;
															}	
															echo "</tbody>";
															mysqli_close($strconn);
														echo "</thead>";
													echo "</table>";
												echo "</div>";
											echo "</div>";
											echo "</div>";
											echo "<div class='col-md-5 my-3'>";
												echo '<div class="bg-mattBlackLight px-3 py-3">';
													echo '<div class="table-responsive">';
														echo '<table class="table table-sm table-striped table-bordered table-hover">';
															echo '<thead>';
																echo "<tr>";
																echo "<th>Loja</th>";
																echo "<th>PDV</th>";
																echo "<th>Cod Empresa</th>";
																echo "<th>Cod Filial</th>";
																echo "<th>Cod PDV</th>";
															echo "</tr>";

															$n_loja = $_POST['txt_consulta'];
															$nu_loja = str_pad($nume_loja,5,'0', STR_PAD_LEFT);

															// Conectando ao banco de dados:
															$strconn = mysqli_connect('localhost','root','diabrasil','srvremoto') or die('Erro ao conectar ao banco de dados');
															$sql = "SELECT Loja, Caixa, Cod_Empresa, Filial, Cod_Pdv FROM tb_dados_tef WHERE Loja ='$nu_loja'";
															$result = mysqli_query($strconn,$sql) or die("Erro ao retornar dados");
															// Obtendo os dados por meio de um loop while
															echo "<tbody>";
															$i=1;
															while ($reg = mysqli_fetch_array($result))
															{
																if($i <= $qntd_tpvs){
																	$loja = $reg['Loja'];
																	$caixa = $reg['Caixa'];
																	$cod_empresa = $reg['Cod_Empresa'];
																	$cod_filial = $reg['Filial'];
																	$cod_pdv = $reg['Cod_Pdv'];
																		
																	echo "<tr>";
																		echo "<td>".$loja."</td>";
																		echo "<td>".$caixa."</td>";
																		echo "<td>".$cod_empresa."</td>";
																		echo "<td>".$cod_filial."</td>";
																		echo "<td>".$cod_pdv."</td>";
																	echo "</tr>";
																}
																$i++;
															}	
															echo "</tbody>";
														echo "</thead>";
														mysqli_close($strconn);
													echo "</table>";
												echo "</div>";
											echo "</div>";
										echo "</div>";
									echo "</div>";
											
							}else{
								$metodo_consulta = $_POST['cb_consulta'];
								
								if( $metodo_consulta == "sat")
									
									{
																								
										//Criando tabela e cabeçalho de dados:
										echo '<div class="container-fluid">';
											echo '<div class="row">';
												echo "<div class='col-sm-12'>";
													echo '<div class="bg-mattBlackLight px-3 py-3">';
														echo '<div class="table-responsive">';
															echo '<table class="table table-sm table-striped table-bordered table-hover font-size="small">';
																echo '<thead>';
																	echo "<tr>";
																		echo "<th>SAT</th>";
																		echo "<th>Loja</th>";
																		echo "<th>PDV</th>";
																		echo "<th>IP</th>";
																		echo "<th>MASK</th>";
																		echo "<th>GW</th>";
																		echo "<th>DNS 1</th>";
																		echo "<th>DNS 2</th>";
																		echo "<th>Firmware</th>";
																		echo "<th>Layout</th>";
																		echo "<th>Usado</th>";
																		echo "<th>Ativacao</th>";
																		echo "<th>Fim Ativacao</th>";
																	echo "</tr>";
																echo "</thead>";	 
																$nume_sat = $_POST['txt_consulta'];
																$num_sat = str_pad($nume_sat,9,'0', STR_PAD_LEFT);
																// Conectando ao banco de dados:
																$strcon = mysqli_connect('localhost','root','diabrasil','srvremoto') or die('Erro ao conectar ao banco de dados');
																$sql = "SELECT sat, loja, caixa, ip, mask, gw, dns_1, dns_2, firmware, layout, disco, disco_usado, data_ativacao, data_fim_ativacao, status FROM tb_sat WHERE sat ='$num_sat' AND Status = 'Ativo' ORDER BY loja ";
																$resultado = mysqli_query($strcon,$sql) or die("Erro ao retornar dados");
																// Obtendo os dados por meio de um loop while
																echo "<tbody>";
																while ($registro = mysqli_fetch_array($resultado))
																{
																	$sat = $registro['sat'];
																	$loja = $registro['loja'];
																	$caixa = $registro['caixa'];
																	$ip = $registro['ip'];
																	$mascara = $registro['mask'];
																	$geteway = $registro['gw'];
																	$dns_1 = $registro['dns_1'];
																	$dns_2 = $registro['dns_2'];
																	$firmware = $registro['firmware'];
																	$layout = $registro['layout'];
																	$disco = $registro['disco'];
																	$disco_usado = $registro['disco_usado'];
																	$data_ativacao = $registro['data_ativacao'];
																	$data_fim_ativacao = $registro['data_fim_ativacao'];
																	$status = $registro['status'];
																	echo "<tr>";
																		echo "<td>".$sat."</td>";
																		echo "<td>".$loja."</td>";
																		echo "<td>".$caixa."</td>";
																		echo "<td>".$ip."</td>";
																		echo "<td>".$mascara."</td>";
																		echo "<td>".$geteway."</td>";
																		echo "<td>".$dns_1."</td>";
																		echo "<td>".$dns_2."</td>";
																		echo "<td>".$firmware."</td>";
																		echo "<td>".$layout."</td>";
																		echo "<td>".$disco_usado."</td>";
																		echo "<td>".$data_ativacao."</td>";
																		echo "<td>".$data_fim_ativacao."</td>";
																	echo "</tr>";
																}
											
																echo "</tbody>";
																mysqli_close($strcon);
															echo "</table>";
														echo "</div>";
													echo "</div>";
												echo "</div>";
											echo "</div>";
										echo "</div>";
									}
								}
							}	
						?>
                </main>
            </div>
        </div>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/jquery.slimscroll.min.js"></script>
        <script src="../js/script.js"></script>
        <script src="../js/feather.min.js"></script>
        <script>
        feather.replace()
        </script>
    </body>
</html>
