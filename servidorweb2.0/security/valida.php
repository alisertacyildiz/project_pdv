<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicon.ico">

    <title>Support TPVs</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../signin.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../js/ie-emulation-modes-warning.js"></script>
</head>

<body class="text-center">
<?php
//Incluimos o codigo de sistema de sedgurança
include_once("seguranca.php");
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    //Salva duas variaveis com o que foi digitado no formulario do login
    //Detalhe: faz uma verificação utilizando a função isset() para saber se o campo foi preenchido
    $usuariot = (isset($_POST['txt_usuario'])) ? $_POST['txt_usuario'] : '';
    $senhat = (isset($_POST['txt_senha'])) ? $_POST['txt_senha'] : '';
    $remembert = (isset($_POST['lembrete'])) ? $_POST['lembrete'] : '';
	
    //Utilizamos uma função criada no seguranca.php para valida os dados digitados
    if (validaUsuario($usuariot,$senhat) == true){
        //Usuarios e a senha foram validados, manda para pagina respectiva ao nivel
        //de acesso do usuário, também gavamos um varável global com o resultado.
        if($remembert == 'rememberme')
        {
            $expira = time() + 60*60*24*30;
            setcookie('cookieLembreme', base64_encode('rememberme'), $expira);
            setcookie('cookieUsuario', base64_encode($usuariot), $expira);
            setcookie('cookieSenha', base64_encode($senhat), $expira);
        }else{
            setcookie('cookieLembreme');
            setcookie('cookieUsuario');
            setcookie('cookieSenha');
        }
        $_SESSION['resultado'] = "Usuario conectado com sucesso!";
        if ($_SESSION['usuarioNivel'] == 1){
            header("Location: admin/admin.php");
    
        }elseif($_SESSION['usuarioNivel'] == 2){
            header("Location: user2/user2.php");
		
        }elseif($_SESSION['usuarioNivel'] == 3){
			header("Location: user/user.php");
		}else{
            header("Location: user3/user.php");
        }
    }else{
        //O usuario e/ou senha são inválidos, manda de volta para o form login
        //Chamados a dentro da pagina de seguranca.php
            //expulsaVisitante();
        $_SESSION['resultado'] = "Erro ao executar comando, usuario ou senha invalidos!";
		echo '<div class="alert alert-danger" role="alert">';
        echo " <strong>Ops! Usuário ou Senha inváidos.</strong>";
        echo "</div>";
				
    }
	
}
?>
</body>
</html>
