<?php
include_once("../security/seguranca.php");
protegePagina();
include_once("../security/conecta.php");

$pagina = filter_input(INPUT_POST, 'pagina', FILTER_SANITIZE_NUMBER_INT);
$qnt_result_pg = filter_input(INPUT_POST, 'qnt_result_pg', FILTER_SANITIZE_NUMBER_INT);
//calcular o inicio visualização
$inicio = ($pagina * $qnt_result_pg) - $qnt_result_pg;

//consultar no banco de dados
$result_ip_sat = "SELECT * FROM cn_sat_erro_ip LIMIT $inicio, $qnt_result_pg";
$resultado_ip_sat = mysqli_query($conn, $result_ip_sat);


//Verificar se encontrou resultado na tabela "usuarios"
if(($resultado_ip_sat) AND ($resultado_ip_sat->num_rows != 0)){
?>
    <div class="table-responsive">
        <table class="table table-sm table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Loja</th>
                    <th>Caixa</th>
                    <th>S@T</th>
                    <th>IP S@T</th>
                    <th>IP Loja</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    while($row_ip_sat = mysqli_fetch_assoc($resultado_ip_sat)){
                ?>
                    <tr>
                        <th><?php echo $row_ip_sat['loja']; ?></th>
                        <td><?php echo $row_ip_sat['caixa']; ?></td>
                        <td><?php echo $row_ip_sat['sat']; ?></td>
                        <td><?php echo $row_ip_sat['ip']; ?></td>
                        <td><?php echo $row_ip_sat['IPLoja']; ?></td>
                    </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
<nav aria-label="Page navigation exemple">
    <ul class="pagination pagination-sm justify-content-end">
        <?php

        //Paginação - Somar a quantidade de usuários
        $result_pg = "SELECT COUNT(loja) AS num_result FROM cn_sat_erro_ip";
        $resultado_pg = mysqli_query($conn, $result_pg);
        $row_pg = mysqli_fetch_assoc($resultado_pg);

        //Quantidade de pagina
        $quantidade_pg = ceil($row_pg['num_result'] / $qnt_result_pg);

        //Limitar os link antes depois
        $max_links = 2;
        if($pagina==1){
            echo "<li class='page-item disabled'>";
            echo "<a class='page-link' href='#' onclick='lista_ip_sat(1, $qnt_result_pg)'>Primeira</a>";    
        }else{
            echo "<li class='page-item'>";
            echo "<a class='page-link' href='#' onclick='lista_ip_sat(1, $qnt_result_pg)'>Primeira</a> ";
        }
        for($pag_ant = $pagina - $max_links; $pag_ant <= $pagina - 1; $pag_ant++){
            if($pag_ant >= 1){
                echo "<li class='page-item'>";
                echo "<a class='page-link' href='#' onclick='lista_ip_sat($pag_ant, $qnt_result_pg)'>$pag_ant</a>";
            }
        }
        echo "<li class='page-item disabled'>";
        echo "<a class='page-link'>$pagina</a> ";

        for ($pag_dep = $pagina + 1; $pag_dep <= $pagina + $max_links; $pag_dep++) {
            if($pag_dep <= $quantidade_pg){
                echo "<li class='page-item'>";
                echo "<a class='page-link' href='#' onclick='lista_ip_sat($pag_dep, $qnt_result_pg)'>$pag_dep</a> ";
            }
        }
        if($pagina==$quantidade_pg){
            echo "<li class='page-item disabled'>";
            echo "<a class='page-link' href='#' onclick='lista_ip_sat($quantidade_pg, $qnt_result_pg)'>Última</a>";
        }else{
            echo "<li class='page-item'>";
            echo "<a class='page-link' href='#' onclick='lista_ip_sat($quantidade_pg, $qnt_result_pg)'>Última</a>";
        }
        ?>
    </ul>
</nav>
<?php
    }
?>