<?php
   include_once("../security/seguranca.php");
   protegePagina();
   $venc = filter_input(INPUT_GET, 'venc', FILTER_SANITIZE_STRING);
	header( 'Content-type: application/csv' );   
	header( 'Content-Disposition: attachment; filename=export_sat_venc'.$venc.'.csv' );   
	header( 'Content-Transfer-Encoding: binary' );
	header( 'Pragma: no-cache');
	$pdo = new PDO( 'mysql:host=localhost;dbname=srvremoto', 'root', 'diabrasil' );
	$stmt = $pdo->prepare( 'SELECT sat, loja, caixa, ip, mask, gw, dns_1, dns_2, firmware, layout, disco, disco_usado, data_ativacao, data_fim_ativacao, modelo_sat,status FROM tb_sat WHERE YEAR(data_fim_ativacao) = "'.$venc.'" AND Status = "Ativo" ORDER BY data_fim_ativacao ');   
	$stmt->execute();
	$results = $stmt->fetchAll( PDO::FETCH_ASSOC );

	$out = fopen( 'php://output', 'w' );
	foreach ( $results as $result ) 
	{
		fputcsv( $out, $result );
	}
	fclose( $out );
?>
