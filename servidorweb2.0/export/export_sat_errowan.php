<?php
    include_once("../security/seguranca.php");
    protegePagina();
    $errwan = filter_input(INPUT_GET, 'errwan', FILTER_SANITIZE_STRING);
    header( 'Content-type: application/csv' );   
    header( 'Content-Disposition: attachment; filename=export_'.$errwan.'.csv' );   
    header( 'Content-Transfer-Encoding: binary' );
    header( 'Pragma: no-cache');

    $pdo = new PDO( 'mysql:host=localhost;dbname=srvremoto', 'root', 'diabrasil' );
    $stmt = $pdo->prepare( 'SELECT sat, loja, caixa, ipsat, disco_usado, status_wan, iploja FROM cn_sat_st_wan;' );   
    $stmt->execute();
    $results = $stmt->fetchAll( PDO::FETCH_ASSOC );

    $out = fopen( 'php://output', 'w' );
    foreach ( $results as $result ) 
    {
        fputcsv( $out, $result );
    }
    fclose( $out );
?>