<?php
   include_once("../security/seguranca.php");
   protegePagina();
   $fw = filter_input(INPUT_GET, 'fw', FILTER_SANITIZE_STRING);
   header( 'Content-type: application/csv' );   
   header( 'Content-Disposition: attachment; filename=export_sat_fw'.$fw.'.csv' );   
   header( 'Content-Transfer-Encoding: binary' );
   header( 'Pragma: no-cache');

   $pdo = new PDO( 'mysql:host=localhost;dbname=srvremoto', 'root', 'diabrasil' );
   $stmt = $pdo->prepare( 'SELECT sat, loja, caixa, ip, mask, gw, dns_1, dns_2, firmware, layout, disco, disco_usado, data_ativacao, data_fim_ativacao, modelo_sat,status FROM tb_sat WHERE firmware = "'.$fw.'" AND Status = "Ativo";' );   
   $stmt->execute();
   $results = $stmt->fetchAll( PDO::FETCH_ASSOC );

   $out = fopen( 'php://output', 'w' );
   foreach ( $results as $result ) 
   {
      fputcsv( $out, $result );
   }
   fclose( $out );
?>
