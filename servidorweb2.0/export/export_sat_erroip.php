<?php
    include_once("../security/seguranca.php");
    protegePagina();
    $errip = filter_input(INPUT_GET, 'errip', FILTER_SANITIZE_STRING);
    header( 'Content-type: application/csv' );   
    header( 'Content-Disposition: attachment; filename=export_sat_'.$errip.'_cx.csv' );   
    header( 'Content-Transfer-Encoding: binary' );
    header( 'Pragma: no-cache');

    $pdo = new PDO( 'mysql:host=localhost;dbname=srvremoto', 'root', 'diabrasil' );
    $stmt = $pdo->prepare( 'SELECT loja, caixa, sat, ip, IPLoja FROM cn_sat_erro_ip;' );   
    $stmt->execute();
    $results = $stmt->fetchAll( PDO::FETCH_ASSOC );

    $out = fopen( 'php://output', 'w' );
    foreach ( $results as $result ) 
    {
        fputcsv( $out, $result );
    }
    fclose( $out );
?>