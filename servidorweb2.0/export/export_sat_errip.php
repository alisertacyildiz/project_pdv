<?php
    include_once("../security/seguranca.php");
    protegePagina();
    $errip = filter_input(INPUT_GET, 'errip', FILTER_SANITIZE_STRING);
    header( 'Content-type: application/csv' );   
    header( 'Content-Disposition: attachment; filename=export_sat_errip_cx'.$errip.'.csv' );   
    header( 'Content-Transfer-Encoding: binary' );
    header( 'Pragma: no-cache');

    $pdo = new PDO( 'mysql:host=localhost;dbname=srvremoto', 'root', 'diabrasil' );
    $stmt = $pdo->prepare( 'SELECT loja, caixa, sat, ip FROM cn_sat_erro_ip WHERE caixa = "'.$errip.'";' );   
    $stmt->execute();
    $results = $stmt->fetchAll( PDO::FETCH_ASSOC );

    $out = fopen( 'php://output', 'w' );
    foreach ( $results as $result ) 
    {
        fputcsv( $out, $result );
    }
    fclose( $out );
?>