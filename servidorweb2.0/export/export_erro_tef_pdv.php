<?php
    include_once("../security/seguranca.php");
    protegePagina();
    $errtefpdv = filter_input(INPUT_GET, 'errtefpdv', FILTER_SANITIZE_STRING);
    header( 'Content-type: application/csv' );   
    header( 'Content-Disposition: attachment; filename=export_erro_'.$errtefpdv.'_cx.csv' );   
    header( 'Content-Transfer-Encoding: binary' );
    header( 'Pragma: no-cache');

    $pdo = new PDO( 'mysql:host=localhost;dbname=srvremoto', 'root', 'diabrasil' );
    $stmt = $pdo->prepare( 'SELECT Loja, Caixa, Cod_Empresa, Filial, Cod_Pdv FROM cn_erro_tef_pdv;' );   
    $stmt->execute();
    $results = $stmt->fetchAll( PDO::FETCH_ASSOC );

    $out = fopen( 'php://output', 'w' );
    foreach ( $results as $result ) 
    {
        fputcsv( $out, $result );
    }
    fclose( $out );
?>