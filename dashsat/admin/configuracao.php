<?php
    session_start();
    include_once("../security/seguranca.php");
    protegePagina();
    include_once("../security/connect.php");

    if(!empty($_SESSION['usuarioIDDashSAT']) && $_SESSION['usuarioNivelDashSAT'] != 1 ){
        //Grava LOG
        include_once("../processo/processa_log.php");
        $id = $_SESSION['usuarioIDDashSAT'];
        $userName = $_SESSION['usuarioNomeDashSAT'];
        $userLogin = $_SESSION['usuarioLoginDashSAT'];
        $dataLog = date('Y-m-d H:i:s');
        $appCallLog = 'Pagina Admin/Configuração'; 
        $msgLog = 'Usuário logado não tem permissão para acesso a esta pagina, será direcionado a pagina de login.';
        insert_log_I($id,$userName,$userLogin,$appCallLog,$dataLog,$msgLog);
        expulsaVisitante();
    }elseif(empty($_SESSION['usuarioIDDashSAT'])){
        //Grava LOG
        include_once("../processo/processa_log.php");
        $dataLog = date('Y-m-d H:i:s');
        $appCallLog = 'Pagina Admin/Configuração'; 
        $msgLog = 'Usuário logado não tem permissão para acesso a esta pagina, será direcionado a pagina de login.';
        insert_log_I('99','System','root',$appCallLog,$dataLog,$msgLog);
        expulsaVisitante();
    }

    $token = filter_input(INPUT_GET, 'token', FILTER_SANITIZE_STRING);
    if(!empty($token) && !empty($_SESSION['usuarioIDDashSAT'])){
        if($_SESSION['tokenLogonDashSAT'] != $token){
            //Grava LOG
            include_once("../processo/processa_log.php");
            $id = $_SESSION['usuarioIDDashSAT'];
            $userName = $_SESSION['usuarioNomeDashSAT'];
            $userLogin = $_SESSION['usuarioLoginDashSAT'];
            $dataLog = date('Y-m-d H:i:s');
            $appCallLog = 'Pagina Admin/Configuração'; 
            $msgLog = 'Token inválido ou expirado ['.$token.'].';
            insert_log_I($id,$userName,$userLogin,$appCallLog,$dataLog,$msgLog);
            expulsaVisitante();
        }
    }elseif(empty($token) && !empty($_SESSION['usuarioIDDashSAT'])){
        //Grava LOG
        include_once("../processo/processa_log.php");
        $id = $_SESSION['usuarioIDDashSAT'];
        $userName = $_SESSION['usuarioNomeDashSAT'];
        $userLogin = $_SESSION['usuarioLoginDashSAT'];
        $dataLog = date('Y-m-d H:i:s');
        $appCallLog = 'Pagina Admin/Configuração'; 
        $msgLog = 'Token não informado ['.$token.'].';
        insert_log_I($id,$userName,$userLogin,$appCallLog,$dataLog,$msgLog);
        expulsaVisitante();
    }elseif(empty($token) && empty($_SESSION['usuarioIDDashSAT'])){
        //Grava LOG
        include_once("../processo/processa_log.php");
        $dataLog = date('Y-m-d H:i:s');
        $appCallLog = 'Pagina Admin/Configuração'; 
        $msgLog = 'Token inválido ou expirado ['.$token.'].';
        insert_log_I('99','System','root',$appCallLog,$dataLog,$msgLog);
        expulsaVisitante();
    }
    
    $id = $_SESSION['usuarioIDDashSAT'];
    $userName = $_SESSION['usuarioNomeDashSAT'];
    $userLogin = $_SESSION['usuarioLoginDashSAT'];
	$result_usuario = "SELECT * FROM tb_usuarios_dashsat WHERE id = '$id'";
	$resultado_usuario = mysqli_query($conn, $result_usuario);
    $row_usuario = mysqli_fetch_assoc($resultado_usuario);
    $avatar = $_SESSION['usuarioAvatarDashSAT'];

    $sqlGroupModeloSat = "SELECT modelo_sat, count(id) AS qntd FROM tb_sat WHERE status = 'Ativo' AND data_atualizacao BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() GROUP BY modelo_sat";
    $queryGroupModeloSat = mysqli_query($conn,$sqlGroupModeloSat);
    while($resultGroupModeloSat = mysqli_fetch_assoc($queryGroupModeloSat)){
        //echo "Modelo:" . $resultGroupModeloSat['modelo_sat'] . "Qntd:" . $resultGroupModeloSat['qntd'] ."";
        $objGroupSat[] = (object) $resultGroupModeloSat;
    }
    //print_r($obj);
    $totalGroupSat=0;
    foreach($objGroupSat as $key=>$val){
        //print "#{$key} " . $obj[$key]->qntd . "<br/>";
        $totalGroupSat = $objGroupSat[$key]->qntd + $totalGroupSat;
    }
    /*print "Total" . $totalGroupSat;
    print "<br/>";
    foreach($objGroupSat as $key=>$val){
        print round((($objGroupSat[$key]->qntd/$totalGroupSat)*100),3) . "<br/>";
    }
    */
    //Comunicação com a SEFAZ
    $sqlCountSatErrComunSefaz = "SELECT COUNT(loja) AS total_registros FROM cn_sat_com_sefaz";
    $queryCountSatErrComunSefaz = mysqli_query($conn,$sqlCountSatErrComunSefaz);
    $rowCountSatErrComunSefaz = mysqli_fetch_assoc($queryCountSatErrComunSefaz);
    $percCountSatErrComunSefaz = round((($rowCountSatErrComunSefaz['total_registros']/$totalGroupSat)*100),0);

    //XMLs Presos
    $sqlCountSatXmlPresos = "SELECT COUNT(loja) AS total_registros FROM cn_sat_disco";
    $queryCountSatXmlPresos = mysqli_query($conn,$sqlCountSatXmlPresos);
    $rowCountSatXmlPresos = mysqli_fetch_assoc($queryCountSatXmlPresos);
    $percCountSatXmlPresos = round((($rowCountSatXmlPresos['total_registros']/$totalGroupSat)*100),0);

    $_SESSION['total_notificacoes'] = $rowCountSatErrComunSefaz['total_registros']+$rowCountSatXmlPresos['total_registros'];
    $_SESSION['total_reg_sat_s_comun_sefaz'] = $rowCountSatErrComunSefaz['total_registros'];
    $_SESSION['total_reg_sat_xml_presos'] = $rowCountSatXmlPresos['total_registros'];

	$sqlComboNivel = "SELECT nivel, descricao FROM tb_niveis_dashsat";
    $queryComboNivel = mysqli_query($conn,$sqlComboNivel);
    $sqlComboStatus = "SELECT status, descricao FROM tb_status_user";
    $queryComboStatus = mysqli_query($conn,$sqlComboStatus);

    $sqlComboStatusEdit = "SELECT status, descricao FROM tb_status_user";
    $queryComboStatusEdit = mysqli_query($conn,$sqlComboStatusEdit);
    $sqlComboNivelEdit = "SELECT nivel, descricao FROM tb_niveis_dashsat";
    $queryComboNivelEdit = mysqli_query($conn,$sqlComboNivelEdit);

    //Conexão ao banco de dados, exclusiva para monitorar inatividade do usuário
    $idLoginTemp = $_SESSION['idLoginTempDashSAT'];
    $conexao = new PDO('mysql:host=localhost;dbname=srvremoto',"root","diabrasil");
    $usuarioLogado = $conexao->prepare("SELECT * FROM tb_sessoes_login_dashsat WHERE id = '$idLoginTemp'");
    $usuarioLogado->execute();
    $fech = $usuarioLogado->fetchAll();

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Admin SAT | Dashboard</title>
		<link rel="icon" href="../favicon.ico" />
        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="../plugins/fonts-google/fontgoogle.css" rel="stylesheet">
    </head>
    <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed" id="limpaTempo">
    <?php
            date_default_timezone_set("America/Sao_Paulo");
            setlocale(LC_ALL, 'pt_BR');
            $num_users = "SELECT count(id) AS cadastrado FROM tb_usuarios_dashsat";
            //Obter a data atual
            $resultado_qnt_cadastros = mysqli_query($conn, $num_users);
            $row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
            
            $data['atual'] = date('Y-m-d H:i:s'); 

            //Diminuir 20 segundos 
            $data['online'] = strtotime($data['atual'] . " - 20 seconds");
            $data['online'] = date("Y-m-d H:i:s",$data['online']);
            
            //Pesquisar os ultimos usuarios online nos 20 segundo
            $result_qnt_visitas = "SELECT count(id) AS online FROM tb_visitas_dahshsat WHERE data_final >= '" . $data['online'] . "'";
            
            $resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
            $row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
            
            $qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
            $qnt_perc = round((($row_qnt_visitas['online'] / $row_qnt_cadastros['cadastrado'])*100),2);

        ?>   
        <script type="text/javascript">
            //Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
            setInterval(function(){
            //Incluir e enviar o POST para o arquivo responsável em fazer contagem
            $.post("../processo/processa_vis.php", {contar: '',}, function(data){
                $('#online').text(data);
            });
            }, 10000);
        </script>
        <div class="wrapper">
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="home.php?token=<?php echo $token;?>" class="nav-link">
                            Home
                        </a>
                    </li>
                </ul>
                <!-- Right navbar links -->
                <?php
                    include_once("../notificacao/alertas.php");
                ?>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle dropdown-hover" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user-alt"></i>
                            <span class="text">
                                Account
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaDadosPessoais">
                                <i class="far fa-id-card"></i> Pefil</a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editaSenha">
                            <i class="fas fa-lock"></i> Alterar senha</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="../security/sair.php">
                                <i class="fas fa-sign-out-alt"></i> Log Out</a>
                        </div>
                    </li>
                </ul>   
            </nav>
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <a href="home.php?token=<?php echo $token;?>" class="brand-link">
                    <img src="../dist/img/logo-dia.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                    style="opacity: .8">
                    <span class="brand-text font-weight-light">Admin SAT</span>
                </a>
                <div class="sidebar">
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                        <?php
                            echo  "<img src='$avatar' class='img-circle elevation-2' alt='User Image'>";
                        ?>
                        </div>
                        <div class="info">
                            <a href="#" class="d-block"><?php print $_SESSION['usuarioNomeDashSAT']?></a>
                        </div>
                    </div>
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <li class="nav-item has-treeview">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        Dashboard
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="dashboard.php?token=<?php echo $token;?>" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Dashboard S&#64;T</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="tables/table.php?token=<?php echo $token;?>" class="nav-link">
                                    <i class="nav-icon fas fa-table"></i>
                                    <p>
                                        Tables
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="tables/table.php?token=<?php echo $token;?>" class="nav-link">
                                            <i class="fas fa-server nav-icon"></i>
                                            <p>Todos os S&#64;Ts</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="tables/tb-comun-sefaz.php?token=<?php echo $token;?>" class="nav-link">
                                            <i class="fas fa-satellite-dish nav-icon"></i>
                                            <p>Comunicação SEFAZ</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="tables/tb-xml-presos.php?token=<?php echo $token;?>" class="nav-link">
                                            <i class="fas fa-hdd nav-icon"></i>
                                            <p>XMLs Pendentes</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="tables/tb-erro-config.php?token=<?php echo $token;?>" class="nav-link">
                                            <i class="fas fa-cogs nav-icon"></i>
                                            <p>Erros de Configurações</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="tables/tb-sats-falha.php?token=<?php echo $token;?>" class="nav-link">
                                            <i class="fas fa-exclamation-circle nav-icon"></i>
                                            <p>S&#64;Ts em falha</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="configuracao.php?token=<?php echo $token;?>" class="nav-link active">
                                    <i class="fas fa-cogs nav-icon"></i>
                                    <p>Configurações</p>
                                </a>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="sistema.php?token=<?php echo $token;?>" class="nav-link">
                                    <i class="fab fa-ubuntu nav-icon"></i>
                                    <p>Sistema</p>
                                </a>
                            </li>
                        </ul> 
                    </nav>
                </div>    
            </aside>
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1>Configurações</h1>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="home.php?token=<?php echo $token;?>">Home</a></li>
                                    <li class="breadcrumb-item active">Configurações</li>
                                </ol>
                            </div>
                        </div>
                    </div><!-- /.container-fluid -->
                </section>
                <section class="content">
                    <div class="container-fluid">
                        <div class="card">
                            <div class="card-header p-2">
                                <ul class="nav nav-pills">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#dadosNivel" data-toggle="tab">
                                            Nível
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#dadosUsuarios" data-toggle="tab">
                                            Usuários
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#dadosAlertas" data-toggle="tab">
                                            Alertas
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="active tab-pane" id="dadosNivel">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="card card-info card-outline">
                                                    <div class="card-header">
                                                    <h3 class="card-title">Niveis de acesso</h3>
                                                        <div class="card-tools">
                                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="table-responsive-sm">
                                                            <table id="tbNiveis" class="table table-sm table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>Nível</th>
                                                                        <th>Ações</th>
                                                                    </tr>
                                                                </thead>
                                                                <?php
                                                                    $sqlNiveis = "SELECT id, nivel, descricao, data_criacao, data_modificacao FROM tb_niveis_dashsat";
                                                                    $queryNiveis = mysqli_query($conn,$sqlNiveis);
                                                                    echo '<tbody>';
                                                                        while($rowNiveis = mysqli_fetch_array($queryNiveis)){
                                                                            $idNivel = $rowNiveis['id'];
                                                                            $descNivel = $rowNiveis['descricao'];

                                                                            echo '<tr>';
                                                                                echo '<td>'.$idNivel.'</td>';
                                                                                echo '<td>'.$descNivel.'</td>';
                                                                                echo '<td><a href="#"><i class="fas fa-edit"></i></a>&nbsp; &nbsp;<a href="../processo/delete_nivel.php?id='.$idNivel.'&desc-nivel='.$descNivel.'"><i class="fas fa-trash-alt"></i></a></td>';
                                                                            echo '</tr>';

                                                                        }
                                                                        echo '</tbody>';
                                                                ?>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="card card-secondary card-outline">
                                                    <div class="card-header">
                                                    <h3 class="card-title">Novo nível</h3>
                                                        <div class="card-tools">
                                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
													<form action="../processo/cadastrar_nivel.php" name="frm_cad_nivel"  method="POST">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label for="nivel" class="col-sm-4 col-form-label">Nível</label>
                                                                    <div class="col-sm-6">
                                                                        <input type="text" class="form-control form-control-sm col-sm-4" id="nivel" name="nivel">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <label for="desc-nivel" class="col-sm-6 col-form-label">Descrição nível</label>
                                                                    <div class="col-sm-6">
                                                                        <input type="text" class="form-control form-control-sm" id="desc-nivel" name="desc-nivel">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="col-sm-6">
                                                                        <label for="cb_nivel" class="col-sm-6 col-form-label"></label>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <a href="">
                                                                            <button type="submit" class="btn btn-outline-primary" >Salvar</button>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>				
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="dadosUsuarios">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="card card-info card-outline">
                                                    <div class="card-header">
                                                    <h3 class="card-title">Usuários</h3>
                                                        <div class="card-tools">
                                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="table-responsive-sm">
                                                            <table id="tbUsuarios" class="table table-sm table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>Login</th>
                                                                        <th>Nível</th>
                                                                        <th>Ações</th>
                                                                    </tr>
                                                                </thead>
                                                                <?php
                                                                    $sqlTbUsuarios = "SELECT id, nome, login, email, nivel, descricao, ativo,desc_status, avatar, data_criacao, data_modificacao FROM cn_dados_usuario_dashsat";
                                                                    $queryTbUsuarios = mysqli_query($conn,$sqlTbUsuarios);
                                                                    echo '<tbody>';
                                                                        while($rowTbUsuarios = mysqli_fetch_array($queryTbUsuarios)){
                                                                            $idTbUser     = $rowTbUsuarios['id'];
                                                                            $loginTbUser  = $rowTbUsuarios['login'];
                                                                            $nivelTbUser  = $rowTbUsuarios['descricao'];
                                                                            $nomeTbUser   = $rowTbUsuarios['nome'];
                                                                            $emailTbUser  = $rowTbUsuarios['email'];
                                                                            $nvTbUser     = $rowTbUsuarios['nivel'];
                                                                            $statusTbuser = $rowTbUsuarios['ativo'];
                                                                            $dtCriacaoTbUser = $rowTbUsuarios['data_criacao'];
                                                                            $dtModifTbUser = $rowTbUsuarios['data_modificacao'];
                                                                            $descStatusTbuser = $rowTbUsuarios['desc_status'];

                                                                            $data_user['atual'] = date('Y-m-d H:i:s');  
                                                                            //Diminuir 20 segundos 
                                                                            $data_user['online'] = strtotime($data_user['atual'] . " - 20 seconds");
                                                                            $data_user['online'] = date("Y-m-d H:i:s",$data_user['online']);
                                                                            
                                                                            //Pesquisar os ultimos usuarios online nos 20 segundo
                                                                            $result_user_online = "SELECT * FROM tb_visitas_dahshsat WHERE id_usuario = '" . $rowTbUsuarios['id'] . "' AND data_final >= '" . $data_user['online'] . "'";
                                                                            
                                                                            $resultado_user_online = mysqli_query($conn, $result_user_online);
                                                                            $row_user_online = mysqli_fetch_assoc($resultado_user_online);

                                                                            echo '<tr>';
                                                                                echo '<td>'.$idTbUser.'</td>';
                                                                                if (!empty($row_user_online))
                                                                                {
                                                                                echo "<td><img width='10px' height='10px' src='../dist/img/online.png' mr-2/>".$loginTbUser."</td>";
                                                                                }else{
                                                                                echo "<td><img width='10px' height='10px' src='../dist/img/offline.png' mr-2/>".$loginTbUser."</td>";
                                                                                }
                                                                                echo '<td>'.$nivelTbUser.'</td>';
                                                                                echo '<td><a href="#" data-toggle="modal" data-target="#viewDadosUsuario" data-id="'.$idTbUser.'" data-nome="'.$nomeTbUser.'" data-email="'.$emailTbUser.'" data-login="'.$loginTbUser.'" data-nivel="'.$nivelTbUser.'" data-ativo="'.$descStatusTbuser.'" data-dtcriacao="'.$dtCriacaoTbUser.'" data-dtmodif="'.$dtModifTbUser.'"><i class="fas fa-info"></i></a>&nbsp; &nbsp;<a href="#" data-toggle="modal" data-target="#editaDadosUsuario" data-id="'.$idTbUser.'" data-nome="'.$nomeTbUser.'" data-email="'.$emailTbUser.'" data-login="'.$loginTbUser.'" data-nivel="'.$nvTbUser.'" data-ativo="'.$statusTbuser.'"><i class="fas fa-edit"></i></a>&nbsp; &nbsp;<a href="../processo/delete_user.php?id='.$idTbUser.'&login-user='.$loginTbUser.'&nivel-user='.$nivelTbUser.'"><i class="fas fa-trash-alt"></i></a>&nbsp; &nbsp;<a href="../processo/reset_passwd.php?id='.$idTbUser.'&login-user='.$loginTbUser.'&nivel-user='.$nivelTbUser.'"><i class="fas fa-key"></i></a></td>';
                                                                            echo '</tr>';

                                                                        }
                                                                        echo '</tbody>';
                                                                ?>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="card card-secondary card-outline">
                                                    <div class="card-header">
                                                    <h3 class="card-title">Novo usuário</h3>
                                                        <div class="card-tools">
                                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <form action="../processo/cadastrar_user.php" name="frm_cad_user"  method="POST" onsubmit="return valida_campos_new_user();">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <label for="nome-usuario" class="col-sm-12 col-form-label">Nome usuário</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control form-control-sm" id="nome-usuario" name="nome-usuario" <?php if(!empty($_SESSION['tempNome'])){echo "value='".$_SESSION['tempNome']."'";}?> onkeyup="click_campos_new_user('nome-usuario');">
                                                                        <span id="span-nome" style="color:#ff0000;"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label for="email-usuario" class="col-sm-12 col-form-label">E-mail usuário</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control form-control-sm" id="email-usuario" name="email-usuario" <?php if(!empty($_SESSION['tempNome'])){echo "value='".$_SESSION['tempEmail']."'";}?> onkeyup="click_campos_new_user('email-usuario');valida_email('email-usuario');">
                                                                        <span id="span-email" style="color:#ff0000;"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label for="nome-login" class="col-sm-12 col-form-label">Login usuário</label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control form-control-sm" id="nome-login" name="nome-login" <?php if(!empty($_SESSION['tempNome'])){echo "value='".$_SESSION['tempLogin']."'";}?> maxlength="10" onkeyup="click_campos_new_user('nome-login');">
                                                                        <span id="span-login" style="color:#ff0000;"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label for="cb_nivel" class="col-sm-12 col-form-label">Nível</label>
                                                                    <div class="col-sm-12">
                                                                        <select id="cb_nivel" name="cb_nivel" class="form-control form-control-sm" onkeyup="click_campos_new_user('cb_nivel');" onclick="click_campos_new_user('cb_nivel');">
																			<option>Selecione...</option>
																			<?php while($rowComboNivel = mysqli_fetch_array($queryComboNivel)){ ?>
																			<option value="<?php print $rowComboNivel['nivel']?>"><?php print $rowComboNivel['descricao']?></option><?php }?>
                                                                        </select>
                                                                        <span id="span-nivel" style="color:#ff0000;"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label for="cb_status" class="col-sm-12 col-form-label">Status</label>
                                                                    <div class="col-sm-12">
                                                                        <select id="cb_status" name="cb_status" class="form-control form-control-sm" onkeyup="click_campos_new_user('cb_status');" onclick="click_campos_new_user('cb_status');">
																			<option>Selecione...</option>
																			<?php while($rowComboStatus = mysqli_fetch_array($queryComboStatus)){ ?>
																			<option value="<?php print $rowComboStatus['status']?>"><?php print $rowComboStatus['descricao']?></option><?php }?>
                                                                        </select>
                                                                        <span id="span-status" style="color:#ff0000;"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <br />
                                                                    <div class="col-sm-6">
                                                                        <a href="">
                                                                            <button type="submit" class="btn btn-outline-primary" >Salvar</button>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="dadosAlertas">
                                        <form class="form-horizontal">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card card-secondary card-outline">
                                                        <div class="card-header">
                                                        <h3 class="card-title">Alarmes</h3>
                                                            <div class="card-tools">
                                                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="card-body">
                                                            <form id="" name="" action="" method="POST">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form name="frm_senha" action="../processo/salva_senha.php" method="POST">
                        <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>  
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                                                <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                                                <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-outline-primary">Confirma</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form name="frm_dados_pessoais" action="../processo/atualizar_usuario.php" method="POST">
                        <div class="modal fade" id="editaDadosPessoais" tabindex="-1" role="dialog" aria-labelledby="editaDadosPessoaisLabel" aria-hidden="true">    
                            <div class="modal-dialog" role="document">          
                                <!-- Modal content-->      
                                <div class="modal-content">        
                                    <div class="modal-header">          
                                        <h4 class="modal-title texto-modal text-center">Dados Pessoais</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>  
                                    </div>        
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="text">Avatar </label>    
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" id="avatar" name="avatar">
                                                    <label class="form-check-label"><img src="../dist/img/avatar.png" class="img-circle elevation-2" alt="User Image" width="48" height="48"></label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" id="avatar2" name="avatar2">
                                                    <label class="form-check-label"><img src="../dist/img/avatar2.png" class="img-circle elevation-2" alt="User Image" width="48" height="48"></label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" id="avatar3" name="avatar3">
                                                    <label class="form-check-label"><img src="../dist/img/avatar3.png" class="img-circle elevation-2" alt="User Image" width="48" height="48"></label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" id="avatar4" name="avatar4">
                                                    <label class="form-check-label"><img src="../dist/img/avatar4.png" class="img-circle elevation-2" alt="User Image" width="48" height="48"></label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" id="avatar5" name="avatar5">
                                                    <label class="form-check-label"><img src="../dist/img/avatar5.png" class="img-circle elevation-2" alt="User Image" width="48" height="48"></label>
                                                </div>
                                                <label for="text">ID </label>
                                                <input for="text" class="form-control" name="id" value="<?php echo $row_usuario['id'] ?>" readonly>
                                                <label for="text">Nome </label>
                                                <input type="text" class="form-control" name="nome" value="<?php echo $row_usuario['nome']; ?>" >
                                                <label for="email">E-mail </label>
                                                <input type="email" class="form-control" name="email" value="<?php echo $row_usuario['email']; ?>">
                                                <label for="text">Login </label>
                                                <input type="text" class="form-control" name="login" value="<?php echo $row_usuario['login']; ?>" readonly>
                                            </div>
                                        </form>        
                                    </div>        
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-outline-success">Atualizar</button>        
                                    </div>      
                                </div> 
                            </div>
                        </div>
                    </form>
                    <form action="../processo/editar_user.php" method="POST">
                        <div class="modal fade" id="editaDadosUsuario" tabindex="-1" role="dialog" aria-labelledby="editaDadosUsuario" aria-hidden="true">    
                            <div class="modal-dialog" role="document">          
                                <!-- Modal content-->      
                                <div class="modal-content">        
                                    <div class="modal-header">          
                                        <h4 class="modal-title texto-modal text-center">Dados Usuário</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>  
                                    </div>        
                                    <div class="modal-body">
                                        <form id="editaDadosUsuarioForm">
                                            <div class="form-group">
                                                <label for="text">ID </label>
                                                <input for="text" class="form-control form-control-sm" id="id" name="id" readonly>
                                                <label for="text">Nome </label>
                                                <input type="text" class="form-control form-control-sm" id="nome" name="nome" onkeyup="click_campos_new_user('nome');">
                                                <span id="nome-edit"></span>
                                                <label for="email">E-mail </label>
                                                <input type="email" class="form-control form-control-sm" id="email" name="email" onkeyup="click_campos_new_user('email');">
                                                <label for="text">Login </label>
                                                <input type="text" class="form-control form-control-sm" id="login" name="login" onkeyup="click_campos_new_user('login');">
                                                <label for="cb_nivel">Nível</label>
                                                <select id="nivel" name="cb_nivel" class="form-control form-control-sm" onkeyup="click_campos_new_user('nivel');">
                                                    <?php while($rowComboNivelEdit = mysqli_fetch_array($queryComboNivelEdit)){ ?>
                                                    <option value="<?php print $rowComboNivelEdit['nivel']?>"><?php print $rowComboNivelEdit['descricao']?></option><?php }?>
                                                </select>
                                                <label for="cb_nivel">Status</label>
                                                <select id="status" name="cb_status" class="form-control form-control-sm" onkeyup="click_campos_new_user('status');">
                                                    <?php while($rowComboStatusEdit = mysqli_fetch_array($queryComboStatusEdit)){ ?>
                                                    <option value="<?php print $rowComboStatusEdit['status']?>"><?php print $rowComboStatusEdit['descricao']?></option><?php }?>
                                                </select>
                                            </div>
                                        </form>        
                                    </div>        
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class="btn btn-outline-success">Atualizar</button>        
                                    </div>      
                                </div> 
                            </div>
                        </div>
                    </form>
                    <form name="frm_view_user" action="#" method="POST">
                        <div class="modal fade" id="viewDadosUsuario" tabindex="-1" role="dialog" aria-labelledby="viewDadosUsuario" aria-hidden="true">    
                            <div class="modal-dialog" role="document">          
                                <!-- Modal content-->      
                                <div class="modal-content">        
                                    <div class="modal-header">          
                                        <h4 class="modal-title texto-modal text-center">Dados Usuário</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>  
                                    </div>        
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="text">ID </label>
                                                <input for="text" class="form-control form-control-sm" id="id" name="id" readonly>
                                                <label for="text">Data Criação </label>
                                                <input type="text" class="form-control form-control-sm" id="dtcriacao" name="dtcriacao" readonly>
                                                <label for="text">Nome </label>
                                                <input type="text" class="form-control form-control-sm" id="nome" name="nome" readonly>
                                                <label for="email">E-mail </label>
                                                <input type="email" class="form-control form-control-sm" id="email" name="email" readonly>
                                                <label for="text">Login </label>
                                                <input type="text" class="form-control form-control-sm" id="login" name="login" readonly>
                                                <label for="text">Nível </label>
                                                <input type="text" class="form-control form-control-sm" id="nivel" name="nivel" readonly>
                                                <label for="text">Status </label>
                                                <input type="text" class="form-control form-control-sm" id="status" name="status" readonly>
                                                <label for="text">Data Modificação </label>
                                                <input type="text" class="form-control form-control-sm" id="dtmodificacao" name="dtmodificacao" readonly>
                                            </div>
                                        </form>        
                                    </div>        
                                    <div class="modal-footer">   
                                    </div>      
                                </div> 
                            </div>
                        </div>
                    </form>
                </section>
            </div>
            <!-- /.control-sidebar -->
            <footer class="main-footer">
                <strong>Copyright &copy; 2020-2020 <a href="#">Develpoment TPVs</a>.</strong>
                All rights reserved.
                <div class="float-right d-none d-sm-inline-block">
                <b>Version</b> 3.0
                </div>
            </footer>

             <!-- Control Sidebar -->
             <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
        </div>
        <!-- ./wrapper -->

        <script src="../dist/js/pages/dashboard2.js"></script>
        <!-- jQuery -->
        <script src="../plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <!-- DataTables -->
        <script src="../plugins/datatables/jquery.dataTables.js"></script>
        <script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
        <!-- overlayScrollbars -->
        <script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        <script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- ChartJS -->
        <script src="../plugins/chart.js/Chart.min.js"></script>
        <!-- AdminLTE App -->
        <script src="../dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../dist/js/demo.js"></script>
        <!-- page script -->
        <script type="text/javascript">
            function valida_campos_new_user(){
                var msg = 'Campo obrigatório';
                var texto = msg.bold();
                
                if(document.getElementById('nome-usuario').value == ''){
                    document.getElementById('span-nome').innerHTML = texto;
                    document.getElementById('nome-usuario').style.border='1px solid red';
                    document.getElementById('nome-usuario').focus();
                    return false;
                }
                if(document.getElementById('email-usuario').value == ''){
                    document.getElementById('span-email').innerHTML = texto;
                    document.getElementById('email-usuario').style.border='1px solid red';
                    document.getElementById('email-usuario').focus();
                    return false;
                }
                if(document.getElementById('nome-login').value == ''){
                    document.getElementById('span-login').innerHTML = texto;
                    document.getElementById('nome-login').style.border='1px solid red';
                    document.getElementById('nome-login').focus();
                    return false;
                }
                if(document.getElementById('cb_nivel').selectedIndex == 0){
                    document.getElementById('span-nivel').innerHTML = texto;
                    document.getElementById('cb_nivel').style.border='1px solid red';
                    document.getElementById('cb_nivel').focus();
                    return false;
                }
                if(document.getElementById('cb_status').selectedIndex == 0){
                    document.getElementById('span-status').innerHTML = texto;
                    document.getElementById('cb_status').style.border='1px solid red';
                    document.getElementById('cb_status').focus();
                    return false; 
                }
            }
        </script>
        <script type="text/javascript">
            function click_campos_new_user(id){
                document.getElementById(id).style.border='1px solid green';
                document.getElementById('span-nome').innerHTML = '';
                document.getElementById('span-email').innerHTML = '';
                document.getElementById('span-login').innerHTML = '';
                document.getElementById('span-nivel').innerHTML = '';
                document.getElementById('span-status').innerHTML = '';
                document.getElementById('nome-edit').innerHTML = '';
            }
            function valida_email(email){
                var msg = 'E-mail inválido [email@dominio.com].';
                var texto = msg.bold();
                if(document.getElementById(email).value.indexOf('@')==-1 || document.getElementById(email).value.indexOf('.')==-1){
                    document.getElementById('span-email').innerHTML = texto;
                    document.getElementById('email-usuario').style.border='1px solid red';
                    document.getElementById('email-usuario').focus();
                    return false;
                }
            }
        </script>
        <script type="text/javascript">
            $('#editaDadosUsuario').on('show.bs.modal', function(event){
                var button = $(event.relatedTarget)
                var idUserEdit = button.data('id')
                var nomeEditUser = button.data('nome')
                var emailEditUser = button.data('email')
                var loginEdituser = button.data('login')
                var nivelEditUser = button.data('nivel')
                var statusEditUser = button.data('ativo')

                var modal = $(this)
                modal.find('#id').val(idUserEdit)
                modal.find('#nome').val(nomeEditUser)
                modal.find('#email').val(emailEditUser)
                modal.find('#login').val(loginEdituser)
                modal.find('#nivel').val(nivelEditUser)
                modal.find('#status').val(statusEditUser)
            })
                                                        
            $('#viewDadosUsuario').on('show.bs.modal', function(event){
                var button = $(event.relatedTarget)
                var idUserView = button.data('id')
                var nomeViewUser = button.data('nome')
                var dtCriacao = button.data('dtcriacao')
                var emailViewuser = button.data('email')
                var loginViewUser = button.data('login')
                var nivelViewUser = button.data('nivel')
                var statusViewUser = button.data('ativo')
                var dtModif = button.data('dtmodif')

                var modal = $(this)
                modal.find('#id').val(idUserView)
                modal.find('#nome').val(nomeViewUser)
                modal.find('#dtcriacao').val(dtCriacao)
                modal.find('#email').val(emailViewuser)
                modal.find('#login').val(loginViewUser)
                modal.find('#nivel').val(nivelViewUser)
                modal.find('#status').val(statusViewUser)
                modal.find('#dtmodificacao').val(dtModif)
            })
        </script>
        <script>
            $(function () {
                $('#tbNiveis').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 3
                });
                $('#tbUsuarios').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "pageLength": 3
                });
            });
        </script>       
    </body>
    <?php
        echo '
        <script type="text/javascript">
            $(document).ready(function(){
                function atualizaTempo(){
                    $.ajax({
                        url: "../processo/atualiza_tempo.php",
                        type: "POST",
                        data: {id:'.$fech[0]["id"].'},
                        success: function(data){
                            if(data == 1){
                                location.href="../processo/encerra_sessao.php";
                            }
                        }
                    }); 
                }setInterval(atualizaTempo,10000);
            });
            $("#limpaTempo").on("click", function(){
                $.ajax({
                    url: "../processo/limpa_tempo.php",
                    type: "POST",
                    data: {id:'.$fech[0]["id"].'}
                });
            });
        </script>';
    ?>
</html>