<?php
    session_start();
    include_once("../security/seguranca.php");
    protegePagina();
    include_once("../security/connect.php");
	
	$token = $_SESSION['tokenLogonDashSAT'];
	
	$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
	$nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
	$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
	$login = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING); 
	$nivel = filter_input(INPUT_POST, 'cb_nivel', FILTER_SANITIZE_STRING);
	$ativo = filter_input(INPUT_POST, 'cb_status', FILTER_SANITIZE_STRING);
	$data = date('Y-m-d H:i:s'); 
		
	if(!empty($id)){
		if(empty($nivel) || $nivel == 0){
			//Grava LOG
			include_once("processa_log.php");
			$dataLog = date('Y-m-d H:i:s');
			$appCallLog = 'Editar Usuário'; 
			$msgLog = 'Dados ['.$nome.']:['.$email.']:['.$login.']:['.$nivel.']:['.$ativo.'], erro ao realizar atualização.Revise os dados.';
			if($_SESSION['usuarioIDDashSAT'] != 0 ){
				insert_log_I($_SESSION['usuarioIDDashSAT'],$_SESSION['usuarioNomeDashSAT'],$_SESSION['usuarioLoginDashSAT'],$appCallLog,$dataLog,$msgLog);
			}
			echo "<script>alert('Erro ao atualizar dados do usuário revise os dados!');</script>";
			echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../admin/configuracao.php?token='.$token.'">';
		}

		$result_usuario = "UPDATE tb_usuarios_dashsat SET nome='$nome', login='$login',email='$email',nivel='$nivel',ativo='$ativo',data_modificacao='$data' WHERE id = '$id'";
		$resultado_usuario = mysqli_query($conn, $result_usuario);
		if(mysqli_affected_rows($conn)){
			//Grava LOG
			include_once("processa_log.php");
			$dataLog = date('Y-m-d H:i:s');
			$appCallLog = 'Editar Usuário'; 
			$msgLog = 'Dados ['.$nome.']:['.$email.']:['.$login.']:['.$nivel.']:['.$ativo.'], atualizado com sucesso.';
			if($_SESSION['usuarioIDDashSAT'] != 0 ){
				insert_log_I($_SESSION['usuarioIDDashSAT'],$_SESSION['usuarioNomeDashSAT'],$_SESSION['usuarioLoginDashSAT'],$appCallLog,$dataLog,$msgLog);
			}
			echo "<script>alert('Dados atualizado com sucesso!');</script>";
			echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../admin/configuracao.php?token='.$token.'">';
		}else{
			//Grava LOG
			include_once("processa_log.php");
			$dataLog = date('Y-m-d H:i:s');
			$appCallLog = 'Editar Usuário'; 
			$msgLog = 'Dados ['.$nome.']:['.$email.']:['.$login.']:['.$nivel.']:['.$ativo.'], erro ao realizar atualização.Revise os dados.';
			if($_SESSION['usuarioIDDashSAT'] != 0 ){
				insert_log_I($_SESSION['usuarioIDDashSAT'],$_SESSION['usuarioNomeDashSAT'],$_SESSION['usuarioLoginDashSAT'],$appCallLog,$dataLog,$msgLog);
			}
			echo "<script>alert('Erro ao atualizar dados do usuário revise os dados!');</script>";
			echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../admin/configuracao.php?token='.$token.'">';
		}
	}else{
		//Grava LOG
		include_once("processa_log.php");
		$dataLog = date('Y-m-d H:i:s');
		$appCallLog = 'Editar Usuário'; 
		$msgLog = 'Necessário selecionar um usuário!';
		if($_SESSION['usuarioIDDashSAT'] != 0 ){
			insert_log_I($_SESSION['usuarioIDDashSAT'],$_SESSION['usuarioNomeDashSAT'],$_SESSION['usuarioLoginDashSAT'],$appCallLog,$dataLog,$msgLog);
		}	
		$_SESSION['msg'] = "<p style='color:red;'>Necessário selecionar um usuário</p>";
		echo "<script>alert('Necessário selecionar um usuário!');</script>";
		echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../admin/configuracao.php?token='.$token.'">';
	}

?>