<?php
/**
* Sistema de segurança com acesso restrito
*
* Usado para restringir o acesso de certas páginas do seu site
*
* @version 2.0
* @package SistemaSeguranca
*/
//  Configurações do Script
// ==============================
$_SG['conectaServidorDashSAT'] = true;       // Abre uma conexão com o servidor MySQL?
$_SG['abreSessaoDashSAT'] = true;            // Inicia a sessão com um session_start()?
$_SG['caseSensitiveDashSAT'] = false;        // Usar case-sensitive?
$_SG['validaSempreDashSAT'] = true;          // Deseja validar o usuário e a senha a cada carregamento de página
$_SG['paginaLoginDashSAT'] = "../index.php"; // Página de login

if ($_SG['conectaServidorDashSAT'] == true) {
    //Conectando no servidor
    require_once("connect.php");
    }
    // Verifica se precisa iniciar a sessão
    if ($_SG['abreSessaoDashSAT'] == true)
      session_start();
    /**
    * Função que valida um usuário e senha
    *
    * @param string $usuariot - O usuário a ser validado
    * @param string $senhat - A senha a ser validada
    *
    * @return bool - Se o usuário foi validado ou não (true/false)
    */
    function validaUsuario($usuariot, $senhat) {
		global $_SG;
		$cS = ($_SG['caseSensitiveDashSAT']) ? 'BINARY' : ''; 
		// Usa a função addslashes para escapar as aspas
		$nusuario = addslashes($usuariot);
		$nsenha = addslashes($senhat);
		//$nlembrete = addcslashes($remembert);
		//echo "<script>alert('Senha1:$nsenha');</script>";
		$user = "root";
		$password = "diabrasil";
		
		// Monta uma consulta SQL (query) para procurar um usuário
		$conn = mysqli_connect('localhost', $user, $password , 'srvremoto')or die ("Erro ao conectar com o banco de dados!");
		//require_once("conecta.php");
		//$sql = "SELECT * FROM tb_usuarios WHERE login='$nusuario' AND senha='$nsenha' LIMIT 1";
		$sql = "SELECT * FROM tb_usuarios_dashsat WHERE login='$nusuario'";
		$query = mysqli_query($conn,$sql);
		$resultado = mysqli_fetch_assoc($query);
		// Verifica se encontrou algum registro
		if (empty($resultado)){
		// Nenhum registro foi encontrado => o usuário é inválido
		return false;
		}else{
			$sql_ = "SELECT * FROM tb_usuarios_dashsat WHERE login='$nusuario'";
			$query_ = mysqli_query($conn,$sql_);
			while($resulta = mysqli_fetch_array($query_))
			{
				$senha_crypt = $resulta['senha'];
				//echo "<script>alert('$senha_crypt');</script>";
				//echo "<script>alert('Senha2:$nsenha');</script>";
				$check_senha=password_verify($nsenha, $senha_crypt);
				//echo "<script>alert('$check_senha');</script>";
				if($check_senha == true){
					// Definimos dois valores na sessão com os dados do usuário
					$_SESSION['usuarioIDDashSAT'] = $resulta['id']; // Pega o valor da coluna 'id do registro encontrado no MySQL
					$_SESSION['usuarioNomeDashSAT'] = $resulta['nome']; // Pega o valor da coluna 'nome' do registro encontrado no MySQL
					$_SESSION['usuarioNivelDashSAT'] = $resulta['nivel'];
					$_SESSION['usuarioAvatarDashSAT'] = $resulta['avatar'];
					$_SESSION['usuarioIDAvatarDashSAT'] = $resulta['idavatar'];
					$_SESSION['usuarioStatusDashSAT'] = $resulta['ativo'];

					// Verifica a opção se sempre validar o login
					if ($_SG['validaSempreDashSAT'] == true) {
						// Definimos dois valores na sessão com os dados do login
						$_SESSION['usuarioLoginDashSAT'] = $nusuario;
						$_SESSION['usuarioSenhaDashSAT'] = $nsenha;
					}
					$randomico = mt_rand(0,999999);
					$nivelLoginToken = $_SESSION['usuarioNivelDashSAT'];
					$id = $_SESSION['usuarioIDDashSAT'];
					$userName = $_SESSION['usuarioNomeDashSAT'];
					$userLogin = $_SESSION['usuarioLoginDashSAT'];
					$dataLog = date('Y-m-d H:i:s');
					$appCallLog = 'Login Sistema'; 
					$msgLog = 'Usuário conectado com sucesso.';
					insert_log($id,$userName,$userLogin,$appCallLog,$dataLog,$msgLog);
					
					$tokenLogon = $randomico.$id.$nivelLoginToken.$userName.$userLogin.$dataLog;
					$tokenLogon = md5($tokenLogon);
					$tokenLogon = $tokenLogon.md5($tokenLogon);
					$_SESSION['tokenLogonDashSAT'] = $tokenLogon;

					$user = "root";
					$password = "diabrasil";
					
					// Monta uma consulta SQL (query) para procurar um usuário
					$dataIniSessao = date('Y-m-d H:i:s');
					$conn_ = mysqli_connect('localhost', $user, $password , 'srvremoto');
					$sqlInsertRegistraTempLogin = "INSERT INTO tb_sessoes_login_dashsat (data_inicio,token,id_user, nome_user, login_user) VALUES ('$dataIniSessao','$tokenLogon','$id', '$userName', '$userLogin')";
					$queryRegistraTempLogin = mysqli_query($conn_, $sqlInsertRegistraTempLogin);
					if(mysqli_insert_id($conn_)){
						//Grava LOG
						$_SESSION['idLoginTempDashSAT'] = mysqli_insert_id($conn_);
						$dataLog = date('Y-m-d H:i:s');
						$appCallLog = 'Registro de Sessão'; 
						$msgLog = 'Registro de sessão do login realizado com sucesso.';
						insert_log($id,$userName,$userLogin,$appCallLog,$dataLog,$msgLog);
					}else{
						//Grava LOG
						$dataLog = date('Y-m-d H:i:s');
						$appCallLog = 'Registro de Sessão'; 
						$msgLog = 'Erro ao registrar sessão do login.';
						insert_log($id,$userName,$userLogin,$appCallLog,$dataLog,$msgLog);
					}
						
					return true;
				}else{
					return false;
				}
			}
		}
	}
	/**
    * Função fpara gravar os Logs do usuário do Sistema
    *
    * @param int    $idUserLog - ID usuário registrado no Log
    * @param string $nomeUserLog - Usuário registrado no Log
    * @param string $loginUserLog - Login do usuário registrado no Log
    * @param string $appLog - Aplicação que executou o Log
    * @param string $dataLog - Data do Log
    * @param string $logDados - Dados do Log
    *
    * @return bool - Se o Log foi gravado (true/false)
    */
    function insert_log($idUserLog,$nomeUserLog,$loginUserLog,$appLog,$dataLog,$logDados){
        $user="root";
        $passwd="diabrasil";
        $host="127.0.0.1";
        $banco="srvremoto";
        $conn= mysqli_connect($host,$user,$passwd,$banco);

        $sqlInsertLog = "INSERT INTO tb_log_dashsat (id_user, nome_user, login_user, aplicacao, data_log, log_dados) VALUES ('$idUserLog','$nomeUserLog','$loginUserLog','$appLog','$dataLog','$logDados')";
        $queryInsertLog = mysqli_query($conn,$sqlInsertLog);
        
        if(mysqli_insert_id($conn)){
            return false;
        }else{
            return true;
        }
    }
    /**
    * Função que protege uma página
    */
    function protegePagina() {
      	global $_SG;
      	if (!isset($_SESSION['usuarioIDDashSAT']) OR !isset($_SESSION['usuarioNomeDashSAT'])) {
        // Não há usuário logado, manda pra página de login
        expulsaVisitante();
      	}else if(!isset($_SESSION['usuarioIDDashSAT']) OR !isset($_SESSION['usuarioNomeDashSAT'])) {
        // Há usuário logado, verifica se precisa validar o login novamente
			if ($_SG['validaSempreDashSAT'] == true) {
				// Verifica se os dados salvos na sessão batem com os dados do banco de dados
				if (!validaUsuario($_SESSION['usuarioLoginDashSAT'], $_SESSION['usuarioSenhaDashSAT'])) {
					// Os dados não batem, manda pra tela de login
					expulsaVisitante();
				}
        	}
      	}
    }
    /**
    * Função para expulsar um visitante
    */
    function expulsaVisitante() {
		$id = $_SESSION['usuarioIDDashSAT'];
		$userName = $_SESSION['usuarioNomeDashSAT'];
		$userLogin = $_SESSION['usuarioLoginDashSAT'];
		$dataLog = date('Y-m-d H:i:s');
		$appCallLog = 'Login Sistema'; 
		$msgLog = 'Usuário desconectado com sucesso.';
		if($id != 0){
			insert_log($id,$userName,$userLogin,$appCallLog,$dataLog,$msgLog);
		}
		
		$user = "root";
		$password = "diabrasil";
		$idLoginTemp = $_SESSION['idLoginTempDashSAT'];
		// Monta uma consulta SQL (query) para procurar um usuário
		$dataFimSessao = date('Y-m-d H:i:s');
		$conn_ = mysqli_connect('localhost', $user, $password , 'srvremoto');
		$sqlDelRegistraTempLogin = "UPDATE tb_sessoes_login_dashsat SET data_fim = '$dataFimSessao' WHERE id = '$idLoginTemp'";
		$queryRegistraTempLogin = mysqli_query($conn_,$sqlDelRegistraTempLogin);
		if(mysqli_affected_rows($conn_)){
			//Grava LOG
			$dataLog = date('Y-m-d H:i:s');
			$appCallLog = 'Registro de Sessão'; 
			$msgLog = 'Sessão encerrada com sucesso.';
			if($id != 0){
				insert_log($id,$userName,$userLogin,$appCallLog,$dataLog,$msgLog);
			}
		}else{
			//Grava LOG
			$dataLog = date('Y-m-d H:i:s');
			$appCallLog = 'Registro de Sessão'; 
			$msgLog = 'Erro ao registrar dados de Sessão.';
			if($id != 0){
				insert_log($id,$userName,$userLogin,$appCallLog,$dataLog,$msgLog);
			}
		}
		global $_SG;
		
      	// Remove as variáveis da sessão (caso elas existam)
		unset($_SESSION['usuarioIDDashSAT'], $_SESSION['usuarioNomeDashSAT'], $_SESSION['usuarioLoginDashSAT'], $_SESSION['usuarioSenhaDashSAT'], $_SESSION['usuarioAvatarDashSAT'], $_SESSION['usuarioIDAvatarDashSAT'],$_SESSION['idLoginTempDashSAT'],$_SESSION['tokenLogonDashSAT'],$_SESSION['usuarioStatusDashSAT']);
		
		//Limpa cookie intativade
		setcookie('cookieInatividade', '', time()-3600);
      	// Manda pra tela de login
      	$token = 'logoff';
		$token = md5($token);
		$token = $token.md5($token);
		header("Location: ".$_SG['paginaLoginDashSAT']."?token=$token");
	}
	function encerraSessao() {
		$id = $_SESSION['usuarioIDDashSAT'];
		$userName = $_SESSION['usuarioNomeDashSAT'];
		$userLogin = $_SESSION['usuarioLoginDashSAT'];
		$dataLog = date('Y-m-d H:i:s');
		$appCallLog = 'Login Sistema'; 
		$msgLog = 'Usuário desconectado com sucesso.';
		if($id != 0){
			insert_log($id,$userName,$userLogin,$appCallLog,$dataLog,$msgLog);
		}
		
		$user = "root";
		$password = "diabrasil";
		$idLoginTemp = $_SESSION['idLoginTempDashSAT'];
		// Monta uma consulta SQL (query) para procurar um usuário
		$dataFimSessao = date('Y-m-d H:i:s');
		$conn_ = mysqli_connect('localhost', $user, $password , 'srvremoto');
		$sqlDelRegistraTempLogin = "UPDATE tb_sessoes_login_dashsat SET data_fim = '$dataFimSessao' WHERE id = '$idLoginTemp'";
		$queryRegistraTempLogin = mysqli_query($conn_,$sqlDelRegistraTempLogin);
		if(mysqli_affected_rows($conn_)){
			//Grava LOG
			$dataLog = date('Y-m-d H:i:s');
			$appCallLog = 'Registro de Sessão'; 
			$msgLog = 'Sessão encerrada com sucesso.';
			if($id != 0){
				insert_log($id,$userName,$userLogin,$appCallLog,$dataLog,$msgLog);
			}
		}else{
			//Grava LOG
			$dataLog = date('Y-m-d H:i:s');
			$appCallLog = 'Registro de Sessão'; 
			$msgLog = 'Erro ao registrar dados de Sessão.';
			if($id != 0){
				insert_log($id,$userName,$userLogin,$appCallLog,$dataLog,$msgLog);
			}
		}
		global $_SG;
		
      	// Remove as variáveis da sessão (caso elas existam)
      	unset($_SESSION['usuarioIDDashSAT'], $_SESSION['usuarioNomeDashSAT'], $_SESSION['usuarioLoginDashSAT'], $_SESSION['usuarioSenhaDashSAT'], $_SESSION['usuarioAvatarDashSAT'], $_SESSION['usuarioIDAvatarDashSAT'],$_SESSION['idLoginTempDashSAT'],$_SESSION['tokenLogonDashSAT'],$_SESSION['usuarioStatusDashSAT']);
		// Manda pra tela de login
		$token = 'inativa';
		$token = md5($token);
		$token = $token.md5($token);
		header("Location: ".$_SG['paginaLoginDashSAT']."?token=$token");
	}
	
?>
