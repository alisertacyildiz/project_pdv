#!/bin/bash
# Programa para verificar os combos gravados
# e retornar uma validação sobre a gravação do combo
# 
# Author: Marcos Marin/Walter Moura
# Data Criação: 23/03/2020
#
# SQLite3
# MAEOFER2 / miraBD 100  - Maestro de Ofertas - Registro de propiedades de oferta
# MAEOFER32 / miraBD 101 - Maestro de Ofertas - Registro de Condiciones
# MAEOFER33  / miraBD 102 - Maestro de Ofertas - Registro de Regalos de la oferta
# MAEGRUP1 / miraBD 103  - Maestro de Grupos de Ofertas
# Documento para orientação
# https://docs.google.com/document/d/1bMr2jsjqvSfVAa1f-ys1TC3U72WtB3ky/edit
#
#

# Definições de variaveis 'globais'
PATH_USER="/root/testsales"
PATH_BKP="$PATH_USER/backup"
PATH_LOG="$PATH_USER/log"
ARQRET="$PATH_USER/analise_oferta.log"
ARQNAME="analise_oferta.log"
ARQARRAYFILES="$PATH_USER/.array_files.txt"
ARQARRAYFILESLOG="$PATH_USER/.array_files_log.txt"
LOG="$PATH_LOG/error.testsales.log"
LOGNAME="error.testsales.log"
ARQTEMP=""
ARQSHOPS=""
ARQOFER="$PATH_USER/ofertas.txt"
ARQCODIBASE="$PATH_USER/codibase.txt"
ARQBASEREGA="$PATH_USER/baserega.txt"
PROMO_RESULT="$PATH_USER/resultado_analise_combo.txt"

# ** Mudança no algoritmo **
# Informar o código de oferta
# Após isso, mostrar quais artigos fazem parte da promoção
# Mostrar as dinâmicas

function create_oferta(){
    /confdia/bin/miraBD 100 | awk -F "|" '{print $1}' > $ARQOFER
}
create_oferta


# Tabela Base Regalo
#VALOR | DESCRIPCIÓN
# 1    | Ticket
# 2    | Sección
# 3    | Familia
# 4    | Subfamilia
# 5    | Producto
# 6-7  | Grupo sección y fin grupo
# 8-9  | Grupo familia y fin grupo
# 10-11| Grupo subfamila y fin grupo
# 12-13| Grupo artículos y fin grupo.
# 14   | Grupo definido.

# Passos:
# - Pegar informações da oferta (código, descrição, limites minimo e maximo);
# - Saber a base de condição (entre 1 e 14);
# - Pegar o Formato de condição (1UN, 2IMPORTE, 3KG);
# - Tipo de relação (0 OBRIGATORIO, 1 OR, 2 AND, 3)*****;
# - Com o codigo base pesquisar na tabela maegroup1;

for oferta in $(cat $ARQOFER); do
    # Pega a descrição da oferta
    desc_ofer=$(echo "SELECT DescOfer FROM maeofer2 WHERE CodiOfer LIKE '$oferta';" | sqlite3 --noheader)

    # Pega limites minimos e máximos e quantidade
    limi_min=$(echo "SELECT LimiMin FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)
    limi_max=$(echo "SELECT LimiMaxi FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)
    cant_cond=$(echo "SELECT CantCond FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)
    
    # Pega o codigo de condição em que os artigos pertencem a promoção
    base_cond=$(echo "SELECT BaseCond FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)

    # Pega o tipo de promoção -> 1UN, 2IMPORTE, 3Kilos
    form_cond=$(echo "SELECT FormCond FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)

    # Pega o tipo relação -> 0 Obrigatorio, 1 OR, 2 e 3 AND
    tipo_rel=$(echo "SELECT TipoRela FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)

    # Pega o código base a ser pesquisado na tabela maegroup1
    codi_base=$(echo "SELECT CodiBase FROM maeofer32 WHERE TipoRegi LIKE '2' AND CodiOfer LIKE '$oferta';" | sqlite3 --noheader)

    case $base_cond in
        "1")
            echo 1
        ;;
        "2")
            echo 2
        ;;
        "3")
            echo 3
        ;;
        "4")
            echo 4
        ;;
        "5")
            echo 5
        ;;
        "6" | "7")
            echo 6 7
        ;;
        "8" | "9")
            echo 8 9
        ;;
        "10" | "11")
            echo 10 11
        ;;
        "12" | "13")
            echo 12 13
        ;;
        "14")
            codigos_dos_artigos=$(echo "SELECT CodiElem FROM maegrup1 WHERE CodiGrup LIKE '$codi_base';" | sqlite3 --noheader)
            codi_arti=$(echo $codigos_dos_artigos | tr ' ' ',')
            echo $desc_ofer >> $PROMO_RESULT
            echo "SELECT Codi, Desc, Pvp_4_ FROM maearti1 WHERE Codi IN ($codi_arti);" | sqlite3 --noheader --column >> $PROMO_RESULT
            echo '---------------------------------------------------------------------' >> $PROMO_RESULT
                        
            # quant_arti=$(echo $codigos_artigos | awk -F " " '{print NF}')
            # echo $oferta '|' $codigos_artigos '|' $quant_arti

            # for i in $( seq 1 8); do
            #     codi_arti=$(echo $codigos_artigos | awk -F " " '{print $i}')
            #     echo $codi_arti
            # done 


            # artigo_promo=$("SELECT Codi FROM maearti1 WHERE Codi = '$codigos_artigos';" | sqlite3 --noheader --column)

        ;;
    esac
done
    # Limpando arquivo de codigos de base
    # echo '' > $ARQCODIBASE
    # echo '' > $ARQBASEREGA

    

function stop(){
for x in $(cat $ARQBASEREGA); do
    case $x in
        "01")
        ;;
        "02")
        ;;
        "03")
        ;;
        "04")
        ;;
        "05")
        ;;
        "06"|"07")
        ;;
        "08"|"09")
        ;;
        "10"|"11")
        ;;
        "12"|"13")
            for i in $(cat $ARQCODIBASE); do
                codiArti=$(/confdia/bin/miraBD 103 | grep $i | awk -F "|" '{ print $3 }' | head -n1); echo $codiArti
                echo  CODIGO OFERTA = $oferta '|' BASE = $codiBase '|' ARTIGO = $codiArti # >>  $TEMPORARY 
                artigo=$(/confdia/bin/miraBD 3 | grep $codiArti | awk -F "|" '{ print $1, $2 }')
                echo $artigo '=>' $descOfer
            done
        ;;
        "14")
        ;;
        *)
    esac
done
}



function useMiraBD(){
for oferta in $(cat $ARQOFER); do
    # Buscando os códigos da oferta
    codiBase=$(/confdia/bin/miraBD 102 | grep $oferta | awk -F "|" '{ print $6 }' | tr -d '0')
    baseRega=$(/confdia/bin/miraBD 102 | grep $oferta | awk -F "|" '{ print $7 }' | tr -d '0')
    descOfer=$(/confdia/bin/miraBD 100 | grep $oferta | awk -F "|" '{ print $13 }')
    echo $codiBase >> $ARQCODIBASE
done
}

function useSelect(){
for oferta in $(cat $ARQOFER); do
    codiBase=$(echo "SELECT DISTINCT CodiBase FROM maeofer32 WHERE CodiOfer LIKE '%$oferta%';" | sqlite3 --noheader)
    echo $codiBase >> $ARQCODIBASE
    if [ $? -eq 0 ];then
        for i in $(cat $ARQCODIBASE); do
            codiArti=$(echo "SELECT DISTINCT CodiElem FROM maegrup1 WHERE CodiGrup LIKE '%$i%';" | sqlite3 --noheader)
            descOfer=$(echo "SELECT DescOfer FROM maeofer2 WHERE CodiOfer LIKE '%$oferta%';" | sqlite3 --noheader)
            artigo=$(echo "SELECT Desc FROM maearti1 WHERE Codi = '$codiArti' ;" | sqlite3 --noheader)

            echo CODIGO OFERTA = $oferta '|' BASE = $codiBase '|' ARTIGO = $codiArti '=>' $descOfer # '&' $artigo
        done
    fi

done
echo '' > $ARQCODIBASE
}


