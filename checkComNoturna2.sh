#!/bin/bash -x
# Programa para verificar as lojas que não transmitiram no dia anterior, e
# e retornar o motivo da não transmissão.
# 
# Author: Marcos Marin/Walter Moura
# Data Criação: 29/10/2019 - 15:51
# Modificado por: Walter Moura/Marcos Marin
# Data Modificação: 03/11/2019 - 20:21
# Versão 2.0

# Definições de variaveis 'globais'
USERACTUAL=$(grep $EUID /etc/group | awk -F ":" '{print $1}')
PATH_USER="/home/$USERACTUAL/programgn"
PATH_BKP="$PATH_USER/backup"
PATH_LOG="$PATH_USER/log"
BD="$PATH_USER/.baseDeDados.db"
ARQRET="$PATH_USER/analise_transmisao.log"
ARQNAME="analise_transmisao.log"
ARQARRAYFILES="$PATH_USER/.array_files.txt"
ARQARRAYFILESLOG="$PATH_USER/.array_files_log.txt"
FILEJOB="$PATH_USER/.jobcom"
LOG="$PATH_LOG/error.programgn.log"
LOGNAME="error.programgn.log"
ARQTEMP="$PATH_USER/.temp_list.txt"
ARQSHOPS="$PATH_USER/.list_shop.txt"
USER="root"
PASS="root"
DTATUAL="$(date +%Y%m%d)"

function TratarErrors(){

    case $ERR in
        "1")
            echo "$(date +%Y%m%d-%H%M%S.%s):TRATARERROR:Erro na execução do comando -> [$ERR]." >> $LOG
            echo "$SHOP""|""Desenvolvedores""|""Erro na execução do comando -> [$ERR]." >> $ARQRET
        ;;
        "2")
            echo "$(date +%Y%m%d-%H%M%S.%s):TRATARERROR:Erro de sintaxe -> [$ERR]." >> $LOG
            echo "$SHOP""|""Desenvolvedores""|""Erro de sintaxe -> [$ERR]" >> $ARQRET
        ;;
        "126")
            echo "$(date +%Y%m%d-%H%M%S.%s):TRATARERROR:Comando não executável (sem permissão) -> [$ERR]." >> $LOG
            echo "$SHOP""|""Desenvolvedores""|""Comando não executável (sem permissão) -> [$ERR]." >> $ARQRET
        ;;
        "127")
            echo "$(date +%Y%m%d-%H%M%S.%s):TRATARERROR:Comando não encontrado ('command not found') -> [$ERR]." >> $LOG
            echo "$SHOP""|""Desenvolvedores""|""Comando não encontrado ('command not found') -> [$ERR]." >> $ARQRET
        ;;
        "128")
            echo "$(date +%Y%m%d-%H%M%S.%s):TRATARERROR:O parâmetro para o 'command' não é um decimal -> [$ERR]." >> $LOG
            echo "$SHOP""|""Desenvolvedores""|""O parâmetro para o 'command' não é um decimal -> [$ERR]." >> $ARQRET
        ;;
        "128+n")
            echo "$(date +%Y%m%d-%H%M%S.%s):TRATARERROR:128 + código do sinal que o matou -> [$ERR]." >> $LOG
            echo "$SHOP""|""Desenvolvedores""|""128 + código do sinal que o matou -> [$ERR]." >> $ARQRET
        ;;
        "130")
            echo "$(date +%Y%m%d-%H%M%S.%s):TRATARERROR:O programa interrompido com o Ctrl+C (128 + 2) -> [$ERR]." >> $LOG
            echo "$SHOP""|""Desenvolvedores""|""O programa interrompido com o Ctrl+C (128 + 2) -> [$ERR]." >> $ARQRET
        ;;
        "255")
            echo "$(date +%Y%m%d-%H%M%S.%s):TRATARERROR:Parâmetro para o 'command' não está entre 0 e 255 -> [$ERR]." >> $LOG
            echo "$SHOP""|""Desenvolvedores""|""Parâmetro para o 'command' não está entre 0 e 255 -> [$ERR]." >> $ARQRET
        ;;
        *)
            echo "$(date +%Y%m%d-%H%M%S.%s):TRATARERROR:Erro não catologado -> [$ERR]." >> $LOG
            echo "$SHOP""|""Desenvolvedores""|""Erro não catologado -> [$ERR]." >> $ARQRET
        ;;
    esac  

}

function main(){

    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inicio programa." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):MAIN:Inciando os menus." >> $LOG
    Menu

}

function Menu(){

    echo "$(date +%Y%m%d-%H%M%S.%s):MENU:Iniciando Menu." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):MENU:Selecionando opção no Menu." >> $LOG
    OPMENU=$(dialog --no-cancel --backtitle "Comunicação Noturna - 2.0" --title "Menu Principal" --menu "System change: " 25 100 50 \
    01 "Comunicação Noturna" \
    02 "Visualizar arquivo de análise" \
    03 "Visualizar Log Programa "\
    04 "Visualizar Log Debug" \
    99 "Exit/Quit" \
    --stdout)

    case $OPMENU in
        "1"|"01")
            echo "$(date +%Y%m%d-%H%M%S.%s):MENU:Opção selecionada -> ['$OPMENU']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):MENU:Chamando o função ['ComunicacaoNoturna']." >> $LOG
            ComunicacaoNoturna
        ;;
        "2"|"02")
            VisualizarArquivoAnalise
        ;;
        "3"|"03")
            VisualizarLogPrograma
        ;;
        "4"|"04")
            VisualizarDebugLog
        ;;
        "5"|"05")
            clear
            AuxInicializarPrograma
        ;;
        "99")
            echo "$(date +%Y%m%d-%H%M%S.%s):MENU:Opção selecionada -> ['$OPMENU']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):MENU:Progama encerrado." >> $LOG
            clear
            exit 1
        ;;
    esac
}   

function ComunicacaoNoturna(){

    clear
    echo "$(date +%Y%m%d-%H%M%S.%s):COMUNICACAONOTURNA:Iniciando Menu 2." >> $LOG
    MENU=$(dialog --backtitle "Comunicação Noturna - 2.0" --title "Menu Secundário" --ok-label "Avançar" --cancel-label "Voltar" --radiolist "Escolha um opção:" 25 100 50 \
          01 "Criar lista de lojas" OFF \
          02 "Selecionar a data para gerar o ralatório" OFF \
          03 "Inicializar relatório" OFF \
          --stdout)

    if [ $? -eq 0 ]; then
        case $MENU in
            "1"|"01")
                clear
                echo "$(date +%Y%m%d-%H%M%S.%s):COMUNICACAONOTURNA:Opção selecionada -> ['$MENU']." >> $LOG
                CreateListShops
            ;;
            "2"|"02")
                clear
                echo "$(date +%Y%m%d-%H%M%S.%s):COMUNICACAONOTURNA:Opção selecionada -> ['$MENU']." >> $LOG
                SelectedDate
            ;;
            "3"|"03")
                clear
                echo "$(date +%Y%m%d-%H%M%S.%s):COMUNICACAONOTURNA:Opção selecionada -> ['$MENU']." >> $LOG
                InicializarRelatorio
            ;;
            *)
                clear
                Menu
            ;;
        esac
    else
        clear
        echo "$(date +%Y%m%d-%H%M%S.%s):COMUNICACAONOTURNA:Retornando para o menu principal." >> $LOG
        Menu
    fi
            
}

function CreateListShops(){

    echo "" > $ARQTEMP
    echo "$(date +%Y%m%d-%H%M%S.%s):CREATELISTSHOPS:Criando arquivo lista de lojas." >> $LOG
    dialog --ok-label "Salvar" --cancel-label "Voltar" --backtitle "Comunicação Noturna - 2.0" --title "Criar arquivo de lojas:" --editbox $ARQTEMP 0 0 2> $ARQTEMP
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        clear
        echo "$(date +%Y%m%d-%H%M%S.%s):CREATELISTSHOPS:Arquivo de lojas criado com sucesso." >> $LOG
        dialog --ok-label "OK" --title "Comunicação Noturna - 2.0" --msgbox "Arquivo de lojas criado com sucesso." 5 70
        ComunicacaoNoturna
    elif [ $RETURN -eq 1 ]; then
        clear
        echo "$(date +%Y%m%d-%H%M%S.%s):CREATELISTSHOPS:Arquivo não criado." >> $LOG
        ComunicacaoNoturna
    elif [ $RETURN -eq 255 ]; then
        clear
        echo "$(date +%Y%m%d-%H%M%S.%s):CREATELISTSHOPS:Arquivo não criado." >> $LOG
        ComunicacaoNoturna
    fi

}

function SelectedDate(){
    
    echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Selecionando data." >> $LOG
    . .jobcom
    DTRELAT=$(dialog --backtitle "Comunicação Noturna - 2.0" --title "Calendário" --no-cancel --ok-label "Confirmar" --date-format "%Y%m%d" --calendar "Selecione a data para gerar o relatório:" 0 0 --stdout)
    RETURN=$?
    DTSELECT=$(echo $DTRELAT | cut -c 1-8)

    if [ $RETURN -eq 0 ]; then
        if [ $DTSELECT -lt $DTATUAL ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Data selecionada é ['$DTSELECT'] < ['$DTATUAL']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Selecionado -> ['$DTRELAT']." >> $LOG
            DATA=$(echo $DTRELAT | cut -c 1-8)
            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Data selecionada -> ['$DATA']." >> $LOG
            DIA=$(echo $DTRELAT | cut -c 7-8)
            DIA=`seq $DIA $DIA`
            DIA=`printf "%d" $DIA`
            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Dia selecionado -> ['$DIA']." >> $LOG
            MES=$(echo $DTRELAT | cut -c 5-6)
            MES=`seq $MES $MES`
            MES=`printf "%d" $MES`
            DESCMESES=`echo "SELECT desc_es FROM tb_mes WHERE mes = '$MES';" | sqlite3 $BD`
            DESCMESEN=`echo "SELECT desc_en FROM tb_mes WHERE mes = '$MES';" | sqlite3 $BD`
            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Mes selecionado -> ['$MES']['$DESCMESEN']['$DESCMESES']." >> $LOG
            ANO=$(echo $DTRELAT | cut -c 1-4)
            ANO=`seq $ANO $ANO`
            ANO=`printf "%d" $ANO`
            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Ano selecionado -> ['$ANO']." >> $LOG

            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Passando os valores acima para o JOBCOM." >> $LOG
            sed "s/^RANO=..../RANO=$ANO/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
            sed "s/^RDATA=......../RDATA=$DATA/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom

            . .jobcom
            if [ ${RDIA} -le 9 ]; then
                sed "s/^RDIA=./RDIA=$DIA/" .jobcom > .jobcom.new
                cp -f .jobcom.new .jobcom
                sed "s/^RMESEN=*.*/RMESEN=$DESCMESEN/" .jobcom > .jobcom.new
                cp -f .jobcom.new .jobcom
                sed "s/^RMESES=*.*/RMESES=$DESCMESES/" .jobcom > .jobcom.new
                cp -f .jobcom.new .jobcom
            else
                sed "s/^RDIA=../RDIA=$DIA/" .jobcom > .jobcom.new
                cp -f .jobcom.new .jobcom
                sed "s/^RMESEN=*.*/RMESEN=$DESCMESEN/" .jobcom > .jobcom.new
                cp -f .jobcom.new .jobcom
                sed "s/^RMESES=*.*/RMESES=$DESCMESES/" .jobcom > .jobcom.new
                cp -f .jobcom.new .jobcom
            fi

            BKDTRELAT=$(date --date="$DATA +1 day" +%Y%m%d)
            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Selecionado BKP -> ['$BKDTRELAT']." >> $LOG
            BKDATA=$(echo $BKDTRELAT | cut -c 1-8)
            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Data selecionada BKP -> ['$BKDATA']." >> $LOG
            BKDIA=$(echo $BKDTRELAT | cut -c 7-8)
            BKDIA=`seq $BKDIA $BKDIA`
            BKDIA=`printf "%d" $BKDIA`
            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Dia selecionado BKP -> ['$BKDIA']." >> $LOG
            BKMES=$(echo $BKDTRELAT | cut -c 5-6)
            BKMES=`seq $BKMES $BKMES`
            BKMES=`printf "%d" $BKMES`
            BKDESCMESES=`echo "SELECT desc_es FROM tb_mes WHERE mes = '$BKMES';" | sqlite3 $BD`
            BKDESCMESEN=`echo "SELECT desc_en FROM tb_mes WHERE mes = '$BKMES';" | sqlite3 $BD`
            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Mes selecionado BKP -> ['$BKMES']['$BKDESCMESEN']['$BKDESCMESES']." >> $LOG
            BKANO=$(echo $BKDTRELAT | cut -c 1-4)
            BKANO=`seq $BKANO $BKANO`
            BKANO=`printf "%d" $BKANO`
            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Ano selecionado BKP -> ['$BKANO']." >> $LOG

            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Passando os valores BKP acima para o JOBCOM." >> $LOG
            sed "s/^BKPANO=..../BKPANO=$BKANO/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
            sed "s/^BKPDATA=......../BKPDATA=$BKDATA/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom

            if [ ${BKPDIA} -le 9 ]; then
                sed "s/^BKPDIA=./BKPDIA=$BKDIA/" .jobcom > .jobcom.new
                cp -f .jobcom.new .jobcom
                sed "s/^BKPMESEN=.../BKPMESEN=$BKDESCMESEN/" .jobcom > .jobcom.new
                cp -f .jobcom.new .jobcom
                sed "s/^BKPMESES=.../BKPMESES=$BKDESCMESES/" .jobcom > .jobcom.new
                cp -f .jobcom.new .jobcom
            else
                sed "s/^BKPDIA=../BKPDIA=$BKDIA/" .jobcom > .jobcom.new
                cp -f .jobcom.new .jobcom
                sed "s/^BKPMESEN=.../BKPMESEN=$BKDESCMESEN/" .jobcom > .jobcom.new
                cp -f .jobcom.new .jobcom
                sed "s/^BKPMESES=.../BKPMESES=$BKDESCMESES/" .jobcom > .jobcom.new
                cp -f .jobcom.new .jobcom
            fi
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Data selecionada é ['$DTSELECT'] >= ['$DTATUAL']." >> $LOG
            dialog --ok-label "OK" --backtitle "Comunicação Noturna - 2.0" --title "Calendário" --msgbox "ERROR:Data selecionada é ['$DTSELECT'] >= ['$DTATUAL']." 5 70
            SelectedDate
        fi
    elif [ $RETURN -eq 255 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Não foi informada a data, será considereda -1 dia." >> $LOG
        dialog --ok-label "OK" --title "Comunicação Noturna - 2.0" --msgbox "ERROR: Não foi informada a data, será considereda -1 dia." 5 70
        DTRELAT=$(date +%Y%m%d --date="-1 day")
        echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Selecionado -> ['$DTRELAT']." >> $LOG
        DATA=$(echo $DTRELAT | cut -c 1-8)
        echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Data selecionada -> ['$DATA']." >> $LOG
        DIA=$(echo $DTRELAT | cut -c 7-8)
        DIA=`seq $DIA $DIA`
        DIA=`printf "%d" $DIA`
        echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Dia selecionado -> ['$DIA']." >> $LOG
        MES=$(echo $DTRELAT | cut -c 5-6)
        MES=`seq $MES $MES`
        MES=`printf "%d" $MES`
        DESCMESES=`echo "SELECT desc_es FROM tb_mes WHERE mes = '$MES';" | sqlite3 $BD`
        DESCMESEN=`echo "SELECT desc_en FROM tb_mes WHERE mes = '$MES';" | sqlite3 $BD`
        echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Mes selecionado -> ['$MES']['$DESCMESEN']['$DESCMESES']." >> $LOG
        ANO=$(echo $DTRELAT | cut -c 1-4)
        ANO=`seq $ANO $ANO`
        ANO=`printf "%d" $ANO`
        echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Ano selecionado -> ['$ANO']." >> $LOG

        echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Passando os valores acima para o JOBCOM." >> $LOG
        sed "s/^RANO=..../RANO=$ANO/" .jobcom > .jobcom.new
        cp -f .jobcom.new .jobcom
        sed "s/^RDATA=......../RDATA=$DATA/" .jobcom > .jobcom.new
        cp -f .jobcom.new .jobcom

        if [ ${RDIA} -le 9 ]; then
            sed "s/^RDIA=./RDIA=$DIA/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
            sed "s/^RMESEN=.../RMESEN=$DESCMESEN/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
            sed "s/^RMESES=.../RMESES=$DESCMESES/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
        else
            sed "s/^RDIA=../RDIA=$DIA/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
            sed "s/^RMESEN=.../RMESEN=$DESCMESEN/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
            sed "s/^RMESES=.../RMESES=$DESCMESES/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
        fi

        
        BKDTRELAT=$(date --date="$DATA +1 day" +%Y%m%d%b)
        echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Selecionado BKP -> ['$BKDTRELAT']." >> $LOG
        BKDATA=$(echo $BKDTRELAT | cut -c 1-8)
        echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Data selecionada BKP -> ['$BKDATA']." >> $LOG
        BKDIA=$(echo $BKDTRELAT | cut -c 7-8)
        BKDIA=`seq $BKDIA $BKDIA`
        BKDIA=`printf "%d" $BKDIA`
        echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Dia selecionado BKP -> ['$BKDIA']." >> $LOG
        BKMES=$(echo $BKDTRELAT | cut -c 5-6)
        BKMES=`seq $BKMES $BKMES`
        BKMES=`printf "%d" $BKMES`
        BKDESCMESES=`echo "SELECT desc_es FROM tb_mes WHERE mes = '$BKMES';" | sqlite3 $BD`
        BKDESCMESEN=`echo "SELECT desc_en FROM tb_mes WHERE mes = '$BKMES';" | sqlite3 $BD`
        echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Mes selecionado BKP -> ['$BKMES']['$BKDESCMESEN']['$BKDESCMESES']." >> $LOG
        BKANO=$(echo $BKDTRELAT | cut -c 1-4)
        BKANO=`seq $BKANO $BKANO`
        BKANO=`printf "%d" $BKANO`
        echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Ano selecionado BKP -> ['$BKANO']." >> $LOG

        echo "$(date +%Y%m%d-%H%M%S.%s):SELECTEDDATE:Passando os valores BKP acima para o JOBCOM." >> $LOG
        sed "s/^BKPANO=..../BKPANO=$BKANO/" .jobcom > .jobcom.new
        cp -f .jobcom.new .jobcom
        sed "s/^BKPDATA=......../BKPDATA=$BKDATA/" .jobcom > .jobcom.new
        cp -f .jobcom.new .jobcom

        if [ ${BKPDIA} -le 9 ]; then
            sed "s/^BKPDIA=./BKPDIA=$BKDIA/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
            sed "s/^BKPMESEN=.../BKPMESEN=$BKDESCMESEN/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
            sed "s/^BKPMESES=.../BKPMESES=$BKDESCMESES/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
        else
            sed "s/^BKPDIA=../BKPDIA=$BKDIA/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
            sed "s/^BKPMESEN=.../BKPMESEN=$BKDESCMESEN/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
            sed "s/^BKPMESES=.../BKPMESES=$BKDESCMESES/" .jobcom > .jobcom.new
            cp -f .jobcom.new .jobcom
        fi
    fi
    ComunicacaoNoturna   

}

function VisualizarArquivoAnalise(){

    dialog --backtitle "Comunicação Noturna - 2.0" --title "Arquivo de Análise" --textbox $ARQRET 25 100
    if [ $? -eq 0 ]; then
        Menu
    else
        Menu
    fi
}

function VisualizarLogPrograma(){

    dialog --backtitle "Comunicação Noturna - 2.0" --title "Log Sistema" --textbox $LOG 0 0
    if [ $? -eq 0 ]; then
        Menu
    else
        Menu
    fi
}

function VisualizarDebugLog(){
    
    dialog --backtitle "Comunicação Noturna - 2.0" --title "Debug" --textbox $PATH_USER/.DebugcheckComNoturna.log.1 0 0
    if [ $? -eq 0 ]; then
        Menu
    else
        Menu
    fi   
}

function AuxInicializarPrograma(){

    clear
    A=$(dialog --stdout --sleep 2 --title "Comunicação Noturna - 2.0" --infobox "Programa em execução." 5 70)
    InicializarRelatorio

}

function InicializarRelatorio(){

    clear
    echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Iniciando a função ['AjustArqLog']." >> $LOG
    AjustArqLog
    echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Iniciando a função ['AjustArqRetorno']." >> $LOG
    AjustArqRetorno
    echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Iniciando a função ['GroupShop']." >> $LOG
    GroupShop
    i=0
    PERC=0
    echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Iniciamos um while no array ['ARRAYSHOPS']" >> $LOG
    while [ $i != ${#ARRAYSHOPS[@]} ]
    do
        echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Percentual -> ['$PERC']." >> $LOG
        (echo $PERC; \
            ) | dialog --backtitle "Comunicação Noturna - 2.0" --title "Processamento" --gauge "Programa em execução -> LOJA:${ARRAYSHOPS[i]}" 8 70 0
        echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Verificamos se o valor ['${ARRAYSHOPS[i]}'] é do tipo númerico." >> $LOG
        if [[ ${ARRAYSHOPS[i]} = ?(+|-)+([0-9]) ]]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Valor ['${ARRAYSHOPS[i]}'] é do tipo número, incluímos 0 a esquerda." >> $LOG
            SHOP=`printf "%05d" ${ARRAYSHOPS[i]}`
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):INICIALIZARRELATORIO:Valor ['${ARRAYSHOPS[i]}'] não é do tipo númerico, será ignorado." >> $LOG
        fi
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:INICIALIZARRELATORIO:Iniciando a função ['GetIP']." >> $LOG
        GetIP
        if [ $RET -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:INICIALIZARRELATORIO:Iniciando a função ['ValidaConexao']." >> $LOG
            ValidaConexao
            if [ $? -eq 0 ]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:INICIALIZARRELATORIO:Enviamos o arquivo ['$FILEJOB'] -> ['$SHOP:$ip']." >> $LOG
                sshpass -p $PASS scp -o ConnectTimeout=1 -P10001 $FILEJOB $USER@$ip:/tmp/
                if [ $? -eq 0 ]; then
                        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:INICIALIZARRELATORIO:Iniciando a função ['VerificaTienda']." >> $LOG
                        VerificaTienda
                    if [ $? -eq 0 ] && [ $STLOJA -eq 0 ]; then
                        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:INICIALIZARRELATORIO:Iniciando a função ['VerificaLojaAberta']." >> $LOG
                        VerificaLojaAberta
                    elif [ $? != 0 ] && [ $STLOJA -eq 1 ]; then
                        echo "$SHOP""|""Indefinido""|""Loja encontrada difere da informada, ['$NUMETIEN']." >> $ARQRET
                        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:INICIALIZARRELATORIO:Loja encontrada difere da informada, ['$NUMETIEN']." >> $LOG
                    else
                        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:INICIALIZARRELATORIO:Erro ao executar comando -> ['$?']." >> $LOG
                        echo "$SHOP""|""Desenvolvedores""|""Erro ao executar comando." >> $ARQRET
                    fi
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:INICIALIZARRELATORIO:Comando não executado -> ['$?']." >> $LOG
                fi
            else
                echo "$SHOP""|""Link""|""Sem Link." >> $ARQRET
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:INICIALIZARRELATORIO:Erro ao conectar com a loja ['${ARRAYSHOPS[i]}']." >> $LOG
            fi
        else
            echo "$SHOP""|""Desenvolvedores""|""Erro ao obter ip da loja." >> $ARQRET
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:INICIALIZARRELATORIO:Erro ao obter ip da loja ['${ARRAYSHOPS[i]}']." >> $LOG
        fi
        let "i = i +1"
        PERC=$(( $PERC + $(( 100 / ${#ARRAYSHOPS[@]} )) ))
        if [ $i -eq ${#ARRAYSHOPS[@]} ]; then
            PERC=100
            (echo $PERC; \
            ) | dialog --backtitle "Comunicação Noturna - 2.0" --title "Processamento" --gauge "Programa em execução -> LOJA:${ARRAYSHOPS[i]}" 8 70 0
        fi
    done
    if [ -e $PATH_USER/.DebugcheckComNoturna.log ]; then
        cp $PATH_USER/.DebugcheckComNoturna.log $PATH_USER/.DebugcheckComNoturna.log.1
    fi
    sleep 2
    clear
    Menu
}

function AjustArqLog(){

    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Iniciando a função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Verifica se existe o diretorio ['$PATH_LOG']." >> $LOG
    if [ -e $PATH_LOG ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Diretório OK." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Verifica se existe o arquivo ['$LOGNAME']." >> $LOG
        if [ -e $LOG ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Arquivo OK." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Verificamos o tamanho do arquivo ['$LOGNAME'']." >> $LOG
            SIZEKB=`du -hsk $LOG | awk -F " " '{print $1;}'`
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Size -> ['$SIZEKB']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Verificamos se o tamanho do log é superior a 100MB." >> $LOG
            if [ $SIZEKB -ge 100000 ]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Arquivo de log maior que 100MB." >> $LOG
                COUNTLOG=`ls -ltr $PATH_BKP/$LOGNAME* | wc -l`
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Verificamos a quntidade de arquivos -> ['$COUNTLOG']." >> $LOG
                if [ $COUNTLOG -eq 5 ]; then
                    cd $PATH_BKP
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Verificamos os arquivos -> ['$PATH_BKP/$LOGNAME']." >> $LOG
                    FILETEMPLOG=`ls -tr $LOGNAME*`
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Preenchemos a variável ['FILETEMPLOG'] com um ['ls $LOGNAME*'] ." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:FILETEMPLOG -> ['$FILETEMPLOG']." >> $LOG
                    echo $FILETEMPLOG > $ARQARRAYFILESLOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Passamos os valores da varável ['FILETEMPLOG'] -> ['$ARQARRAYFILES']." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Iniciamos um while read n arquivo ['$ARQARRAYFILES'] e gravamos cada valor em um array." >> $LOG
                    while read filenamelog;
                        do
                            progsx=("$filenamelog")
                    done < $ARQARRAYFILESLOG
                    ARRAYFILESLOG=(${progsx[0]})
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQAJUSTARQLOGRETORNO:ARRAY Files -> ['$ARRAYFILESLOG']." >> $LOG
                    i=0
                    x=1
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Iniciamos um while no array ['ARRAYFILESLOG']" >> $LOG
                    while [ $i != ${#ARRAYFILESLOG[@]} ]
                    do
                        if [ $i -eq 0 ]; then
                            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Removendo o arquivo ['${ARRAYFILESLOG[i]}']." >> $LOG
                            rm -f ${ARRAYFILESLOG[i]}
                            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Renomendo o arquivo ['${ARRAYFILESLOG[x]}'] -> ['${ARRAYFILESLOG[i]}']." >> $LOG
                            mv -f ${ARRAYFILESLOG[x]} ${ARRAYFILESLOG[i]}
                        else
                            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Renomendo o arquivo ['${ARRAYFILESLOG[x]}'] -> ['${ARRAYFILESLOG[i]}']." >> $LOG
                            mv -f ${ARRAYFILESLOG[x]} ${ARRAYFILESLOG[i]}
                        fi
                        let "i = i +1"
                        let "x = x +1" 
                    done
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Movendo o arquivo ['$LOGNAME'] -> ['$PATH_BKP/$LOGNAME.5']." >> $LOG
                    mv -f $LOGNAME $ARQNAME.5
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Detectado ['$COUNTLOG'] arquivo." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Renomeando este arquivo." >> $LOG
                    cd $PATH_LOG
                    mv -f $LOGNAME $LOGNAME.$COUNTLOG
                    mv -f $LOGNAME.$COUNTLOG $PATH_BKP
                fi
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Arquivo de log menor que 100MB." >> $LOG
            fi
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Arquivo NOK." >> $LOG
        fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQLOG:Diretório NOK." >> $LOG
    fi
    cd $PATH_USER

}

function AjustArqRetorno(){

    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Iniciando a função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Verifica se existe o diretorio ['$PATH_BKP']." >> $LOG
    if [ -e $PATH_BKP ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Diretório OK." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Verifica se existe o arquivo ['$ARQRET']." >> $LOG
        if [ -e $ARQRET ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Arquivo OK." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Verifica a quntidades de arquivos [$ARQNAME] dentro do diretorio ['$PATH_BKP']." >> $LOG
            COUNTBKP=`ls -ltr $PATH_BKP/$ARQNAME* | wc -l`
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Quantidade de arquivos econtrados -> ['$COUNTBKP']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Testamos se o valor retornado é maior que 0." >> $LOG
            if [ $COUNTBKP -eq 0 ]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Valor retornado igual a 0." >> $LOG
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Movendo ['$ARQRET'] -> ['$PATH_BKP/$ARQNAME.0']." >> $LOG
                mv $ARQRET $PATH_BKP/$ARQNAME.0
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Valor retornado maior que 0." >> $LOG
                echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Verificamos se valor retornado é igual a 100." >> $LOG
                if [ $COUNTBKP -eq 101 ]; then
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Valor retornado igual a 100." >> $LOG
                    cd $PATH_BKP
                    FILESTEMP=`ls -tr $ARQNAME*`
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Preenchemos a variável ['FILESTEMP'] com um ['ls $ARQNAME*'] ." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:FILESTEMP -> ['$FILESTEMP']." >> $LOG
                    echo $FILESTEMP > $ARQARRAYFILES
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Passamos os valores da varável ['FILESTEMP'] -> ['$ARQARRAYFILES']." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Iniciamos um while read n arquivo ['$ARQARRAYFILES'] e gravamos cada valor em um array." >> $LOG
                    while read filename;
                        do
                            progs=("$filename")
                    done < $ARQARRAYFILES
                    ARRAYFILES=(${progs[0]})
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:ARRAY Files -> ['$ARRAYFILES']." >> $LOG
                    i=0
                    x=1
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Iniciamos um while no array ['ARRAYFILES']" >> $LOG
                    while [ $i != ${#ARRAYFILES[@]} ]
                    do
                        if [ $i -eq 0 ]; then
                            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Removendo o arquivo ['${ARRAYFILES[i]}']." >> $LOG
                            rm -f ${ARRAYFILES[i]}
                            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Renomendo o arquivo ['${ARRAYFILES[x]}'] -> ['${ARRAYFILES[i]}']." >> $LOG
                            mv -f ${ARRAYFILES[x]} ${ARRAYFILES[i]}
                        else
                            echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Renomendo o arquivo ['${ARRAYFILES[x]}'] -> ['${ARRAYFILES[i]}']." >> $LOG
                            mv -f ${ARRAYFILES[x]} ${ARRAYFILES[i]}
                        fi
                        let "i = i +1"
                        let "x = x +1"
                        if [ $i -eq 100 ]; then
                            break
                        fi 
                    done
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Movendo o arquivo ['$ARQRET'] -> ['$PATH_BKP/$ARQNAME.100']." >> $LOG
                    mv $ARQRET $ARQNAME.100
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Valor é menor que 100." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Movendo o arquivo ['$ARQRET'] -> ['$PATH_BKP/$ARQNAME.$COUNTBKP']." >> $LOG    
                    mv $ARQRET $PATH_BKP/$ARQNAME.$COUNTBKP
                fi
            fi
        fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Diretório NOK." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Criando diretório ['$PATH_BKP']." >> $LOG
        cd $PATH_USER
        mkdir backup
        echo "$(date +%Y%m%d-%H%M%S.%s):AJUSTARQRETORNO:Movendo o arquivo ['$ARQRET'] -> ['$PATH_BKP/$ARQNAME.0']." >> $LOG
        mv $ARQRET $PATH_BKP/$ARQNAME.0
    fi
    cd $PATH_USER
    
}

function GroupShop(){
    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Inciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Verificamos se arquivo ['$ARQTEMP'] existe." >> $LOG
    if [ -e $ARQTEMP ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo OK." >> $LOG
        VARTEMP=`while read linha ; do if [ -z "$linha" ]; then ERR=$(echo $linha)  ; else  printf "%d\t" $linha ; fi ; done < $ARQTEMP`
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Construímos um array com número das lojas e passamos a variável ['VARTEMP']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:VARTEM -> ['$VARTEMP']." >> $LOG
        if [ $? -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Passamos os valores da varável ['VARTEMP'] -> ['$ARQSHOPS'].." >> $LOG
            echo $VARTEMP > $ARQSHOPS
            if [ $? -eq 0 ]; then
                echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Verificamos se arquivo ['$ARQSHOPS'] existe." >> $LOG
                if [ -e $ARQSHOPS ]; then
                    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo OK." >> $LOG
                    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Iniciamos um while read no arquivo ['$ARQSHOPS'] e gravamos cada valor em um array." >> $LOG
                    while read idshop;
                        do
                            progress=("$idshop")
                    done < $ARQSHOPS
                    ARRAYSHOPS=(${progress[0]})
                    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:ARRAY Shops -> ['$ARRAYSHOPS']." >> $LOG
                else
                    echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo NOK." >> $LOG
                fi
            else
                echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Erro ao executar comando VARTEMP -> ARQSHOPS -> ['$?']." >> $LOG
            fi
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Erro ao executar comando VARTEMP -> WHILE -> ['$?']." >> $LOG
        fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):GROUPSHOP:Arquivo NOK." >> $LOG
    fi
}

function GetIP(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Verificamos se a loja ['$SHOP'] existe no banco ['$BD']." >> $LOG
    EXISTE=`mysql --connect-timeout=5 -u dba srvremoto -h 10.106.77.224 -N -e "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP'"`
    RETURN=$?
    if [ $RETURN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno existe ['$EXISTE']." >> $LOG
        if [ $EXISTE -eq 1 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] existe, buscamos o IP." >> $LOG
            ip=`mysql -u dba srvremoto -h 10.106.77.224 -N -e "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP'"`
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno IP -> ['$ip']." >> $LOG
            RET=0
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
            RET=1
        fi
    else
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Erro ao obter IP loja ['$SHOP'] problema ao acessar banco de dados MySQL." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Saindo por contigencia." >> $LOG
        EXISTE=`echo "SELECT COUNT(id) FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno existe ['$EXISTE']." >> $LOG
        if [ $EXISTE -eq 1 ]; then
            ip=`echo "SELECT ip FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Retorno IP -> ['$ip']." >> $LOG
            RET=0
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:GETIP:Loja ['$SHOP'] não econtrada no banco de dados." >> $LOG
            RET=1
        fi
    fi
}

function ValidaConexao(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VALIDACONEXAO:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VALIDACONEXAO:Validando conexão ['$SHOP:$ip']." >> $LOG
    sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip exit
}

function VerificaTienda(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICATIENDA:Verificamos se a loja ['$SHOP'] é a mesma que conectamos pelo ip ['$ip']." >> $LOG
    NUMETIEN=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /confdia/bin/setvari;
    echo $NUMETIEN')
    ERR=$?
    if [ $ERR -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICATIENDA:Valor retornado -> ['$NUMETIEN']." >> $LOG
        if [ $NUMETIEN == $SHOP ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICATIENDA:['$SHOP'] == ['$NUMETIEN']." >> $LOG
            STLOJA="0"
        else
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICATIENDA:['$SHOP'] != ['$NUMETIEN']." >> $LOG
            STLOJA="1"
        fi
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICATIENDA:Iniciamos a função ['VerificaLojaAberta']." >> $LOG
    else
        TratarErrors
    fi
}

function VerificaLojaAberta(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALOJAABERTA:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALOJAABERTA:Validamos o retorno da função ['VarificaLojaAberta']." >> $LOG
    STLABERTA=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom; STINIDIA=$(DT=${RDIA}; DT=$(printf "%d" $DT); if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i -a "^....$DT"; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i -a "^.....$DT"; fi | grep -a -i "syslog: Iniciac.*syslogd succeeded" | tail -n1 | wc -l); echo $STINIDIA')
    ERR=$?
    if [ $ERR -eq 0 ]; then 
        if [ $STLABERTA -eq 0 ]; then
            NTPVS=`echo "SELECT tpvs FROM tb_ip WHERE loja LIKE '$SHOP';" | sqlite3 $BD`
            STCAJAS=0
            n=0
            z=0
            for z in $(seq 2 $NTPVS); do sshpass -p $PASS scp -o ConnectTimeout=1 -P1000$z $FILEJOB $USER@$ip:/tmp/ ; done
            for n in $(seq 2 $NTPVS); do STCAJAS=$(sshpass -p $USER ssh -o ConnectTimeout=1 -p1000$n -l $USER $ip ' . /tmp/.jobcom; cat /confdia/DE/D_E_${RDATA}*.log | grep -a "RETIRADA" | wc -l'); if [ -z $STCAJAS ]; then echo "0"; else if [ $STCAJAS -ge 1 ]; then break; fi; fi; done
            if [ $STCAJAS -ge 1 ]; then 
                STCAJAS="2"
            else 
                STCAJAS="3"
            fi
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALOJAABERTA:['$STCAJAS']." >> $LOG
        fi
        if [ $STLABERTA -eq 1 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALOJAABERTA:['$STLABERTA']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALOJAABERTA:Iniciamos a função ['VerificaCajeraLog']." >> $LOG
            VerificaCajeraLog
        elif [ $STLABERTA -eq 0 ] && [ $STCAJAS -eq 2 ]; then
            echo "$SHOP""|""PDV""|""Problema na máster." >> $ARQRET
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALOJAABERTA:['$STLABERTA']['Problema na máster.']." >> $LOG
        elif [ $STLABERTA -eq 0 ] && [ $STCAJAS -eq 3 ]; then
            echo "$SHOP""|""Operacional""|""Loja não foi aberta." >> $ARQRET
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALOJAABERTA:['$STLABERTA']['Loja não foi aberta.']." >> $LOG
        fi
    else
        TratarErrors
    fi

}

function VerificaCajeraLog(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAJERALOG:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAJERALOG:Validamos o retorno da função ['VarificaTienda']['$STLOJA']." >> $LOG
    if [ $STLOJA -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAJERALOG:Retorno da função ['VerificaTienda'] -> ['$STLOJA']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAJERALOG:Verificamos qual caminho estão os logs necessário para avaliação." >> $LOG
        STVERCACAJERA=$(sshpass -p $USER ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom; comando=$(cat /var/log/cajera.log | grep -a -e "${RDATA}" | wc -l);
        if [ $comando -ge 1 ]; then
            echo "export DIRCAJE="\0"" >> /tmp/.jobcom
            echo "DIRCAJE=0"
        else
            echo "export DIRCAJE="\1"" >> /tmp/.jobcom
            echo "DIRCAJE=1"
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAJERALOG:['$STVERCACAJERA']." >> $LOG
        else
            TratarErrors
        fi
        STBKVERCACAJERA=$(sshpass -p $USER ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom; comando=$(cat /var/log/cajera.log | grep -a -e "${BKPDATA}" | wc -l);
        if [ $comando -ge 1 ]; then
            echo "export BKPDIRCAJE="\0"" >> /tmp/.jobcom
            echo "BKPDIRCAJE=0"
        else
            echo "export BKPDIRCAJE="\1"" >> /tmp/.jobcom
            echo "BKPDIRCAJE=1"
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAJERALOG:['$STVERCACAJERA']." >> $LOG
            VerificaSeFezFimDiaAnt
        else
            TratarErrors
        fi
    fi
}

function VerificaSeFezFimDiaAnt(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICASEFEZFIMDIAANT:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICASEFEZFIMDIAANT:Validamos o Fim Dia." >> $LOG
    STFIMDIA=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom; 
    if [ $DIRCAJE -eq 0 ]; then
        STFIMDIA=$(cat /var/log/cajera.log | grep -a -e  "${RDATA}-2[0123].*/ FIM DE DIA" | wc -l)
        if [ $STFIMDIA -eq 1 ]; then
            echo "1"
        else
            echo "0"
        fi
    else
        STFIMDIA=$(zcat /confdia/backup/cajera.log.gz.* | grep -a -e  "${RDATA}-2[0123].*/ FIM DE DIA" | wc -l)
        if [ $STFIMDIA -eq 1 ]; then
            echo "1"
        else
            echo "0"
        fi
    fi')
    ERR=$?
    if [ $ERR -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICASEFEZFIMDIAANT:['$STFIMDIA']." >> $LOG
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICASEFEZFIMDIAANT:Iniciamos a função ['VerificaInventario']." >> $LOG
        if [ $STFIMDIA -eq 0 ]; then
            STFIMDIAB=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom; 
            if [ $DIRCAJE -eq 0 ]; then
                STFIMDIAB=$(cat /var/log/cajera.log | grep -a -e  "${RDATA}-1[89].*/ FIM DE DIA" | wc -l)
                if [ $STFIMDIAB -eq 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            else
                STFIMDIAB=$(zcat /confdia/backup/cajera.log.gz.* | grep -a -e  "${RDATA}-1[89].*/ FIM DE DIA" | wc -l)
                if [ $STFIMDIAB -eq 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            fi')
        fi
        STFIMDIAB=0
        VerificaInventario
    else
        TratarErrors
    fi

}

function VerificaInventario(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:Validamos o retorno do Fim Dia -> ['$STFIMDIA']." >> $LOG
    if [ $STFIMDIA -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:Validamos o Inventario." >> $LOG
        STINVEN=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom; 
        if [ $DIRCAJE -eq 0 ]; then
            STINVEN=$(cat /var/log/cajera.log | grep -a -e  "${RDATA}-2[0123].*05.*INVENTARIOS" | wc -l)
            if [ $STINVEN -ge 1 ]; then
                echo "1"        
            else
                STINVEN=$(cat /var/log/cajera.log | grep -a -e  "${BKPDATA}-0[012345].*05.*INVENTARIOS" | wc -l)
                if [ $STINVEN -ge 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            fi
        else
            STINVEN=$(zcat /confdia/backup/cajera.log.gz.* | grep -a -e "${RDATA}-2[0123].*05.*INVENTARIOS" | wc -l)
            if [ $STINVEN -ge 1 ]; then
                echo "1"
            else
                STINVEN=$(zcat /confdia/backup/cajera.log.gz.* | grep -a -e "${BKPDATA}-0[012345].*05.*INVENTARIOS" | wc -l)
                if [ $STINVEN -ge 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            fi
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:['$STINVEN']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:Iniciamos a função ['VerificaUltMsgVisorCajera']." >> $LOG
            VerificaUltMsgVisorCajera
        else
            TratarErrors
        fi
    elif [ $STFIMDIAB -eq 1 ] && [ $STFIMDIA -eq 0 ] ; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:Validamos o Inventario." >> $LOG
        STINVEN=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom; 
        if [ $DIRCAJE -eq 0 ]; then
            STINVEN=$(cat /var/log/cajera.log | grep -a -e  "${RDATA}-1[89].*05.*INVENTARIOS" | wc -l)
            if [ $STINVEN -ge 1 ]; then
                echo "1"        
            else
                STINVEN=$(cat /var/log/cajera.log | grep -a -e  "${BKPDATA}-0[012345].*05.*INVENTARIOS" | wc -l)
                if [ $STINVEN -ge 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            fi
        else
            STINVEN=$(zcat /confdia/backup/cajera.log.gz.* | grep -a -e "${RDATA}-1[89].*05.*INVENTARIOS" | wc -l)
            if [ $STINVEN -ge 1 ]; then
                echo "1"
            else
                STINVEN=$(zcat /confdia/backup/cajera.log.gz.* | grep -a -e "${BKPDATA}-0[012345].*05.*INVENTARIOS" | wc -l)
                if [ $STINVEN -ge 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            fi
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:['$STINVEN']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:Iniciamos a função ['VerificaUltMsgVisorCajera']." >> $LOG
            VerificaUltMsgVisorCajera
        else
            TratarErrors
        fi
    elif [ $STFIMDIA -eq 1 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:Validamos o Inventario." >> $LOG
        STINVEN=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom; 
        if [ $DIRCAJE -eq 0 ]; then
            STINVEN=$(cat /var/log/cajera.log | grep -a -e  "${RDATA}-2[0123].*05.*INVENTARIOS" | wc -l)
            if [ $STINVEN -ge 1 ]; then
                echo "1"        
            else
                STINVEN=$(cat /var/log/cajera.log | grep -a -e  "${BKPDATA}-0[012345].*05.*INVENTARIOS" | wc -l)
                if [ $STINVEN -ge 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            fi
        else
            STINVEN=$(zcat /confdia/backup/cajera.log.gz.* | grep -a -e "${RDATA}-2[0123].*05.*INVENTARIOS" | wc -l)
            if [ $STINVEN -ge 1 ]; then
                echo "1"
            else
                STINVEN=$(zcat /confdia/backup/cajera.log.gz.* | grep -a -e "${BKPDATA}-0[012345].*05.*INVENTARIOS" | wc -l)
                if [ $STINVEN -ge 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            fi
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:['$STINVEN']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:Iniciamos a função ['VerificaUltMsgVisorCajera']." >> $LOG
            VerificaUltMsgVisorCajera
        else
            TratarErrors
        fi
    elif [ $STFIMDIAB -eq 0 ] && [ $STFIMDIA -eq 0 ] ; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:Validamos o Inventario." >> $LOG
        STINVEN=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom; 
        if [ $DIRCAJE -eq 0 ]; then
            STINVEN=$(cat /var/log/cajera.log | grep -a -e  "${RDATA}-1[89].*05.*INVENTARIOS" | wc -l)
            if [ $STINVEN -ge 1 ]; then
                echo "1"        
            else
                STINVEN=$(cat /var/log/cajera.log | grep -a -e  "${BKPDATA}-0[012345].*05.*INVENTARIOS" | wc -l)
                if [ $STINVEN -ge 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            fi
        else
            STINVEN=$(zcat /confdia/backup/cajera.log.gz.* | grep -a -e "${RDATA}-1[89].*05.*INVENTARIOS" | wc -l)
            if [ $STINVEN -ge 1 ]; then
                echo "1"
            else
                STINVEN=$(zcat /confdia/backup/cajera.log.gz.* | grep -a -e "${BKPDATA}-0[012345].*05.*INVENTARIOS" | wc -l)
                if [ $STINVEN -ge 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            fi
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:['$STINVEN']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:Iniciamos a função ['VerificaUltMsgVisorCajera']." >> $LOG
            VerificaUltMsgVisorCajera
        else
            TratarErrors
        fi
    fi
    
}

function VerificaUltMsgVisorCajera(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAULTMSGVISORCAJERA:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAULTMSGVISORCAJERA:Validamos o retorno do Inventário -> ['$STINVEN']." >> $LOG
    if [ $STINVEN -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAINVENTARIO:Validamos a mensagem do Visor Cajera." >> $LOG
        STVISOR=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom ;
        if [ $DIRCAJE -eq 0 ]; then
            export VAR_TEMP=`cat /var/log/cajera.log | grep -a ${RDATA} | tail -n1 | cut -d ":" -f 4 | cut -d "/" -f 1 | sed 's/.\{2\}//'`
            VAR_TEMP=`echo ${VAR_TEMP} | tr -d "\n"`
            if [ -z "$VAR_TEMP" ]; then
                echo "1"
            else
                echo "0"
            fi
        else
            export VAR_TEMP=`zcat /confdia/backup/cajera.log.gz.* | grep -a ${RDATA} | tail -n1 | cut -d ":" -f 4 | cut -d "/" -f 1 | sed 's/.\{2\}//'`
            VAR_TEMP=`echo ${VAR_TEMP} | tr -d "\n"`
            if [ -z "$VAR_TEMP" ]; then
                echo "1"
            else
                echo "0"
            fi
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAULTMSGVISORCAJERA:['$STVISOR']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFIVERIFICAULTMSGVISORCAJERACAINVENTARIO:Iniciamos a função ['VerificaSeFaltNao']." >> $LOG
            VerificaSeFaltNao
        else
            TratarErrors
        fi
    elif [ $STINVEN -eq 1 ]; then
        echo "$SHOP""|""Inventário""|""Loja estava em Inventário." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFIVERIFICAULTMSGVISORCAJERACAINVENTARIO:['$STINVEN']['Loja estava em Inventário.']." >> $LOG
    fi

}

function VerificaSeFaltNao(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICASEFALTNAO:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICASEFALTNAO:Validamos o retorno mensagem Visor Cajera -> ['$STVISOR']." >> $LOG
    if [ $STVISOR -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICASEFALTNAO:Validamos se a loja apertou o ultimo ['NÃO']." >> $LOG
        STULTNAO=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom;
        if [ $DIRCAJE -eq 0 ]; then
            STULTNAO=$(cat /var/log/cajera.log | grep -a ${RDATA} | tail -n1 | cut -d " " -f 3)
            if [ "$STULTNAO" == "4:TICKET" ] ; then
                echo "1"
            elif [ "$STULTNAO" == "IMPRESSAO" ]; then
                echo "2"
            elif [ "$STULTNAO" == "ERRO" ]; then
                echo "5"
            else
                STUTLNAO=$(cat /var/log/cajera.log | grep -a ${RDATA} | tail -n1 | cut -d ":" -f4 | tr -d " " | cut -d "/" -f1 | cut -c 1-10)
                if [ "$STUTLNAO" == "DESEJAIMPR" ]; then
                    echo "3"
                elif [ "$STUTLNAO" == "FALTAPAPEL" ]; then
                    echo "4"
                elif [ "$STUTLNAO" == "FALTASAIDA" ]; then
                    echo "6"
                elif [ "$STUTLNAO" == "IMPR.NAOPR" ]; then
                    echo "7"
                else
                    echo "0"
                fi
            fi
        else
            STULTNAO=$(zcat /confdia/backup/cajera.log.gz.* | grep -a ${RDATA} | tail -n1 | cut -d " " -f 3)
            if [ "$STULTNAO" == "4:TICKET" ]; then
                echo "1"
            elif [ "$STULTNAO" == "IMPRESSAO" ]; then
                echo "2"
            elif [ "$STULTNAO" == "ERRO" ]; then
                echo "5"
            else
                STUTLNAO=$(cat /var/log/cajera.log | grep -a ${RDATA} | tail -n1 | cut -d ":" -f4 | tr -d " " | cut -d "/" -f1 | cut -c 1-10)
                if [ "$STUTLNAO" == "DESEJAIMPR" ]; then
                    echo "3"
                elif [ "$STUTLNAO" == "FALTAPAPEL" ]; then
                    echo "4"
                elif [ "$STUTLNAO" == "FALTASAIDA" ]; then
                    echo "6"
                elif [ "$STUTLNAO" == "IMPR.NAOPR" ]; then
                    echo "7"
                else
                    echo "0"
                fi
            fi            
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICASEFALTNAO:['$STULTNAO']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICASEFALTNAO:Iniciamos a função ['VerificaLink']." >> $LOG
            VerificaLink
        else
            TratarErrors
        fi
    elif [ $STVISOR -eq 1 ]; then
        echo "$SHOP""|""Operacional""|""Desligou por chave na 2." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICASEFALTNAO:['$STISTVISORNVEN']['Desligou por chave na 2.']." >> $LOG
    fi

}

function VerificaLink(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALINK:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALINK:Validamos o retorno da tecla NÃO -> ['$STVISOR']." >> $LOG
    if [ $STULTNAO -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALINK:Validamos se houve algum problema de comunicação." >> $LOG
        STLINK=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom ;
        if [ $DIRCAJE -eq 0 ]; then
            export VAR_TEMP=`cat /var/log/cajera.log | grep ${RDATA} | tail -n1 | cut -d ":" -f 4 | cut -d "/" -f 1 | sed 's/.\{2\}//'`
            if [ "$VAR_TEMP" == "  ESPERANDO  CHAMADA  " ];then
                STFASE=`cat /var/log/cajera.log | grep -a "${BKPDATA}.*FASE" | cut -d ":" -f 4 | cut -d "/" -f 1 | sed 's/.\{1\}//' | wc -l`
                if [ $STFASE -ge 1 ]; then
                    STTRANSM=`cat /var/log/cajera.log | grep -a "${BKPDATA}.*  TRANSMITIU          /" | tail -n1 | cut -d ":" -f4 | cut -c 3-12 | wc -l`
                    if [ $STTRANSM -eq 0 ]; then
                        echo "1"
                    else
                        STTRAN=`cat /var/log/cajera.log | grep -a "${BKPDATA}.*  TRANSMITIU          /" | tail -n1 | cut -d ":" -f4 | cut -d "/" -f2`
                        if [ -z $STTRAN ]; then    
                            echo "3"
                        else
                            echo "5"
                        fi
                    fi			
                elif [ "$VAR_TEMP" == "  ESPERANDO  CHAMADA  " ] && [ $STFASE -eq 0 ];then
                    echo "4"
                else
                    echo "0"
                fi
            else
                echo "2"
            fi	
        else
            export VAR_TEMP=`zcat /confdia/backup/cajera.log.gz.* | grep -a ${RDATA} | tail -n1 | cut -d ":" -f 4 | cut -d "/" -f 1 | sed 's/.\{2\}//'`
            if [ "$VAR_TEMP" == "  ESPERANDO  CHAMADA  " ];then
                STFASE=`zcat /confdia/backup/cajera.log.gz.* | grep -a "${BKPDATA}.*FASE" | cut -d ":" -f 4 | cut -d "/" -f 1 | sed 's/.\{1\}//' | wc -l`
                if [ $STFASE -ge 1 ]; then
                    STTRANSM=`cat /var/log/cajera.log | grep -a "${BKPDATA}.*  TRANSMITIU          /" | tail -n1 | cut -d ":" -f4 | cut -c 3-12 | wc -l`
                    if [ $STTRANSM -eq 0 ]; then
                        echo "1"
                    else
                        STTRAN=`cat /var/log/cajera.log | grep -a "${BKPDATA}.*  TRANSMITIU          /" | tail -n1 | cut -d ":" -f4 | cut -d "/" -f2`
                        if [ -z $STTRAN ]; then    
                            echo "3"
                        else
                            echo "5"
                        fi
                    fi
                elif [ "$VAR_TEMP" == "  ESPERANDO  CHAMADA  " ] && [ $STFASE -eq 0 ];then
                    echo "4"
                else
                    echo "0"
                fi
            else
                echo "2"
            fi
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALINK:['$STLINK']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALINK:Iniciamos a função ['VerificaChave4']." >> $LOG
            VerificaChave4
        else
            TratarErrors
        fi
    elif [ $STULTNAO -eq 1 ]; then
	    echo "$SHOP""|""Operacional""|""Loja não apertou o úlimo não, máster ficou em [4:TICKET  6:DIARIO.]." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALINK:['$STISTVISORNVEN']['Loja não apertou o úlimo não, máster ficou em [4:TICKET  6:DIARIO.]']." >> $LOG
    elif [ $STULTNAO -eq 2 ]; then
        echo "$SHOP""|""Operacional""|""Loja não apertou a tecla sim, máster ficou em [IMPRESSAO EM PROCESS]." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALINK:['$STISTVISORNVEN']['Loja não apertou a tecla sim, máster ficou em [IMPRESSAO EM PROCESS]']." >> $LOG
    elif [ $STULTNAO -eq 3 ]; then
        echo "$SHOP""|""Operacional""|""Loja não apertou o úlimo não, máster ficou em [DESEJA IMPRIMIR(S/N)]." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALINK:['$STISTVISORNVEN']['Loja não apertou o úlimo não, máster ficou em [DESEJA IMPRIMIR(S/N)]']." >> $LOG
    elif [ $STULTNAO -eq 4 ]; then
        echo "$SHOP""|""Operacional""|""Loja não apertou a tecla C, máster ficou em [FALTA PAPEL !!! ]." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALINK:['$STISTVISORNVEN']['Loja não apertou a tecla C, máster ficou em [FALTA PAPEL !!! ]']." >> $LOG
    elif [ $STULTNAO -eq 5 ]; then
        echo "$SHOP""|""PDV""|""Erro em Base de Dados." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALINK:['$STISTVISORNVEN']['Erro em Base de Dados']." >> $LOG
    elif [ $STULTNAO -eq 6 ]; then
        echo "$SHOP""|""Operacional""|""Loja não realizou o Fim Dia, máster ficou em [FALTA SAIDA CAIX.!]." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALINK:['$STISTVISORNVEN']['Loja não realizou o Fim Dia, máster ficou em [FALTA SAIDA CAIX.!]']." >> $LOG
    elif [ $STULTNAO -eq 7 ]; then
        echo "$SHOP""|""PDV""|""Máster ficou em [IMPR.NAO PREPARADA!]." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICALINK:['$STISTVISORNVEN']['Loja não realizou o Fim Dia, máster ficou em [FALTA SAIDA CAIX.!]']." >> $LOG
    fi
}

function VerificaChave4(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:Validamos o retorno do Link -> ['$STLINK']." >> $LOG
    if [ $STLINK -eq 0 ] || [ $STLINK -eq 2 ] && [ $STFIMDIA -eq 1 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:Validamos se entrou em Selecione Programa." >> $LOG
        STSP=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom;
        if [ $DIRCAJE -eq 0 ]; then
            STSP=$(cat /confdia/ficcaje/error.ventas.log | grep -a "${RDATA}-2[0123].*SALIDA CONTROLADA" | cut -d ":" -f 3 | tr -d "*" | tail -n1 | wc -l)
            if [ $STSP -eq 1 ]; then
                echo "1"
            else
                STSP=$(cat /confdia/ficcaje/error.cierrtef.log | grep -a "${RDATA}-2[0123].*SALIDA CONTROLADA" | cut -d ":" -f 3 | tr -d "*" | tail -n1 | wc -l)
                if [ $STSP -eq 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            fi 
        else
            STSP=$(zcat /confdia/backup/error.ventas.log.gz.* | grep -a "${RDATA}-2[0123].*SALIDA CONTROLADA" | cut -d ":" -f 3 | tr -d "*" | tail -n1 | wc -l)
            if [ $STSP -eq 1 ]; then
                echo "1"
            else
                STSP=$(zcat /confdia/backup/error.cierrtef.log.gz.* | grep -a "${RDATA}-2[0123].*SALIDA CONTROLADA" | cut -d ":" -f 3 | tr -d "*" | tail -n1 | wc -l)
                if [ $STSP -eq 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            fi            
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:['$STSP']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:Iniciamos a função ['VerificaBoot']." >> $LOG
            VerificaBoot
        else
            TratarErrors
        fi
    elif [ $STLINK -eq 0 ] || [ $STLINK -eq 2 ] && [ $STFIMDIAB -eq 1 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:Validamos se entrou em Selecione Programa." >> $LOG
        STSP=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom;
        if [ $DIRCAJE -eq 0 ]; then
            STSP=$(cat /confdia/ficcaje/error.ventas.log | grep -a "${RDATA}-1[89].*SALIDA CONTROLADA" | cut -d ":" -f 3 | tr -d "*" | tail -n1 | wc -l)
            if [ $STSP -eq 1 ]; then
                echo "1"
            else
                STSP=$(cat /confdia/ficcaje/error.cierrtef.log | grep -a "${RDATA}-2[0123].*SALIDA CONTROLADA" | cut -d ":" -f 3 | tr -d "*" | tail -n1 | wc -l)
                if [ $STSP -eq 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            fi 
        else
            STSP=$(zcat /confdia/backup/error.ventas.log.gz.* | grep -a "${RDATA}-1[89].*SALIDA CONTROLADA" | cut -d ":" -f 3 | tr -d "*" | tail -n1 | wc -l)
            if [ $STSP -eq 1 ]; then
                echo "1"
            else
                STSP=$(zcat /confdia/backup/error.cierrtef.log.gz.* | grep -a "${RDATA}-2[0123].*SALIDA CONTROLADA" | cut -d ":" -f 3 | tr -d "*" | tail -n1 | wc -l)
                if [ $STSP -eq 1 ]; then
                    echo "1"
                else
                    echo "0"
                fi
            fi            
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:['$STSP']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:Iniciamos a função ['VerificaBoot']." >> $LOG
            VerificaBoot
        else
            TratarErrors
        fi
    elif  [ $STLINK -eq 4 ]; then
        STKERNEL=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom;
        STKERNEL=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT.*2[0,1,2,3].*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | wc -l ; else cat /var/log/messages | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | wc -l; fi)
        if [ $STKERNEL -eq 1 ]; then
            if [ $BKPDIRCAJE -eq 0 ]; then
                STKBOOT=$(cat /confdia/ficcaje/error.ventas.log | grep "${BKPDATA}-0[5678].*DESLIGADO INCORRETAMENTE" | cut -d ":" -f 3 | tail -n1 | wc -l)
                if [ $STKBOOT -eq 1 ]; then
                    if [ $DIRCAJE -eq 0 ]; then
                        STLASTHR=$(cat /var/log/cajera.log | grep ${RDATA} | tail -n1 | cut -d ":" -f 1 | cut -c 10-11)
                        STAFTERKERNEL=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -A1 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " "; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -A1 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                        if [ "$STAFTERKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                            echo "1"
                        else
                            STAFTKERNEL=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -A2 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " "; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -A2 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                            if [ "$STAFTKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                                echo "1"
                            else
                                STAFTKERNEL=$(DT=${BKPDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -a "........*0[01235].*UPS Alarm" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                                if [ "$STAFTKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                                    echo "1"
                                else
                                    STUPS=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -a ".*[02][0123].*UPS Alarm - Powerfail detected" | wc -l ; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -a ".*[02][0123].*UPS Alarm - Powerfail detected" | wc -l ; fi)
                                    if [ $STUPS -ge 1 ]; then
                                        echo "1"
                                    else
                                        echo "2"
                                    fi
                                fi
                            fi
                        fi
                    elif [ $DIRCAJE -eq 1 ]; then
                        STLASTHR=$( zcat /confdia/backup/cajera.log.gz.* | grep ${RDATA} | tail -n1 | cut -d ":" -f 1 | cut -c 10-11)
                        STAFTERKERNEL=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -A1 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " "; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -A1 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                        if [ "$STAFTERKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                            echo "1"
                        else
                            STAFTKERNEL=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -A2 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " "; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -A2 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                            if [ "$STAFTKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                                echo "1"
                            else
                                STAFTKERNEL=$(DT=${BKPDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -a "........*0[01235].*UPS Alarm" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                                if [ "$STAFTKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                                    echo "1"
                                else
                                    STUPS=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -a ".*[02][0123].*UPS Alarm - Powerfail detected" | wc -l ; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -a ".*[02][0123].*UPS Alarm - Powerfail detected" | wc -l ; fi)
                                    if [ $STUPS -ge 1 ]; then
                                        echo "1"
                                    else
                                        echo "2"
                                    fi
                                fi
                            fi
                        fi
                    fi
                else
                    echo "0"
                fi
            else
                STKBOOT=$(zcat /confdia/backup/error.ventas.log.gz.* | grep "${BKPDATA}-0[5678].*DESLIGADO INCORRETAMENTE" | cut -d ":" -f 3 | tail -n1 | wc -l)
                if [ $STKBOOT -eq 1 ]; then
                    STLASTHR=$(zcat /var/log/cajera.log.gz.* | grep ${RDATA} | tail -n1 | cut -d ":" -f 1 | cut -c 10-11)
                    STAFTERKERNEL=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -A1 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " "; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -A1 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                    if [ "$STAFTERKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                        echo "1"
                    else
                        STAFTKERNEL=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -A2 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " "; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -A2 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                        if [ "$STAFTKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                            echo "1"
                        else
                            STAFTKERNEL=$(DT=${BKPDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -a "........*0[01235].*UPS Alarm" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                            if [ "$STAFTKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                                echo "1"
                            else
                                STUPS=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -a ".*[02][0123].*UPS Alarm - Powerfail detected" | wc -l ; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -a ".*[02][0123].*UPS Alarm - Powerfail detected" | wc -l ; fi)
                                if [ $STUPS -ge 1 ]; then
                                    echo "1"
                                else
                                    echo "2"
                                fi
                            fi
                        fi
                    fi
                else
                    echo "0"
                fi
            fi
        elif [ $STKERNEL -eq 0 ]; then
            STKERNELOLD=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages.* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT.*2[0,1,2,3].*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | wc -l ; else cat /var/log/messages.* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | wc -l; fi)
            if [ $STKERNELOLD -eq 1 ]; then
                if [ $BKPDIRCAJE -eq 0 ]; then
                    STKBOOT=$(cat /confdia/ficcaje/error.ventas.log | grep "${BKPDATA}-0[5678].*DESLIGADO INCORRETAMENTE" | cut -d ":" -f 3 | tail -n1 | wc -l)
                    if [ $STKBOOT -eq 1 ]; then
                        STLASTHR=$(cat /var/log/cajera.log | grep ${RDATA} | tail -n1 | cut -d ":" -f 1 | cut -c 10-11)
                        STAFTERKERNEL=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -A1 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " "; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -A1 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                        if [ "$STAFTERKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                            echo "1"
                        else
                            STAFTKERNEL=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -A2 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " "; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -A2 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                            if [ "$STAFTKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                                echo "1"
                            else
                                STAFTKERNEL=$(DT=${BKPDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -a "........*0[01235].*UPS Alarm" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                                if [ "$STAFTKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                                    echo "1"
                                else
                                    STUPS=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -a ".*[02][0123].*UPS Alarm - Powerfail detected" | wc -l ; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -a ".*[02][0123].*UPS Alarm - Powerfail detected" | wc -l ; fi)
                                    if [ $STUPS -ge 1 ]; then
                                        echo "1"
                                    else
                                        echo "2"
                                    fi
                                fi
                            fi
                        fi
                    else
                        echo "0"
                    fi
                else
                    STKBOOT=$(zcat /confdia/backup/error.ventas.log.gz.* | grep "${BKPDATA}-0[5678].*DESLIGADO INCORRETAMENTE" | cut -d ":" -f 3 | tail -n1 | wc -l)
                    if [ $STKBOOT -eq 1 ]; then
                        STLASTHR=$(zcat /var/log/cajera.log.gz.* | grep ${RDATA} | tail -n1 | cut -d ":" -f 1 | cut -c 10-11)
                        STAFTERKERNEL=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -A1 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " "; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -A1 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                        if [ "$STAFTERKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                            echo "1"
                        else
                            STAFTKERNEL=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -A2 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " "; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -A2 ".*${STLASTHR}.*ioctl32(dcsbee.exe.*: Unknown cmd fd(30) cmd(8004667e){t:*.*;sz:4}" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                            if [ "$STAFTKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                                echo "1"
                            else
                                STAFTKERNEL=$(DT=${BKPDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -a "........*0[01235].*UPS Alarm" | tail -n1 | cut -d ":" -f4 | tr -d " " ; fi)
                                if [ "$STAFTKERNEL" == "UPSAlarm-Powerfaildetected" ]; then
                                    echo "1"
                                else
                                    STUPS=$(DT=${RDIA}; DT=$(printf "%d" $DT);if [ $DT -ge 10 ]; then cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^....$DT" | grep -a ".*[02][0123].*UPS Alarm - Powerfail detected" | wc -l ; else cat /var/log/messages* | egrep -i -a "^(${RMESEN}|${RMESES})" | grep -i "^.....$DT" | grep -a ".*[02][0123].*UPS Alarm - Powerfail detected" | wc -l ; fi)
                                    if [ $STUPS -ge 1 ]; then
                                        echo "1"
                                    else
                                        echo "2"
                                    fi
                                fi
                            fi
                        fi
                    else
                        echo "0"
                    fi
                fi
            else
                echo "1"
            fi
        else
            echo "0"
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            if [ $STKERNEL -eq 0 ]; then
                echo "$SHOP""|""Link""|""Problema com o link." >> $ARQRET
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:['$STKERNEL']['Problema com o link']." >> $LOG
            elif [ $STKERNEL -eq 1 ]; then
                echo "$SHOP""|""Manutenção""|""Detectado falha de energia [UPSAlarm-Powerfaildetected]." >> $ARQRET
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:['$STKERNEL']['Detectado falha de energia [UPSAlarm-Powerfaildetected]']." >> $LOG
            elif [ $STKERNEL -eq 2 ]; then
                echo "$SHOP""|""PDV""|""Máster travada." >> $ARQRET
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:['$STKERNEL']['Error de sistema ['ioctl32(dcsbee.exe:[]): Unknown cmd fd(30) cmd(8004667e)']']." >> $LOG
            fi
        else
            TratarErrors
        fi
    elif [ $STLINK -eq 1 ]; then
        echo "$SHOP""|""Link""|""Problema com o link." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:['$STKERNEL']['Problema com o link']." >> $LOG
    elif [ $STLINK -eq 2 ] && [ $STFIMDIA -eq 0 ]; then
        echo "$SHOP""|""Operacional""|""Loja não fez o fim dia ou realizou processo fora do horário." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:['$STKERNEL']['Loja não fez o fim dia ou realizou processo fora do horário']." >> $LOG
    elif [ $STLINK -eq 3 ]; then
        echo "$SHOP""|""PDV""|""Loja transmitiu." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:['$STKERNEL']['Loja transmitiu']." >> $LOG
    elif [ $STLINK -eq 5 ]; then
        echo "$SHOP""|""Link""|""Transmissão interrompida." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICACHAVE4:['$STKERNEL']['Transmissão interrompida.']." >> $LOG
    fi
}

function VerificaBoot(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:Validamos o retorno Selecione Programa -> ['$STSP']." >> $LOG
    if [ $STSP -eq 0 ] && [ $STFIMDIAB -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:Validamos se Desligou Incorretamente." >> $LOG
        STBOOT=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom;
        if [ $DIRCAJE -eq 0 ]; then
            STBOOT=$(cat /confdia/ficcaje/error.ventas.log | grep "${RDATA}-2[0123].*DESLIGADO INCORRETAMENTE" | cut -d ":" -f 3 | tail -n1 | wc -l)
            if [ $STBOOT -eq 1 ]; then
                echo "1"
            else
                echo "0"
            fi
        else
            STBOOT=$(zcat /confdia/backup/error.ventas.log.gz.* | grep "${RDATA}-2[0123].*DESLIGADO INCORRETAMENTE" | cut -d ":" -f 3 | tail -n1 | wc -l)
            if [ $STBOOT -eq 1 ]; then
                echo "1"
            else
                echo "0"
            fi
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:['$STBOOT']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:Iniciamos a função ['VerificaEBD']." >> $LOG
            VerificaEBD
        else
            TratarErrors
        fi
    elif [ $STSP -eq 0 ] && [ $STFIMDIAB -eq 1 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:Validamos se Desligou Incorretamente." >> $LOG
        STBOOT=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom;
        if [ $DIRCAJE -eq 0 ]; then
            STBOOT=$(cat /confdia/ficcaje/error.ventas.log | grep "${RDATA}-1[89].*DESLIGADO INCORRETAMENTE" | cut -d ":" -f 3 | tail -n1 | wc -l)
            if [ $STBOOT -eq 1 ]; then
                echo "1"
            else
                echo "0"
            fi
        else
            STBOOT=$(zcat /confdia/backup/error.ventas.log.gz.* | grep "${RDATA}-1[89].*DESLIGADO INCORRETAMENTE" | cut -d ":" -f 3 | tail -n1 | wc -l)
            if [ $STBOOT -eq 1 ]; then
                echo "1"
            else
                echo "0"
            fi
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:['$STBOOT']." >> $LOG
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:Iniciamos a função ['VerificaEBD']." >> $LOG
            VerificaEBD
        else
            TratarErrors
        fi
    elif [ $STSP -eq 1 ]; then
        echo "$SHOP""|""PDV""|""Máster entrou em selecione programa." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:['$STKERNEL']['Máster entrou em selecione programa']." >> $LOG 
    fi    
}

function VerificaEBD(){
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAEBD:Iniciando função." >> $LOG
    echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAEBD:Validamos o retorno Verifica Boot -> ['$STBOOT']." >> $LOG
    if [ $STBOOT -eq 0 ] && [ $STFIMDIAB -eq 0 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAEBD:Validamos se entrou em EBD." >> $LOG
        STEBD=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom;
        if [ $DIRCAJE -eq 0 ]; then
            STEBD=$(cat /var/log/cajera.log | grep -a -e "${RDATA}-2[0123].* ARQU. dat NUMERO)" | tail -n1 | wc -l)
            if [ $STEBD -eq 1 ]; then
                echo "1"
            else
                STEEBD=$(cat /var/log/cajera.log | grep -a "${RDATA}-2[0123].*ERRO EM BASE DADOS" | tail -n1 | wc -l)
                if [ $STEEBD -eq 1 ]; then
                    echo "2"
                else
                    echo "0"
                fi
            fi
        else
            STEBD=$(zcat /confdia/backup/cajera.log.gz.* | grep -a -e "${RDATA}-2[0123].* ARQU. dat NUMERO)" | tail -n1 | wc -l)
            if [ $STEBD -eq 1 ]; then
                echo "1"
            else
                STEEBD=$(zcat /confdia/backup/cajera.log.gz.* | grep -a "${RDATA}-2[0123].*ERRO EM BASE DADOS" | tail -n1 | wc -l)
                if [ $STEEBD -eq 1 ]; then
                    echo "2"
                else
                    echo "0"
                fi
            fi
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAEBD:['$STEBD']." >> $LOG
            if [ $STEBD -eq 1 ]; then
                echo "$SHOP""|""PDV""|""Recuperando arquivos." >> $ARQRET
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:['$STEBD']['Recuperando arquivos']." >> $LOG
            elif [ $STEBD -eq 2 ]; then
                echo "$SHOP""|""PDV""|""Erro em Base de Dados." >> $ARQRET
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:['$STEBD']['Erro em Base de Dados']." >> $LOG
            elif [ $STEBD -eq 0 ]; then
                echo "$SHOP""|""Desenvolvedores""|""Erro não catalogado." >> $ARQRET
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:['$STEBD']['Erro não catalogado']." >> $LOG
            fi
        else
            TratarErrors
        fi
    elif [ $STBOOT -eq 0 ] && [ $STFIMDIAB -eq 1 ]; then
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAEBD:Validamos se entrou em EBD." >> $LOG
        STEBD=$(sshpass -p $PASS ssh -o ConnectTimeout=1 -p10001 -l $USER $ip '. /tmp/.jobcom;
        if [ $DIRCAJE -eq 0 ]; then
            STEBD=$(cat /var/log/cajera.log | grep -a -e "${RDATA}-1[89].* ARQU. dat NUMERO" | tail -n1 | wc -l)
            if [ $STEBD -eq 1 ]; then
                echo "1"
            else
                STEEBD=$(cat /var/log/cajera.log | grep -a "${RDATA}-1[89].*ERRO EM BASE DADOS" | tail -n1 | wc -l)
                if [ $STEEBD -eq 1 ]; then
                    echo "2"
                else
                    echo "0"
                fi
            fi
        else
            STEBD=$(zcat /confdia/backup/cajera.log.gz.* | grep -a -e "${RDATA}-1[89].* ARQU. dat NUMERO" | tail -n1 | wc -l)
            if [ $STEBD -eq 1 ]; then
                echo "1"
            else
                STEEBD=$(zcat /confdia/backup/cajera.log.gz.* | grep -a "${RDATA}-1[89].*ERRO EM BASE DADOS" | tail -n1 | wc -l)
                if [ $STEEBD -eq 1 ]; then
                    echo "2"
                else
                    echo "0"
                fi
            fi
        fi')
        ERR=$?
        if [ $ERR -eq 0 ]; then
            echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICAEBD:['$STEBD']." >> $LOG
            if [ $STEBD -eq 1 ]; then
                echo "$SHOP""|""PDV""|""Recuperando arquivos." >> $ARQRET
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:['$STEBD']['Recuperando arquivos']." >> $LOG
            elif [ $STEBD -eq 2 ]; then
                echo "$SHOP""|""PDV""|""Erro em Base de Dados." >> $ARQRET
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:['$STEBD']['Erro em Base de Dados']." >> $LOG
            elif [ $STEBD -eq 0 ]; then
                echo "$SHOP""|""Desenvolvedores""|""Erro não catalogado." >> $ARQRET
                echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:['$STEBD']['Erro não catalogado']." >> $LOG
            fi
        else
            TratarErrors
        fi
    elif [ $STBOOT -eq 1 ]; then
        echo "$SHOP""|""Operacional""|""Caixa desligado incorretamente." >> $ARQRET
        echo "$(date +%Y%m%d-%H%M%S.%s):L$SHOP:VERIFICABOOT:['$STEBD']['Caixa desligado incorretamente']." >> $LOG
    fi
}

if [ -e $PATH_USER/.DebugcheckComNoturna.log ]; then
    cp $PATH_USER/.DebugcheckComNoturna.log $PATH_USER/.DebugcheckComNoturna.log.1
fi
exec 2> $PATH_USER/.DebugcheckComNoturna.log
set -x
main
