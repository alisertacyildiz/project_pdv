# !/bin/bash
# consultaVersão.sh
# Script para consulta de versão de lojas
# Versão 0.1 - Inclusão do código
# Versão 1.0 - Ajuste do código: incluido função sshpass; ajuste no parâmetro '-v'
# Versão 1.1 - Melhoria adicionada: incluido validação do arquivo lojas.txt
# Autor: Marcos Lima Marinho

# Mensagem de ajuda
MENS_AJUDA=$(echo -e"
Uso: $0 [-h|-v] \n
-h, --help  script teste \n
-v, --version  mostra a versão
")

case $1 in
    -h | --help)
        echo $MENS_AJUDA
        exit 0
    ;;
    -v | --version)
        grep '^# Versão' "$0" | tail -n1 | cut -d \- -f 1 | tr -d \#
        exit 0
    ;;
    *)
        if test "$1"
        then
            echo "Opção invalida: $1"
            exit 1
        fi
esac

# Validação do arquivo 'lojas.txt'

test -e lojas.txt
    if [ $? -eq 0 ]; then
# Pocessamento
        for line in $(cat lojas.txt)
        do
            nume_caja=$(sshpass -p "root" ssh -p10001 root@$line -o ConnectTimeout=1 '. /confdia/bin/setvari;
                                                                                            echo "$NUMETPVS"')
            for i in $(seq 1 $nume_caja)
            do
                versao=$(sshpass -p "root" ssh -p1000$i root@$line -o ConnectTimeout=1 'cat /confdia/version')
                info_tienda=$(sshpass -p "root" ssh -p1000$i root@$line -o ConnectTimeout=1 '. /confdia/bin/setvari;
                                                                        echo "$NUMETIEN""|""$NUMECAJA""|""$CODIPDV"')
                echo "$info_tienda""|""$versao" >> resultado.txt
            done
        done
    else
        echo -e "Arquivo 'lojas.txt' não existe !!!"
    fi