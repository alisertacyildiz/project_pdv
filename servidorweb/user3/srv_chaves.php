<?php
    include_once("../security/seguranca.php");
	protegePagina();
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicon.ico">

    <title>Support TPVs | Servidor de Chaves</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dashboard.css" rel="stylesheet">

  </head>

  <body>
  
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="user.php">Usuário -
		<?php  
		  echo $_SESSION['usuarioNome'];
		  ?>
	  </a>      
      <a class="w-100"> </a>
		<ul class="navbar-nav px-3">
        	<li class="nav-item text-nowrap">
				<a class="nav-link" href="srv_chaves.php"><span data-feather="server"></span>
                  Servidor de Chaves
                </a>
			</li>
		</ul>
		<ul class="navbar-nav px-3">
        	<li class="nav-item text-nowrap">
				<a class="nav-link" href="#?" data-toggle="modal" data-target="#editaSenha"><span data-feather="lock"></span>
				Alterar Senha
				</a>
			</li>
		</ul>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
			<a class="nav-link" href="../security/sair.php"><span data-feather="share"></span>
			Sair</a>
        </li>
      </ul>
    </nav>
        <?php
			date_default_timezone_set("America/Sao_Paulo");
     		setlocale(LC_ALL, 'pt_BR');
			$num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
			//Obter a data atual
			$resultado_qnt_cadastros = mysqli_query($conn, $num_users);
			$row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
			
			$data['atual'] = date('Y-m-d H:i:s');	

			//Diminuir 20 segundos 
			$data['online'] = strtotime($data['atual'] . " - 20 seconds");
			$data['online'] = date("Y-m-d H:i:s",$data['online']);
			
			//Pesquisar os ultimos usuarios online nos 20 segundo
			$result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
			
			$resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
			$row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
			
			$qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
		?>
		<script src="../js/jquery-3.2.1.min.js"></script>
		
		<script>
		//Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
		setInterval(function(){
			//Incluir e enviar o POST para o arquivo responsável em fazer contagem
			$.post("../processo/processa_vis.php", {contar: '',}, function(data){
				$('#online').text(data);
			});
		}, 10000);
		</script>
        <form name="frm_senha" action="../processo/salva_senha.php" method="POST">
              <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                          <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                        </div>
                        <div class="form-group">
                          <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                          <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Confirma</button>
                    </div>
                  </div>
                </div>
              </div>
        </form>
        <main role="main" class="col-md-10  ml-sm-auto col-lg-4 pt-4 px-4">
		<form id="frm_consulta" action="srv_chaves.php" method="POST">
					<div class="input-group mb-1">
						<input type="text hidden" class="form-control" id="id_loja" name="nume_loja"  placeholder="Digite o número da loja">
						<div class="input-group-append">
						<!--
							<button class="btn btn-outline-secondary" type="button submit" id="myBtn" data-toggle="modal" data-target="#consulta_chaves" data-whatever="Resultado Loja - ">Consultar</button>-->
							<button class="btn btn-outline-secondary" type="submit" >Consultar</button>
						</div>
					</div>
						<?php
			  				include_once("../processo/consulta_chaves.php");
							get_num_loja();	
					  	?>
		</form>
		</main>
		<main role="main" class="col-md-10 ml-sm-10 col-lg-10 pt-4 px-4">

		
		<div class="col-md-4">
				<button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#nova_chave_sat">Nova Chave SAT</button>
				<button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#nova_chave_nfce">Nova Chave NFCe</button>
		</div>
		</br>
		<div class="col-sm-6" style="float:left;">
			<table class="table table-striped table-bordered table-hover" id="tb_sat">
				
							<thead>
								<tr>
								  <th>#</th>
								  <th>SAT</th>
								</tr>
							</thead>
									<tr>
									<?php
											echo "<tbody>";
											$path='/var/www/html/resorces/srv_chaves/arquivos/claves_sat';
											$diretorio = dir($path);
												while($arquivo = $diretorio -> read()) 										
													{	
														if ($arquivo != '..' && $arquivo != '.'){
																echo "<tr>";
																echo "<td><img width='40px' height='25px' src='../img/zip.jpg'></td>";
																echo "<td><a href='../srv_chaves/arquivos/claves_sat/".$arquivo."'>".$arquivo."</a></td>";
																echo "</tr>";		
															}
													}
													$diretorio -> close();
											echo "</tbody>";
										?>
					</table>
				</div>
		<div class="col-sm-6" style="float:left;">
			<table class="table table-striped table-bordered table-hover" id="tb_nfce">
				
							<thead>
								<tr>
								  <th>#</th>
								  <th>NFCe</th>
								</tr>
							</thead>
									<tr>
										<?php
											echo "<tbody>";
											$path2='/var/www/html/resorces/srv_chaves/arquivos/claves_nfce';
											$diretorio2 = dir($path2);
												while($arquivo2 = $diretorio2 -> read()) 										
													{	
														if ($arquivo2 != '..' && $arquivo2 != '.'){
																echo "<tr>";
																echo "<td><img width='40px' height='25px' src='../img/zip.jpg'></td>";
																echo "<td><a href='../srv_chaves/arquivos/claves_nfce/".$arquivo2."'>".$arquivo2."</a></td>";
																echo "</tr>";		
															}
													}
													$diretorio2 -> close();
											echo "</tbody>";
										?>
					</table>
		</div>
		</main>

	<form name="frm_nova_chave" action="../processo/nova_chave.php" method="POST">
		<div class="modal fade" id="nova_chave_sat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title" id="exampleModalLabel">Gerar Chaves</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  </div>
				  <div class="modal-body">
					<form>
					  <div class="form-group">
						<label for="recipient-name" class="control-label">Loja:</label>
						<input type="text" class="form-control" name="loja">
					  </div>
					  <div class="form-group">
						<label for="recipient-name" class="control-label">CFe-Key:</label>
						<input type="text" class="form-control" name="cfe-key">
					  </div>
					  <div class="form-group">
						<label for="message-text" class="control-label">AC-SAT</label>
							<textarea type="text" class="form-control" name="ac-sat"></textarea>
					  </div>
					</form>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-success">Enviar</button>
				  </div>
				</div>
			  </div>
			</div>
		</form>
	<form name="frm_nova_chave_nfce" action="../processo/nova_chave_nfce.php" method="POST">
		<div class="modal fade" id="nova_chave_nfce" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title" id="exampleModalLabel">Gerar Chaves</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  </div>
				  <div class="modal-body">
					<form>
					  <div class="form-group">
						<label for="recipient-name" class="control-label">Loja:</label>
						<input type="text" class="form-control" name="loja">
					  </div>
					  <div class="form-group">
						<label for="recipient-name" class="control-label">CFe-Key:</label>
						<input type="text" class="form-control" name="cfe-key">
					  </div>
					  <div class="form-group">
						<label for="recipient-name" class="control-label">ID:</label>
						<select name="id" class="custom-select navbar navbar-grey sticky-top bg-grey ">
							<option value="000001">000001</option>
							<option value="000002">000002</option>
							<option value="000003">000003</option>
							<option value="000004">000004</option> 
						</select>  
					  </div>
					  <div class="form-group">
						<label for="message-text" class="control-label">Token:</label>
							<input type="text" class="form-control" name="token">
					  </div>
					</form>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-success">Enviar</button>
				  </div>
				</div>
			  </div>
			</div>
		</form>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="../js/jquery-3.2.1.min.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="../js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-slim.min.js"><\/script>')</script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="../js/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
  </body>
</html>
