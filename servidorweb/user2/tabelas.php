<?php
    include_once("../security/seguranca.php");
    protegePagina();    
?>

<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicon.ico">

    <title>Support TPVs | User N2</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dashboard.css" rel="stylesheet">


  </head>

  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="user2.php">User N2 -
		<?php  
		  echo $_SESSION['usuarioNome'];
		  ?>
	  </a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="../security/sair.php"><span data-feather="share"></span>
           Sair</a>
        </li>
      </ul>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="user2.php">
                  <span data-feather="home"></span>
                  Home<span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="consultas.php">
                  <span data-feather="search"></span>
                  Consultas
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="sats.php?pagina=1">
                  <span data-feather="camera"></span>
                  SATs
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="list_lojas.php?pagina=1">
                  <span data-feather="list"></span>
                  Cadastro de Lojas
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="srv_chaves.php">
                  <span data-feather="server"></span>
                  Servidor de Chaves
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="usuarios.php">
                  <span data-feather="users"></span>
                  Usuarios<span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#?" data-toggle="modal" data-target="#editaSenha">
                  <span data-feather="lock"></span>
                  Alterar Senha
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="tabelas.php">
                  <span data-feather="grid"></span>
                  Tabelas  
                </a>
              </li>
            </ul>

           </div>
        </nav>
     </div>
  </div>
  <?php
              date_default_timezone_set("America/Sao_Paulo");
              setlocale(LC_ALL, 'pt_BR');
              $num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
              //Obter a data atual
              $resultado_qnt_cadastros = mysqli_query($conn, $num_users);
              $row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
              
              $data['atual'] = date('Y-m-d H:i:s'); 

              //Diminuir 20 segundos 
              $data['online'] = strtotime($data['atual'] . " - 20 seconds");
              $data['online'] = date("Y-m-d H:i:s",$data['online']);
              
              //Pesquisar os ultimos usuarios online nos 20 segundo
              $result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
              
              $resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
              $row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
              
              $qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
        ?>  
            <!--<h1>Quantidade de usuários online: <span id='online'><?php echo $row_qnt_visitas['online']; ?></span></h1>-->
            
        <script src="../js/jquery-3.2.1.min.js"></script>
            
        <script>
            //Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
            setInterval(function(){
              //Incluir e enviar o POST para o arquivo responsável em fazer contagem
              $.post("../processo/processa_vis.php", {contar: '',}, function(data){
                $('#online').text(data);
              });
            }, 10000);
        </script>
        <form name="frm_senha" action="../processo/salva_senha.php" method="POST">
              <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                          <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                        </div>
                        <div class="form-group">
                          <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                          <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Confirma</button>
                    </div>
                  </div>
                </div>
              </div>
        </form> 
        <main role="main" class="col-md-4 ml-sm-auto col-lg-10 pt-4 px-4">
        <div class="row">
            <div class="col-sm-6" style="float:left;">
            </div>
            <div class="col-sm-6" style="float:left;">
                <h4>UPDATE BANCOS</h4>
                <div class="table-responsive">
                    <table class="table table-sm table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>BANCO</th>
                                <th>DATA ATUALIZAÇÃO</th>
                                <th>STATUS</th>
                            </tr>
                        </thead>
                            <?php
                                $sql_update_banco = "SELECT id, banco, data_ult_atualizacao, data_atualizacao, status FROM tb_updates_banco";
                                $query_update_banco = mysqli_query($conn, $sql_update_banco);
                                echo "<tbody>";
                                while ($result_update_banco = mysqli_fetch_array($query_update_banco))
                                {
                                    $idpudtbd = $result_update_banco['id'];
                                    $bancoupdtbd = $result_update_banco['banco'];
                                    $dtultatualupdtbd = $result_update_banco['data_ult_atualizacao'];
                                    $dtatualupdtbd = $result_update_banco['data_atualizacao'];
                                    $statusupdtbd = $result_update_banco['status'];

                                    echo "<tr>";
                                        echo "<td>".$idpudtbd."</td>";
                                        echo "<td>".$bancoupdtbd."</td>";
                                        echo "<td>".$dtatualupdtbd."</td>";
                                        if($statusupdtbd == "Atualizado")
                                        {
                                            echo "<td><img width='15px' height='15px' src='../img/atualizado.png' /> ".$statusupdtbd."</td>";
                                        }else if ($statusupdtbd == "Executando"){
                                            echo "<td><img width='15px' height='15px' src='../img/alerta.png' /> ".$statusupdtbd."</td>";
                                        }else{
                                            echo "<td><img width='15px' height='15px' src='../img/desatualizado.png' /> ".$statusupdtbd."</td>";
                                        }
                                    echo "</tr>";
                                }
                                echo "</tbody>";
                            ?>
                    </table>
                </div>
            </div>
        </div>
        <h4>Versões de PDV</h4>
        </br>
        <div class="row">
            <div class="col-sm-6" style="float:left;">
                <div class="table-responsive">
                    <table class="table table-sm table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Versão</th>
                                <th>Quantidade de Lojas</th>
                                <th>Visualizar</th>
                            </tr>
                        </thead>
                            <?php
                                $sql_group_versao = "SELECT versao, COUNT(id) AS qntd FROM tb_dados_loja WHERE data BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() GROUP BY versao" ;
                                $query_group_versao = mysqli_query($conn, $sql_group_versao);
                                echo "<tbody>";
                                $i=1;
                                while ($result_group_versao = mysqli_fetch_array($query_group_versao))
                                {
                                    $vazio = $i;
                                    $versao = $result_group_versao['versao'];
                                    $qntd = $result_group_versao['qntd'];

                                    echo "<tr data-toggle='modal' data-target='#modal_dados_loja".$result_group_versao['versao']."'>";
                                        echo "<td>".$vazio."</td>";
                                        echo "<td>".$versao."</td>";
                                        echo "<td>".$qntd."</td>";
                                        echo "<td><button type='button' class='btn btn-outline-info btn-sm' data-toggle='modal' data-target='#modal_dados_loja".$result_group_versao['versao']."'>Visualizar</button></a></td>";
                                    echo "</tr>";
                                    echo "<div class='modal fade' id='modal_dados_loja".$result_group_versao['versao']."' tabindex='-1' role='dialog' aria-hidden='true'>";
                                        echo "<div class='modal-dialog modal-dialog-scrollable modal-lg'>";
                                            echo "<div class='modal-content'>";
                                                echo "<div class='modal-header'>";
                                                    echo "<h5>Dados da Loja - ".$result_group_versao['versao']."</h5>";
                                                    echo "<button type='button' class='close' data-dismiss='modal'>&times</button>";
                                                echo "</div>";
                                                echo "<div class='modal-header'>";
                                                    echo '<a href="../export/export_dados_loja.php?vs='.$result_group_versao['versao'].'"><button type="submit" class="btn btn-danger">Exportar</button></a>';
                                                echo "</div>";
                                                echo "<div class='modal-body'>";
                                                    echo "<form>";
                                                        echo '<div class="form-group">';
                                                            echo '<div class="row">';
                                                                echo '<div class="col-md-2">Loja</div>';
                                                                echo '<div class="col-md-5">Versão</div>';
                                                                echo '<div class="col-md-3">Qntd PDVs</div>';
                                                                echo '<div class="col-md-1">IP</div>';
                                                            echo "</div>";
                                                            echo '<div class="row">';
                                                                $sql_versao = 'SELECT * FROM tb_dados_loja WHERE Versao = "'.$result_group_versao['versao'].'"' ;
                                                                $query_versao = mysqli_query($conn, $sql_versao);
                                                                while ($result_versao = mysqli_fetch_array($query_versao))
                                                                {
                                                                    $nloja = $result_versao['Loja'];
                                                                    $nversao = $result_versao['Versao'];
                                                                    $ntpvs = $result_versao['Qntd_Tpvs'];
                                                                    $nip = $result_versao['IP'];

                                                                    echo '<div class="col-md-2">'.$nloja.'</div>';
                                                                    echo '<div class="col-md-5">'.$nversao.'</div>';
                                                                    echo '<div class="col-md-3">'.$ntpvs.'</div>';
                                                                    echo '<div class="col-md-1">'.$nip.'</div>'; 
                                                                }
                                                            echo '</div>';
                                                        echo '</div>';
                                                    echo "</form>";
                                                echo "</div>";
                                            echo "</div>";
                                        echo "</div>";
                                    //echo "</div>";
                                    $i++;

                                }
                                echo "</tbody>";
                            ?>
                        </table>
                    </div>
                </div>
            </div>
            <h4>Dados dos S@Ts</h4>
            </br>
            <div class='row'>
              <div class="col-sm-4" style="float:left;">
                <div class="table-responsive">
                  <table class="table table-sm table-striped table-bordered table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Firmware</th>
                              <th>Quantidade S@Ts</th>
                              <th>Visualizar</th>
                          </tr>
                      </thead>
                          <?php
                              $sql_group_fwsat = "SELECT firmware, COUNT(id) AS qntd FROM tb_sat WHERE Status = 'Ativo' GROUP BY firmware" ;
                              $query_group_fwsat = mysqli_query($conn, $sql_group_fwsat);
                              echo "<tbody>";
                              $i=1;
                              while ($result_group_fwsat = mysqli_fetch_array($query_group_fwsat))
                              {
                                  $vazio = $i;
                                  $fwsatversao = $result_group_fwsat['firmware'];
                                  $fwsatqntd = $result_group_fwsat['qntd'];

                                  echo "<tr data-toggle='modal' data-target='#modal_dados_loja".$result_group_fwsat['firmware']."'>";
                                      echo "<td>".$vazio."</td>";
                                      echo "<td>".$fwsatversao."</td>";
                                      echo "<td>".$fwsatqntd."</td>";
                                      echo "<td><button type='button' class='btn btn-outline-info btn-sm' data-toggle='modal' data-target='#modal_dados_loja".$result_group_fwsat['firmware']."'>Visualizar</button></a></td>";
                                  echo "</tr>";
                                  echo "<div class='modal fade' id='modal_dados_loja".$result_group_fwsat['firmware']."' tabindex='-1' role='dialog' aria-hidden='true'>";
                                      echo "<div class='modal-dialog modal-dialog-scrollable modal-lg'>";
                                          echo "<div class='modal-content'>";
                                              echo "<div class='modal-header'>";
                                                  echo "<h5>Dados S@T - ".$result_group_fwsat['firmware']."</h5>";
                                                  echo "<button type='button' class='close' data-dismiss='modal'>&times</button>";
                                              echo "</div>";
                                              echo "<div class='modal-header'>";
                                                  echo '<a href="../export/export_sat_fw.php?fw='.$result_group_fwsat['firmware'].'"><button type="submit" class="btn btn-danger">Exportar</button></a>';
                                              echo "</div>";
                                              echo "<div class='modal-body'>";
                                                  echo "<form>";
                                                      echo '<div class="form-group">';
                                                          echo '<div class="row">';
                                                              echo '<div class="col-md-2">S@T</div>';
                                                              echo '<div class="col-md-3">Firmware</div>';
                                                              echo '<div class="col-md-2">Loja</div>';
                                                              echo '<div class="col-md-2">PDV</div>';
                                                              echo '<div class="col-md-2">IP</div>';
                                                          echo "</div>";
                                                          echo '<div class="row">';
                                                            $sql_fwsatversao = 'SELECT * FROM tb_sat WHERE firmware = "'.$result_group_fwsat['firmware'].'" AND Status = "Ativo"' ;
                                                              $query_fwsatversao = mysqli_query($conn, $sql_fwsatversao);
                                                              while ($result_fwsatversao = mysqli_fetch_array($query_fwsatversao))
                                                              {
                                                                  $fwsatnloja = $result_fwsatversao['sat'];
                                                                  $fwsatnversao = $result_fwsatversao['firmware'];
                                                                  $fwsatntpvs = $result_fwsatversao['loja'];
                                                                  $fwsatntpv = $result_fwsatversao['caixa'];
                                                                  $fwsatnip = $result_fwsatversao['ip'];

                                                                  echo '<div class="col-md-2">'.$fwsatnloja.'</div>';
                                                                  echo '<div class="col-md-3">'.$fwsatnversao.'</div>';
                                                                  echo '<div class="col-md-2">'.$fwsatntpvs.'</div>';
                                                                  echo '<div class="col-md-2">'.$fwsatntpv.'</div>';
                                                                  echo '<div class="col-md-2">'.$fwsatnip.'</div>'; 
                                                              }
                                                          echo '</div>';
                                                      echo '</div>';
                                                  echo "</form>";
                                              echo "</div>";
                                          echo "</div>";
                                      echo "</div>";
                                  //echo "</div>";
                                  $i++;

                              }
                              echo "</tbody>";
                          ?>
                      </table>
                  </div>
              </div>
              <div class="col-sm-4" style="float:left;">
                <div class="table-responsive">
                  <table class="table table-sm table-striped table-bordered table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Modelo S@T</th>
                              <th>Quantidade S@Ts</th>
                              <th>Visualizar</th>
                          </tr>
                      </thead>
                          <?php
                              $sql_group_mdlsat = "SELECT modelo_sat, COUNT(id) AS qntd FROM tb_sat WHERE Status = 'Ativo' GROUP BY modelo_sat" ;
                              $query_group_mdlsat = mysqli_query($conn, $sql_group_mdlsat);
                              echo "<tbody>";
                              $i=1;
                              while ($result_group_mdlsat = mysqli_fetch_array($query_group_mdlsat))
                              {
                                  $vazio = $i;
                                  $mdlsatversao = $result_group_mdlsat['modelo_sat'];
                                  $mdlsatqntd = $result_group_mdlsat['qntd'];

                                  echo "<tr data-toggle='modal' data-target='#modal_dados_loja".$result_group_mdlsat['modelo_sat']."'>";
                                      echo "<td>".$vazio."</td>";
                                      echo "<td>".$mdlsatversao."</td>";
                                      echo "<td>".$mdlsatqntd."</td>";
                                      echo "<td><button type='button' class='btn btn-outline-info btn-sm' data-toggle='modal' data-target='#modal_dados_loja".$result_group_mdlsat['modelo_sat']."'>Visualizar</button></a></td>";
                                  echo "</tr>";
                                  echo "<div class='modal fade' id='modal_dados_loja".$result_group_mdlsat['modelo_sat']."' tabindex='-1' role='dialog' aria-hidden='true'>";
                                      echo "<div class='modal-dialog modal-dialog-scrollable modal-lg'>";
                                          echo "<div class='modal-content'>";
                                              echo "<div class='modal-header'>";
                                                  echo "<h5>Dados Modelo S@T - ".$result_group_mdlsat['modelo_sat']."</h5>";
                                                  echo "<button type='button' class='close' data-dismiss='modal'>&times</button>";
                                              echo "</div>";
                                              echo "<div class='modal-header'>";
                                                  echo '<a href="../export/export_sat_mdl.php?mdl='.$result_group_mdlsat['modelo_sat'].'"><button type="submit" class="btn btn-danger">Exportar</button></a>';
                                              echo "</div>";
                                              echo "<div class='modal-body'>";
                                                  echo "<form>";
                                                      echo '<div class="form-group">';
                                                          echo '<div class="row">';
                                                              echo '<div class="col-md-2">S@T</div>';
                                                              echo '<div class="col-md-3">Firmware</div>';
                                                              echo '<div class="col-md-2">Loja</div>';
                                                              echo '<div class="col-md-2">PDV</div>';
                                                              echo '<div class="col-md-2">IP</div>';
                                                          echo "</div>";
                                                          echo '<div class="row">';
                                                              $sql_mdlsatversao = 'SELECT * FROM tb_sat WHERE modelo_sat = "'.$result_group_mdlsat['modelo_sat'].'" AND Status = "Ativo" ORDER BY sat' ;
                                                              $query_mdlsatversao = mysqli_query($conn, $sql_mdlsatversao);
                                                              while ($result_mdlsatversao = mysqli_fetch_array($query_mdlsatversao))
                                                              {
                                                                  $mdlsatnloja = $result_mdlsatversao['sat'];
                                                                  $mdlsatnversao = $result_mdlsatversao['firmware'];
                                                                  $mdlsatntpvs = $result_mdlsatversao['loja'];
                                                                  $mdlsatntpv = $result_mdlsatversao['caixa'];
                                                                  $dmlsatnip = $result_mdlsatversao['ip'];

                                                                  echo '<div class="col-md-2">'.$mdlsatnloja.'</div>';
                                                                  echo '<div class="col-md-3">'.$mdlsatnversao.'</div>';
                                                                  echo '<div class="col-md-2">'.$mdlsatntpvs.'</div>';
                                                                  echo '<div class="col-md-2">'.$mdlsatntpv.'</div>';
                                                                  echo '<div class="col-md-2">'.$dmlsatnip.'</div>'; 
                                                              }
                                                          echo '</div>';
                                                      echo '</div>';
                                                  echo "</form>";
                                              echo "</div>";
                                          echo "</div>";
                                      echo "</div>";
                                  //echo "</div>";
                                  $i++;

                              }
                              echo "</tbody>";
                          ?>
                      </table>
                  </div>
              </div>
              <div class="col-sm-4" style="float:left;">
                <div class="table-responsive">
                  <table class="table table-sm table-striped table-bordered table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Modelo S@T</th>
                              <th>Ano</th>
                              <th>Quantidade</th>
                              <th>Visualizar</th>
                          </tr>
                      </thead>
                          <?php
                              $sql_group_dtsat = "SELECT YEAR(data_fim_ativacao) AS ano,modelo_sat, COUNT(id) AS qntd FROM tb_sat WHERE Status = 'Ativo' GROUP BY YEAR(data_fim_ativacao)" ;
                              $query_group_dtsat = mysqli_query($conn, $sql_group_dtsat);
                              echo "<tbody>";
                              $i=1;
                              while ($result_group_dtsat = mysqli_fetch_array($query_group_dtsat))
                              {
                                  $vazio = $i;
                                  $dtsatversao = $result_group_dtsat['modelo_sat'];
                                  $dtsatano = $result_group_dtsat['ano'];
                                  $dtsatqntd = $result_group_dtsat['qntd'];

                                  echo "<tr data-toggle='modal' data-target='#modal_dados_loja".$result_group_dtsat['ano']."'>";
                                      echo "<td>".$vazio."</td>";
                                      echo "<td>".$dtsatversao."</td>";
                                      echo "<td>".$dtsatano."</td>";
                                      echo "<td>".$dtsatqntd."</td>";
                                      echo "<td><button type='button' class='btn btn-outline-info btn-sm' data-toggle='modal' data-target='#modal_dados_loja".$result_group_dtsat['ano']."'>Visualizar</button></a></td>";
                                  echo "</tr>";
                                  echo "<div class='modal fade' id='modal_dados_loja".$result_group_dtsat['ano']."' tabindex='-1' role='dialog' aria-hidden='true'>";
                                      echo "<div class='modal-dialog modal-dialog-scrollable modal-lg'>";
                                          echo "<div class='modal-content'>";
                                              echo "<div class='modal-header'>";
                                                  echo "<h5>Dados Modelo S@T - ".$result_group_dtsat['ano']."</h5>";
                                                  echo "<button type='button' class='close' data-dismiss='modal'>&times</button>";
                                              echo "</div>";
                                              echo "<div class='modal-header'>";
                                                  echo '<a href="../export/export_sat_vencimento.php?venc='.$result_group_dtsat['ano'].'"><button type="submit" class="btn btn-danger">Exportar</button></a>';
                                              echo "</div>";
                                              echo "<div class='modal-body'>";
                                                  echo "<form>";
                                                      echo '<div class="form-group">';
                                                          echo '<div class="row">';
                                                              echo '<div class="col-md-2">S@T</div>';
                                                              echo '<div class="col-md-3">Firmware</div>';
                                                              echo '<div class="col-md-2">Loja</div>';
                                                              echo '<div class="col-md-2">PDV</div>';
                                                              echo '<div class="col-md-2">IP</div>';
                                                          echo "</div>";
                                                          echo '<div class="row">';
                                                              $sql_fwsatversao = 'SELECT * FROM tb_sat WHERE YEAR(data_fim_ativacao) = "'.$result_group_dtsat['ano'].'" AND Status = "Ativo"' ;
                                                              $query_fwsatversao = mysqli_query($conn, $sql_fwsatversao);
                                                              while ($result_fwsatversao = mysqli_fetch_array($query_fwsatversao))
                                                              {
                                                                  $fwsatnloja = $result_fwsatversao['sat'];
                                                                  $fwsatnversao = $result_fwsatversao['firmware'];
                                                                  $fwsatntpvs = $result_fwsatversao['loja'];
                                                                  $fwsatntpv = $result_fwsatversao['caixa'];
                                                                  $fwsatnip = $result_fwsatversao['ip'];

                                                                  echo '<div class="col-md-2">'.$fwsatnloja.'</div>';
                                                                  echo '<div class="col-md-3">'.$fwsatnversao.'</div>';
                                                                  echo '<div class="col-md-2">'.$fwsatntpvs.'</div>';
                                                                  echo '<div class="col-md-2">'.$fwsatntpv.'</div>';
                                                                  echo '<div class="col-md-2">'.$fwsatnip.'</div>'; 
                                                              }
                                                          echo '</div>';
                                                      echo '</div>';
                                                  echo "</form>";
                                              echo "</div>";
                                          echo "</div>";
                                      echo "</div>";
                                  //echo "</div>";
                                  $i++;

                              }
                              echo "</tbody>";
                          ?>
                      </table>
                  </div>
              </div>
            </div>
            <h4>S@Ts com Erro de IP</h4>
            </br>
            <div class="row">
                <div class="col-sm-4" style="float:left;">
                    <div class="table-responsive">
                        <table class="table table-sm table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>PDV</th>
                                    <th>Quatidade de S@Ts</th>
                                    <th>Visualizar</th>
                                </tr>
                            </thead>
                            <?php
                                $sql_group_erripsat = "SELECT caixa, COUNT(sat) AS qntd FROM cn_sat_erro_ip GROUP BY caixa" ;
                                $query_group_erripsat = mysqli_query($conn, $sql_group_erripsat);
                                echo "<tbody>";
                                $i=1;
                                while ($result_group_erripsat = mysqli_fetch_array($query_group_erripsat))
                                {
                                    $vazio = $i;
                                    $erripsatcx = $result_group_erripsat['caixa'];
                                    $erripsatqntd = $result_group_erripsat['qntd'];
                                    echo "<tr>";
                                        echo "<td>".$i."</td>";
                                        echo "<td>".$erripsatcx."</td>";
                                        echo "<td>".$erripsatqntd."</td>";
                                        echo "<td><button type='button' class='btn btn-outline-info btn-sm' data-toggle='modal' data-target='#modal_dados_loja".$result_group_erripsat['caixa']."'>Visualizar</button></a></td>";
                                    echo "</tr>";
                                    echo "<div class='modal fade' id='modal_dados_loja".$result_group_erripsat['caixa']."' tabindex='-1' role='dialog' aria-hidden='true'>";
                                        echo "<div class='modal-dialog modal-dialog-scrollable modal-lg'>";
                                            echo "<div class='modal-content'>";
                                                echo "<div class='modal-header'>";
                                                    echo "<h5>Dados Modelo S@T - ".$result_group_erripsat['caixa']."</h5>";
                                                    echo "<button type='button' class='close' data-dismiss='modal'>&times</button>";
                                                echo "</div>";
                                                echo "<div class='modal-header'>";
                                                    echo '<a href="../export/export_sat_errip.php?errip='.$result_group_erripsat['caixa'].'"><button type="submit" class="btn btn-danger">Exportar</button></a>';
                                                echo "</div>";
                                                echo "<div class='modal-body'>";
                                                    echo "<form>";
                                                        echo '<div class="form-group">';
                                                            echo '<div class="row">';
                                                                echo '<div class="col-md-2">S@T</div>';
                                                                echo '<div class="col-md-3">Loja</div>';
                                                                echo '<div class="col-md-2">Caixa</div>';
                                                                echo '<div class="col-md-4">IP</div>';
                                                            echo "</div>";
                                                            echo '<div class="row">';
                                                                $sql_erripsat = 'SELECT loja, caixa, sat, ip FROM cn_sat_erro_ip WHERE caixa = "'.$result_group_erripsat['caixa'].'"';
                                                                $query_erripsat = mysqli_query($conn, $sql_erripsat);
                                                                while ($result_erripsat = mysqli_fetch_array($query_erripsat))
                                                                {
                                                                    $erripsatnloja = $result_erripsat['loja'];
                                                                    $erripsatntpv = $result_erripsat['caixa'];
                                                                    $erripsatnsat = $result_erripsat['sat'];
                                                                    $erripsatnip = $result_erripsat['ip'];

                                                                    echo '<div class="col-md-2">'.$erripsatnsat.'</div>';
                                                                    echo '<div class="col-md-3">'.$erripsatnloja.'</div>';
                                                                    echo '<div class="col-md-2">'.$erripsatntpv.'</div>';
                                                                    echo '<div class="col-md-4">'.$erripsatnip.'</div>'; 
                                                                }
                                                            echo '</div>';
                                                        echo '</div>';
                                                    echo "</form>";
                                                echo "</div>";
                                            echo "</div>";
                                        echo "</div>";
                                        $i++;
                                }
                                echo "</tbody>";

                            ?>
                        </table>
                    </div>
                </div>
            </div>
            <h4>Dados Impressoras</h4>
            </br>
            <div class="row">
              <div class="col-sm-4" style="float:left;">
                <div class="table-responsive">
                  <table class="table table-sm table-striped table-bordered table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Impressora</th>
                              <th>Quantidade</th>
                              <th>Visualizar</th>
                          </tr>
                      </thead>
                          <?php
                              $sql_group_impressora = "SELECT impressora, COUNT(id) AS qntd FROM tb_consolidado_equipamentos GROUP BY impressora" ;
                              $query_group_impressora = mysqli_query($conn, $sql_group_impressora);
                              echo "<tbody>";
                              $i=1;
                              while ($result_group_impressora = mysqli_fetch_array($query_group_impressora))
                              {
                                  $vazio = $i;
                                  $mdlprinter = $result_group_impressora['impressora'];
                                  $qntdprinter = $result_group_impressora['qntd'];

                                  echo "<tr data-toggle='modal' data-target='#modal_dados_loja".$result_group_impressora['impressora']."'>";
                                      echo "<td>".$vazio."</td>";
                                      echo "<td>".$mdlprinter."</td>";
                                      echo "<td>".$qntdprinter."</td>";
                                      echo "<td><button type='button' class='btn btn-sm btn-outline-info' data-toggle='modal' data-target='#modal_dados_loja".$result_group_impressora['impressora']."'>Visualizar</button></a></td>";
                                  echo "</tr>";
                                  echo "<div class='modal fade' id='modal_dados_loja".$result_group_impressora['impressora']."' tabindex='-1' role='dialog' aria-hidden='true'>";
                                      echo "<div class='modal-dialog modal-dialog-scrollable modal-lg'>";
                                          echo "<div class='modal-content'>";
                                              echo "<div class='modal-header'>";
                                                  echo "<h5>Dados Impressora - ".$result_group_impressora['impressora']."</h5>";
                                                  echo "<button type='button' class='close' data-dismiss='modal'>&times</button>";
                                              echo "</div>";
                                              echo "<div class='modal-header'>";
                                                  echo '<a href="../export/export_impressora_mdl.php?mdl='.$result_group_impressora['impressora'].'"><button type="submit" class="btn btn-danger">Exportar</button></a>';
                                              echo "</div>";
                                              echo "<div class='modal-body'>";
                                                  echo "<form>";
                                                      echo '<div class="form-group">';
                                                          echo '<div class="row">';
                                                              echo '<div class="col-md-2">Impressora</div>';
                                                              echo '<div class="col-md-3">Firmware</div>';
                                                              echo '<div class="col-md-2">Loja</div>';
                                                              echo '<div class="col-md-1">PDV</div>';
                                                              echo '<div class="col-md-3">Última Atualização</div>';
                                                          echo "</div>";
                                                          echo '<div class="row">';
                                                              $sql_printerversao = 'SELECT impressora, firmware, loja, pdv, data_modificacao FROM tb_consolidado_equipamentos WHERE impressora = "'.$result_group_impressora['impressora'].'" ORDER BY data_modificacao DESC' ;
                                                              $query_printerversao = mysqli_query($conn, $sql_printerversao);
                                                              while ($result_printerversao = mysqli_fetch_array($query_printerversao))
                                                              {
                                                                  $printernprinter = $result_printerversao['impressora'];
                                                                  $printernversao = $result_printerversao['firmware'];
                                                                  $printernloja = $result_printerversao['loja'];
                                                                  $printerntpv = $result_printerversao['pdv'];
                                                                  $printerndt = $result_printerversao['data_modificacao'];

                                                                  echo '<div class="col-md-2">'.$printernprinter.'</div>';
                                                                  echo '<div class="col-md-3">'.$printernversao.'</div>';
                                                                  echo '<div class="col-md-2">'.$printernloja.'</div>';
                                                                  echo '<div class="col-md-1">'.$printerntpv.'</div>';
                                                                  echo '<div class="col-md-3">'.$printerndt.'</div>'; 
                                                              }
                                                          echo '</div>';
                                                      echo '</div>';
                                                  echo "</form>";
                                              echo "</div>";
                                          echo "</div>";
                                      echo "</div>";
                                  //echo "</div>";
                                  $i++;

                              }
                              echo "</tbody>";
                          ?>
                      </table>
                  </div>
              </div>
            </div>
        </main>    
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <script src="../js/jquery-3.2.1.min.js"></script>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="../js/jquery-slim.min.js"><\/script>')</script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>

        <!-- Icons -->
        <script src="../js/feather.min.js"></script>
        <script>
          feather.replace()
        </script>
  </body>
</html>
