<?php
    include_once("../security/seguranca.php");
	protegePagina();
?>

<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicon.ico">

    <title>Support TPVs | SATs</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../dashboard.css" rel="stylesheet">
	<!--<link href="theme.css" rel="stylesheet">-->
	
	<script src="../js/ie-emulation-modes-warning.js"></script>
	<?php
			date_default_timezone_set("America/Sao_Paulo");
     		setlocale(LC_ALL, 'pt_BR');
			$num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
			//Obter a data atual
			$resultado_qnt_cadastros = mysqli_query($conn, $num_users);
			$row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
			
			$data['atual'] = date('Y-m-d H:i:s');	

			//Diminuir 20 segundos 
			$data['online'] = strtotime($data['atual'] . " - 20 seconds");
			$data['online'] = date("Y-m-d H:i:s",$data['online']);
			
			//Pesquisar os ultimos usuarios online nos 20 segundo
			$result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE data_final >= '" . $data['online'] . "'";
			
			$resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
			$row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
			
			$qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
		?>
  </head>

  <body>
  <script src="../js/jquery-3.2.1.min.js"></script>
		
		<script>
		//Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
		setInterval(function(){
			//Incluir e enviar o POST para o arquivo responsável em fazer contagem
			$.post("../processo/processa_vis.php", {contar: '',}, function(data){
				$('#online').text(data);
			});
		}, 10000);
		</script>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="user2.php">User N2 -
		<?php  
		  echo $_SESSION['usuarioNome'];
		  ?>
	  </a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="../security/sair.php"><span data-feather="share"></span>
           Sair</a>
        </li>
      </ul>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="user2.php">
                  <span data-feather="home"></span>
                  Home<span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="consultas.php">
                  <span data-feather="search"></span>
                  Consultas
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="sats.php?pagina=1">
                  <span data-feather="camera"></span>
                  SATs
                </a>
              </li>
	            <li class="nav-item">
                <a class="nav-link" href="list_lojas.php?pagina=1">
                  <span data-feather="list"></span>
                  Cadastro de Lojas
                </a>
              </li>
			        <li class="nav-item">
                <a class="nav-link" href="srv_chaves.php">
                  <span data-feather="server"></span>
                  Servidor de Chaves
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="usuarios.php">
                  <span data-feather="users"></span>
                  Usuarios<span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#?" data-toggle="modal" data-target="#editaSenha">
                  <span data-feather="lock"></span>
                  Alterar Senha
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="tabelas.php">
                  <span data-feather="grid"></span>
                  Tabelas  
                </a>
              </li>
            </ul>
           </div>
          </nav>
          </div>
        </div>
        <main role="main" class="col-md-4 ml-sm-auto col-lg-10 pt-4 px-4">
        <form name="frm_senha" action="../processo/salva_senha.php" method="POST">
              <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                          <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                        </div>
                        <div class="form-group">
                          <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                          <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Confirma</button>
                    </div>
                  </div>
                </div>
              </div>
        </form> 
      </main>
<main role="main" class="col-md-8 ml-sm-auto col-lg-10 pt-3 px-4">
<h4>Cadastro de SATs</h4>
    <br>    
    <nav class="col-md-8 ml-sm-auto col-lg-1 pt-6 px-4">
    </nav>
<div class="page-header">
  <div class="row">
    <div class="col-sm-2">
    </div>
    <div class="col-sm-12">
      <table class="table table-sm table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>Série</th>
            <th>Loja</th>
            <th>Caixa</th>
            <th>IP</th>
            <th>Firmware</th>
            <th>Layout</th>
            <th>Ações</th>
          </tr>
        </thead>
             <?php
                  include_once("../security/conecta.php");
                 // Conectando ao banco de dados:
                
                 $pagina = $_GET['pagina'];
                 if (!$pagina)
                 {
                  $pc = "1";
                 } else {
                  $pc = $pagina;
                 }
                
                 $total_reg = "10";
                 $inicio = $pc - 1;
                 $inicio = $inicio * $total_reg;
                 
                 $limite = "SELECT * FROM tb_sat WHERE sat != '' ORDER BY sat LIMIT ".$inicio.",".$total_reg."";  
                 $sql = "SELECT * FROM tb_sat WHERE sat != '' ORDER BY sat";

                 $todos = mysqli_query($conn,$sql) or die("Erro ao retornar dados");

                 $tr = mysqli_num_rows($todos);
                 $tp = $tr / $total_reg;

                 $resultado = mysqli_query($conn,$limite) or die("Erro ao retornar dados");
                 // Obtendo os dados por meio de um loop while
                 echo "<tbody>";
                 while ($registro = mysqli_fetch_array($resultado))
                 {
                    $serie = $registro['sat'];
                    $loja = $registro['loja'];
                    $caixa = $registro['caixa'];
                    $ip = $registro['ip'];
                    $firmware = $registro['firmware'];
                    $layout = $registro['layout'];
                        

                      echo "<tr>";  
                      echo "<td>".$serie."</td>";
                      echo "<td>".$loja."</td>";
                      echo "<td>".$caixa."</td>";
                      echo "<td>".$ip."</td>";
                      echo "<td>".$firmware."</td>";
                      echo "<td>".$layout."</td>";

                      echo "<td><button type='button' class='btn btn-info btn-sm'>Visualizar</button> 
                            <a href='editaloja.php?id=".$registro['sat']."'><button type='button' class='btn btn-success btn-sm'>Atualizar</button></a> 
                          </td>";    
                      echo "</tr>";  
                 }
                     echo "</tbody>";

             ?>
        </table>
      <h7>Exibindo de <?php echo $inicio;?> / <?php $soma = $inicio + $total_reg ; if ($soma>$tr){ echo $tr;}else{echo $soma;}; ?> de  <?php echo $tr; ?> | Total de páginas <?php echo $pagina ;?> / <?php echo ceil($tp); ?></h7>
    </div>
  </div>
</div>
<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-end">
<?php
    $anterior = $pc -1;
    $proximo = $pc +1;
    $ultima=ceil($tp);

    if ($pc==1){
        echo "<li class='page-item disabled'>";
        echo "<a class='page-link' href='?pagina=1' tabindex='-1'>Primeira</a>";
    }
    if ($pc>1){
        echo '<li class="page-item">';
        echo "<a class='page-link' href='?pagina=1' tabindex='-1'>Primeira</a>";
        echo '</li>';
    }

    if ($pc==1){
        echo "<li class='page-item disabled'>";
        echo "<a class='page-link' href='?pagina=$anterior' tabindex='-1'>Anterior</a>";
    }
    if ($pc>1){
        echo '<li class="page-item">';
        echo "<a class='page-link' href='?pagina=$anterior' tabindex='-1'>Anterior</a>";
        echo '</li>';
    }

    $penultima= $ultima -1;
    $antipenultima = $ultima -2;
    $pg1= $pagina;
    $pg2=$pg1 -1;
    $pg3=$pg2 -1;
    if ($pg1 == $ultima){
       echo "<li class='page-item'><a class='page-link' href='?pagina=$pg1'>".$pg1."</a></li>";

       $pg2=$pg1 -1;
       $pg3=$pg2 -1;
            
    }else{
            if (($pg2 == 0) OR ($pg2 < 0)){
           
                $pg1= $pagina;
                $pg2=$pg1 +1;
                $pg3=$pg2 +1;

            }

            if (($pg3 == 0) OR ($pg3 < 0)){
           
                $pg1= $pagina;
                $pg2=$pg1 +1;
                $pg3=$pg2 +1;

            }else{

                $pg1= $pagina;
                $pg2=$pg1 +1;
                $pg3=$pg2 +1;

            }

          if ($pg3 > $ultima){
              $pg1 = $pagina -2;
              $pg2 = $pg1 +1;
              $pg3 = $pg2 +1;

          }
          if ($pg3 == $ultima){
              $pg1 = $pagina -3;
              $pg2 = $pg1 +1;
              $pg3 = $pg2 +1;

          }


          echo "<li class='page-item'><a class='page-link' href='?pagina=$pg1'>".$pg1."</a></li>";
          echo "<li class='page-item'><a class='page-link' href='?pagina=$pg2'>".$pg2."</a></li>";
          echo "<li class='page-item'><a class='page-link' href='?pagina=$pg3'>".$pg3."</a></li>";
    }

    if ($pc<$tp){

        echo '<li class="page-item">';
        echo "<a class='page-link' href='?pagina=$proximo'>Próximo</a>";
        echo '</li>';

    }

     if ($pc==$ultima){

        echo '<li class="page-item disabled">';
        echo "<a class='page-link' href='?pagina=$proximo'>Próximo</a>";
        echo '</li>';

    }
    
    echo "<a class='page-link' href='?pagina=$ultima'>Última</a>";
?>  
  </ul>
</nav>
</main>
</div>
</div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-slim.min.js"><\/script>')</script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="../js/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
  </body>
</html>
