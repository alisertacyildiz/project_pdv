<?php
    require_once("../security/seguranca.php");
	protegePagina();
	
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicon.ico">
	
    <title>Support TPVs | Usuarios</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
	
	 <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dashboard.css" rel="stylesheet">

    <script src="../js/ie-emulation-modes-warning.js"></script>
	
		<?php
    date_default_timezone_set("America/Sao_Paulo");
        setlocale(LC_ALL, 'pt_BR');
			$num_users = "SELECT count(id) as cadastrado FROM tb_usuarios";
			//Obter a data atual
			$resultado_qnt_cadastros = mysqli_query($conn, $num_users);
			$row_qnt_cadastros = mysqli_fetch_assoc($resultado_qnt_cadastros);
			
			$data['atual'] = date('Y-m-d H:i:s');	

			//Diminuir 20 segundos 
			$data['online'] = strtotime($data['atual'] . " - 20 seconds");
			$data['online'] = date("Y-m-d H:i:s",$data['online']);
			
			//Pesquisar os ultimos usuarios online nos 20 segundo
			$result_qnt_visitas = "SELECT count(id) as online FROM tb_visitas WHERE  data_final >= '" . $data['online'] . "'";
			
			$resultado_qnt_visitas = mysqli_query($conn, $result_qnt_visitas);
			$row_qnt_visitas = mysqli_fetch_assoc($resultado_qnt_visitas);
			
			$qnt_offline = ($row_qnt_cadastros['cadastrado'] - $row_qnt_visitas['online']);
		?>
  </head>

  <body>
  <script src="../js/jquery-3.2.1.min.js"></script>
		
		<script>
		//Executar a cada 10 segundos, para atualizar a qunatidade de usuários online
		setInterval(function(){
			//Incluir e enviar o POST para o arquivo responsável em fazer contagem
			$.post("../processo/processa_vis.php", {contar: '',}, function(data){
				$('#online').text(data);
			});
		}, 10000);
		</script>
      <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="user2.php">User N2 -
		<?php  
		  echo $_SESSION['usuarioNome'];
		  ?>
	  </a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="../security/sair.php"><span data-feather="share"></span>
           Sair</a>
        </li>
      </ul>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="user2.php">
                  <span data-feather="home"></span>
                  Home<span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="consultas.php">
                  <span data-feather="search"></span>
                  Consultas
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="sats.php?pagina=1">
                  <span data-feather="camera"></span>
                  SATs
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="list_lojas.php?pagina=1">
                  <span data-feather="list"></span>
                  Cadastro de Lojas
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="srv_chaves.php">
                  <span data-feather="server"></span>
                  Servidor de Chaves
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="usuarios.php">
                  <span data-feather="users"></span>
                  Usuarios<span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#?" data-toggle="modal" data-target="#editaSenha">
                  <span data-feather="lock"></span>
                  Alterar Senha
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="tabelas.php">
                  <span data-feather="grid"></span>
                  Tabelas  
                </a>
              </li>
            </ul>
           </div>
          </nav>
          </div>
        </div>
		<form name="frm_senha" action="../processo/salva_senha.php" method="POST">
              <div class="modal fade" id="editaSenha" tabindex="-1" role="dialog" aria-labelledby="editaSenhaLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Alteração de Senha</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Digite a nova senha</label>
                          <input type="password" class="form-control" name="nova-senha" placeholder="Nova Senha">
                        </div>
                        <div class="form-group">
                          <label for="message-text" class="col-form-label">Confirme a a nova senha</label>
                          <input type="password" class="form-control" name="conf-senha" placeholder="Confirme Senha"></textarea>
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Confirma</button>
                    </div>
                  </div>
                </div>
              </div>
        </form> 
<main role="main" class="col-md-8 ml-sm-auto col-lg-10 pt-3 px-4">
		<h4>Usuários Sistema</h4>
  </br>
<form class="form-signin" name="frm_usuario" action="../processo/reset_user.php" method="POST">
  <div class="page-header">
      <table class="table table-sm table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Login</th>
            <th>E-mail</th>
            <th>Nível</th>
            <th>Data Criação</th>
            <th>Ações</th>
          </tr>
        </thead>

               <?php
              		 // Conectando ao banco de dados:
              		 $usuario = "root";
              		 $senha = "diabrasil";
              		 $strcon = mysqli_connect('localhost', $usuario, $senha,'srvremoto') or die('Erro ao conectar ao banco de dados');
              		 $sql = "SELECT * FROM tb_usuarios ";
              		 $resultado = mysqli_query($strcon,$sql) or die("Erro ao retornar dados");
              		 // Obtendo os dados por meio de um loop while
                      echo "<tbody>";
                   while ($registro = mysqli_fetch_array($resultado))
              		 {
                  		$id = $registro['id'];
                  		$nome = $registro['nome'];
                  		$login = $registro['login'];
                  		$email = $registro['email'];
                  		$nivel = $registro['nivel'];
                  		$data_criacao = $registro['data_criacao'];
                  		$data_modificacao = $registro['data_modificacao'];
              			
              		    echo "<tr>";			
              		    echo "<td>".$id."</td>";

                        			$data_user['atual'] = date('Y-m-d H:i:s');	
                        			//Diminuir 20 segundos 
                        			$data_user['online'] = strtotime($data_user['atual'] . " - 20 seconds");
                        			$data_user['online'] = date("Y-m-d H:i:s",$data_user['online']);
                        			
                        			//Pesquisar os ultimos usuarios online nos 20 segundo
                        			$result_user_online = "SELECT * FROM tb_visitas WHERE id_usuario = '" . $registro['id'] . "' AND data_final >= '" . $data_user['online'] . "'";
                        			
                        			$resultado_user_online = mysqli_query($conn, $result_user_online);
                        			$row_user_online = mysqli_fetch_assoc($resultado_user_online);
                        			
                        			if (!empty($row_user_online))
                        			{
                        				echo "<td><img width='10px' height='10px' src='../img/online.png' />".$nome."</td>";
                        			}else{
                        				echo "<td><img width='10px' height='10px' src='../img/offline.png' />".$nome."</td>";
                        			}
                        			echo "<td>".$login."</td>";
                        			echo "<td>".$email."</td>";
                        			echo "<td>".$nivel."</td>";
                        			echo "<td>".$data_criacao."</td>";
                              echo "<td><button type='button' class='btn btn-sm btn-info' data-toggle='modal' data-target='#usuariosModal".$registro['id']."'>Visualizar</button> 
                                    <a href='../processo/reset_user.php?id=".$registro['id']."'><button type='button' class='btn btn-sm btn-warning'>Reset</button></a> 
                                  </td>";
                              echo "</tr>";
                              echo '<div class="modal fade" id="usuariosModal'.$registro['id'].'" tabindex="-1" role="dialog" aria-labelledby="usuariosModalLabel" aria-hidden="true">';
                              echo '<div class="modal-dialog" role="document">';
                                echo '<div class="modal-content">';
                                  echo '<div class="modal-header">';
                                    echo '<h5 class="modal-title" id="usuariosModalLabel">ID Usuário: '.$registro['id'].'</h5>';
                                    echo '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
                                      echo '<span aria-hidden="true">&times;</span>';
                                    echo '</button>';
                                  echo '</div>';
                                  echo '<div class="modal-body">';
                                    echo '<form>';
                                      echo '<div class="form-group">';
                                      echo '<label for="recipient-name" class="col-form-label">Nome</label>'; 
                                      echo "</br>"; 
                                      echo $registro['nome'];
                                      echo '</div>'; 
                                      echo '<div class="form-group">';
                                      echo '<label for="recipient-name" class="col-form-label">Login</label>'; 
                                       echo "</br>";
                                       echo $registro['login'];
                                      echo '</div>'; 
                                      echo '<div class="form-group">';
                                      echo '<label for="recipient-name" class="col-form-label">E-mail</label>';
                                       echo "</br>";
                                       echo $registro['email'];
                                      echo '</div>'; 
                                      echo '<div class="form-group">';
                                      echo '<label for="recipient-name" class="col-form-label">Nível</label>';
                                       echo "</br>";
                                       echo $registro['nivel'];
                                       echo '</div>';
                                       echo '<div class="form-group">';
                                       echo '<label for="recipient-name" class="col-form-label">Data Criação</label>';
                                       echo "</br>";
                                       echo $registro['data_criacao'];
                                      echo '</div>';
                                      echo '<div class="form-group">';
                                      echo '<label for="recipient-name" class="col-form-label">Data Modificação</label>'; 
                                       echo "</br>";
                                       echo $registro['data_modificacao'];
                                      echo '</div>';
                                    echo '</form>';
                                  echo '</div>';
                                  echo '<div class="modal-footer">';
                                    echo '<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Fechar</button>';
                                  echo '</div>';
                                echo '</div>';
                              echo '</div>';
                            echo '</div>';
                        		 }

                            echo "</tbody>";
                            mysqli_close($strcon);
                   
               ?>
        </table>
      </div>
    </div>
  </div>
</form>
</main>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-slim.min.js"><\/script>')</script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="../js/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

    <!-- Graphs 
    <script src="js/Chart.min.js"></script>
    <script>
      var ctx = document.getElementById("myChart");
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
          datasets: [{
            data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
            lineTension: 0,
            backgroundColor: 'transparent',
            borderColor: '#007bff',
            borderWidth: 4,
            pointBackgroundColor: '#007bff'
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: false
              }
            }]
          },
          legend: {
            display: false,
          }
        }
      });
    </script>
	-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="../js/jquery.min.js"><\/script>')</script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/docs.min.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="../js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
