<?php
    $txt_usuario = (isset($_COOKIE['cookieUsuario'])) ? base64_decode($_COOKIE['cookieUsuario']) : '';
    $txt_senha = (isset($_COOKIE['cookieSenha'])) ? base64_decode($_COOKIE['cookieSenha']) : '';
    $lembreme = (isset($_COOKIE['cookieLembreme'])) ? base64_decode($_COOKIE['cookieLembreme']) : '';
    $checked = ($lembreme == 'rememberme') ? 'checked' : '';
    require_once("security/valida.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicon.ico">
	
    <title>Support TPVs | Login</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>
</head>

<body class="text-center">
    <form class="form-signin" name="frmLogin" action="index.php" method="POST">
    <img class="mb-4" src="img/Logo-1.png" alt="" width="175" height="175">
        <h1 class="h3 mb-3 font-weight-normal">Acesso Restrito</h1>
        <label name="lbl_usuario" class="sr-only">Usuário</label>
        <input type="text" value="<?=$txt_usuario?>" name="txt_usuario" id="txt_usuario" class="form-control" placeholder="Digite seu usuário" required autofocus>
        <label for="inputPassword" class="sr-only">Senha</label>
        <input type="password" value="<?=$txt_senha?>" name="txt_senha" id="txt_senha" class="form-control" placeholder="Digite sua senha" required>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="lembrete" value="rememberme" <?=$checked?>> Remember me
            </label>
        </div>
		</div>
        <button class="btn btn-lg btn-danger btn-block" type="submit">Entrar</button>
	
        <p class="mt-5 mb-3 text-muted">&copy; 2017-2019</p>
    </form>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>