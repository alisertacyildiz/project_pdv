<?php
    require_once("../security/seguranca.php");
	protegePagina();
	
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicon.ico">

    <title>Support TPVs | Reset</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../signin.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../js/ie-emulation-modes-warning.js"></script>
</head>

<body class="text-center">
<?php


if($_SESSION['usuarioNivel'] == 1){
	//session_start();
	include_once("../security/conecta.php");
	$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
	if(!empty($id)){
		$padrao = password_hash('123456', PASSWORD_DEFAULT, ['cost' => 12 ]);
		$result_usuario = "UPDATE tb_usuarios set senha ='$padrao', data_modificacao=NOW() WHERE id='$id'";
		$resultado_usuario = mysqli_query($conn, $result_usuario);
		if(mysqli_affected_rows($conn)){
			echo "<script>alert('Reset de senha com sucesso!');</script>";
			echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../admin/usuarios.php">';
		
		}else{
			
			echo "<script>alert('Erro no processo!');</script>";
			echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../admin/usuarios.php">';
		}
	}else{	
		$_SESSION['msg'] = "<p style='color:red;'>Necessário selecionar um usuário</p>";
		header("Location: ../admin/usuarios.php");
	}
}else{
		//session_start();
		include_once("../security/conecta.php");
		$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
		$resultado_nivel = "SELECT * FROM tb_usuarios WHERE id = '".$id."'";
		$resulta_nivel = mysqli_query($conn, $resultado_nivel);
		$row_result_nivel = mysqli_fetch_assoc($resulta_nivel);
		
		$nivel = $row_result_nivel['nivel'];
		//echo "$nivel";
	
		if ($nivel == 1){
			echo "<script>alert('Ops!Usuário não permitido!');</script>";
			echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user2/usuarios.php">';
			}else{
				$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
				if(!empty($id)){
					$padrao = '123456';
					$result_usuario = "UPDATE tb_usuarios set senha ='$padrao',data_modificacao=NOW() WHERE id='$id'";
					$resultado_usuario = mysqli_query($conn, $result_usuario);
					if(mysqli_affected_rows($conn)){
						echo "<script>alert('Reset de senha com sucesso!');</script>";
						echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user2/usuarios.php">';
						
					}else{
						
						echo "<script>alert('Erro no processo!');</script>";
						echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user2/usuarios.php">';
					}
				}else{	
					$_SESSION['msg'] = "<p style='color:red;'>Necessário selecionar um usuário</p>";
					header("Location: ../user2/usuarios.php");
				}
		}
}
?>

</html>