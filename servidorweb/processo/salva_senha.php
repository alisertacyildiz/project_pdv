<?php
include_once ("../security/seguranca.php");
protegePagina();
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="../img/favicon.ico">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
	
	 <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dashboard.css" rel="stylesheet">

    <script src="../js/ie-emulation-modes-warning.js"></script>
	
    <title>Support TPVs | Password</title>
  </head>
  <body>
    <?php
        include_once ("../security/conecta.php");
        $id = $_SESSION['usuarioID'];
        $nivel = $_SESSION['usuarioNivel'];
        $nova_senha = (isset($_POST['nova-senha'])) ? $_POST['nova-senha'] : '';
        $conf_senha = (isset($_POST['conf-senha'])) ? $_POST['conf-senha'] : '';
        $data = date('Y-m-d H:i:s');

        if (!empty($nova_senha) && !empty($conf_senha)){
            if ($nova_senha != $conf_senha){
                //$_SESSION['msg'] = "<strong>Ops!</strong> Usuário ou Senha inválidos.";
                echo "<script>alert('Senhas não coincidem!');</script>";
                echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user/user.php">';
            }else{
				$senha = password_hash($nova_senha, PASSWORD_DEFAULT, ['cost' => 12 ]);
                $update = "UPDATE tb_usuarios SET senha = '$senha', data_modificacao='$data' WHERE id = '$id'";
                $query = mysqli_query($conn, $update);
                    if (mysqli_affected_rows($conn)){
                        if($nivel == 1){
                            echo "<script>alert('Senha alterada com sucesso!');</script>";
                            echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../admin/admin.php">';    
                        }elseif($nivel == 2){
                            echo "<script>alert('Senha alterada com sucesso!');</script>";
                            echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user2/user2.php">';
                        }elseif($nivel == 3){
                            echo "<script>alert('Senha alterada com sucesso!');</script>";
                            echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user/user.php">';
                        }elseif($nivel == 4){
                            echo "<script>alert('Senha alterada com sucesso!');</script>";
                            echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user3/user.php">';
                        }
                        
                    }else{
                        echo "<script>alert('Erro no processo!');</script>";
                        echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user/user.php">';
                    }
            }    
        }else{
            echo "<script>alert('Senha em branco!');</script>";
            echo '<meta HTTP-EQUIV="Refresh" CONTENT="0; URL=../user/user.php">';
        }
    ?>
  </body>
</html>

