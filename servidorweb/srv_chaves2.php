 DirectoryIterator consegui resolver, segue o código atualizado:

   <head>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>  

<?php

$path = "/mnt/claves_sat/";

// Título
echo "<h2>Lista de Arquivos:</h2><br />";

// Abre a tabela, cria títulos
echo "<table>";
echo "<tr> <th>Nome</th> </tr>";

// Loop que gera registros
foreach (new DirectoryIterator($path) as $fileInfo) {

    if($fileInfo->isDot()) continue;

    // Imprime linhas de registros
    echo "<tr>
            <td>
                <a href='".$path.$fileInfo->getFilename() ."'>".$fileInfo->getFilename()."</a><br/>
            </td>
          </tr>";
}

// Fecha a tabela
echo "</table>";

?>
